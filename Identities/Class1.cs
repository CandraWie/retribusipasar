﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Identities
{
    public class IdentitasUtama
    {
        private string snamanasabah;
        private string snomorrekening;
        private Int64 ideposit;
        private string ssingleid;

        public string NamaNasabah
        {
            get
            {
                return snamanasabah;
            }
            set
            {
                if (value.Replace(" ", "").Length == 0)
                    throw new Exception("Nama Nasabah tidak valid");

                if (value.Length > 200)
                    throw new Exception("Nama Nasabah maksimal 200 karakter");

                snamanasabah = value.Replace("  ", " ").Trim().ToUpper();
            }
        }
        
        public string NomorRekening
        {
            get
            {
                return snomorrekening;
            }
            set
            {
                string stemp = value.Replace(" ", "").Replace(".", "").Replace("-", "").ToUpper();
                if (stemp.Length > 30)
                    throw new Exception("Nomor Rekening maksimal 30 karakter");
                snomorrekening = stemp;
            }
        }

        public string Deposit
        {
            get
            {
                return ideposit.ToString();
            }
            set
            {
                ideposit = 0;
                string stemp = value.Replace(" ", "").Replace(".", "");
                if (!Int64.TryParse(stemp, out ideposit))
                    throw new Exception("Nominal Deposit tidak valid");
            }
        }

        public string CurrencyDeposit
        {
            get
            {
                return ideposit.ToString("#,0").Replace(",", ".");
            }
        }

        public string SingleID
        {
            get
            {
                return ssingleid;
            }
            set
            {
                string stemp = value.Replace(" ", "").Replace(".", "").Replace("-", "").ToUpper();
                if (stemp.Length > 30)
                    throw new Exception("Nomor Rekening maksimal 30 karakter");
                ssingleid = stemp;
            }
        }

    }

    public class RetribusiPasar
    {
        private string snpwrd;
        private string sidkios;
        private string stglterakhir;

        public string Id_NPWRD
        {
            get
            {
                return "";
            }
        }
    }

    public class Transaksi
    {
        private bool bsukses;
        private string sketerangan;
        private Int64 inominal;
        private Int64 isaldoakhir;
        private string skoderef;

        public string WaktuTransaksi
        {
            get
            {
                return DateTime.Now.ToString("HHmmss");
            }
        }

        public string TanggalTransaksi
        {
            get
            {
                return DateTime.Now.ToString("ddMMyyyy");
            }
        }

        public string ReferensiTransaksi
        {
            get
            {
                DateTime skrg = DateTime.Now;
                string soutreff = skoderef;
                soutreff += Convert.ToChar((skrg.Year - 2015) + 65);
                soutreff += Convert.ToChar(skrg.Month + 65);
                soutreff += skrg.Day.ToString("X02");
                soutreff += Convert.ToChar(skrg.Hour + 65);
                soutreff += skrg.Minute.ToString("X02");
                soutreff += skrg.Second.ToString("X02");

                return soutreff;
            }
            set
            {
                string stemp = value.Replace(" ", "").Trim();
                if (stemp.Length > 2)
                    stemp = stemp.Substring(0, 2);
                skoderef = value.ToUpper();
            }
        }

        public bool HasilTransaksi
        {
            get
            {
                return bsukses;
            }
            set
            {
                bsukses = false;
                if (!bool.TryParse(value.ToString(), out bsukses))
                    throw new Exception("Hasil transaksi hanya True atau False");
            }
        } 

        public string KeteranganError
        {
            get
            {
                return sketerangan;
            }
            set
            {
                if (value.Replace("  ", " ").Length > 100)
                    throw new Exception("Keterangan error maksimal 100 karakter");
                sketerangan = value.Replace("  ", " ").Trim();
            }
        }

        public string NominalTransaksi
        {
            get
            {
                return inominal.ToString();
            }
            set
            {
                inominal = 0;
                if (!Int64.TryParse(value.Replace(" ", "").Replace(".", ""), out inominal))
                    throw new Exception("Nilai nominal transaksi tidak valid");

            }
        }

        public string NominalCurrencyTransaksi
        {
            get
            {
                return inominal.ToString("#,0").Replace(",", ".");
            }
        }

        public string NominalSaldoAkhir
        {
            get
            {
                return isaldoakhir.ToString();
            }
            set
            {
                isaldoakhir = 0;
                if (!Int64.TryParse(value.Replace(" ", "").Replace(".", ""), out isaldoakhir))
                    throw new Exception("Saldo akhir transaksi tidak valid");
            }
        }

        public string NominalCurrencySaldoAkhir
        {
            get
            {
                return isaldoakhir.ToString("#,0").Replace(",", ".");
            }
        }
    }
    
}
