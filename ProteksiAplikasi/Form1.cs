﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.Win32;
using System.Management;
using System.IO;
using System.Security.Cryptography;

namespace ProteksiAplikasi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\aplikasiku");
            if (rktemp == null)
            {
                try
                {
                    rktemp = rk.CreateSubKey("SOFTWARE\\aplikasiku\\kiosk");
                    rktemp.SetValue("dp", "12345");
                    rktemp.SetValue("sn", "55555");

                    MessageBox.Show("Sukses");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                //rktemp = rk.OpenSubKey()
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\aplikasiku\\kiosk");
            if (rktemp == null)
            {
                MessageBox.Show("Ga ada");
            }
            else
            {
                MessageBox.Show(rktemp.GetValue("dp") + " " + rktemp.GetValue("sn"));
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
            foreach (ManagementObject mo in search.Get())
            {
                if (mo["InterfaceType"].ToString() != "USB")
                {
                    listBox1.Items.Add(mo["Model"].ToString());
                    listBox1.Items.Add(mo["InterfaceType"].ToString());
                    listBox1.Items.Add(mo["SerialNumber"].ToString());
                    label4.Text = mo["SerialNumber"].ToString();
                    listBox1.Items.Add(" ");
                }
            }

            listBox2.Items.Add("HDD SN : " + label4.Text);
        }

        private byte[] AESEncrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        private byte[] AESDecrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        private byte[] Rol(byte[] b)
        {
            byte[] r = new byte[b.Length];
            byte carry = 0;

            for (int i = b.Length - 1; i >= 0; i--)
            {
                ushort u = (ushort)(b[i] << 1);
                r[i] = (byte)((u & 0xff) + carry);
                carry = (byte)((u & 0xff00) >> 8);
            }

            return r;
        }

        private byte[] AESCMAC(byte[] key, byte[] data)
        {
            // SubKey generation
            // step 1, AES-128 with key K is applied to an all-zero input block.
            byte[] L = AESEncrypt(key, new byte[16], new byte[16]);

            // step 2, K1 is derived through the following operation:
            byte[] FirstSubkey = Rol(L); //If the most significant bit of L is equal to 0, K1 is the left-shift of L by 1 bit.
            if ((L[0] & 0x80) == 0x80)
                FirstSubkey[15] ^= 0x87; // Otherwise, K1 is the exclusive-OR of const_Rb and the left-shift of L by 1 bit.

            // step 3, K2 is derived through the following operation:
            byte[] SecondSubkey = Rol(FirstSubkey); // If the most significant bit of K1 is equal to 0, K2 is the left-shift of K1 by 1 bit.
            if ((FirstSubkey[0] & 0x80) == 0x80)
                SecondSubkey[15] ^= 0x87; // Otherwise, K2 is the exclusive-OR of const_Rb and the left-shift of K1 by 1 bit.

            // MAC computing
            if (((data.Length != 0) && (data.Length % 16 == 0)) == true)
            {
                // If the size of the input message block is equal to a positive multiple of the block size (namely, 128 bits),
                // the last block shall be exclusive-OR'ed with K1 before processing
                for (int j = 0; j < FirstSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= FirstSubkey[j];
            }
            else
            {
                // Otherwise, the last block shall be padded with 10^i
                byte[] padding = new byte[16 - data.Length % 16];
                padding[0] = 0x80;

                data = data.Concat<byte>(padding.AsEnumerable()).ToArray();

                // and exclusive-OR'ed with K2
                for (int j = 0; j < SecondSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= SecondSubkey[j];
            }

            // The result of the previous process will be the input of the last encryption.
            byte[] encResult = AESEncrypt(key, new byte[16], data);

            byte[] HashValue = new byte[16];
            Array.Copy(encResult, encResult.Length - HashValue.Length, HashValue, 0, HashValue.Length);

            return HashValue;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(label4.Text);
            byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata, 32);
            else
                Array.Copy(bdatatemp, bdata, bdatatemp.Length);

            byte[] bhasil = AESEncrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            string str = "";
            for (int i = 0; i < bhasil.Length; i++)
                str += bhasil[i].ToString("X02");

            label1.Text = str;

            listBox2.Items.Add("HDDSN enc : " + label1.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string stanggalsekarang = DateTime.Now.ToString("yyyy.MM/dd-HH:mm");
            byte[] bdata = ASCIIEncoding.ASCII.GetBytes(stanggalsekarang);
            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            string str = "";
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            textBox1.Text = str;

            listBox2.Items.Add("F Date enc : " + textBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label2.Text = textBox1.Text + " - " + label1.Text;

            listBox2.Items.Add("SN App : " + label2.Text);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            byte[] bdata = new byte[textBox1.Text.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(textBox1.Text.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            byte[] bori = AESDecrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            string stemp = ASCIIEncoding.ASCII.GetString(bori);
            label5.Text = stemp;

            listBox2.Items.Add("fdate ori : " + label5.Text);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            byte[] bdata = new byte[label1.Text.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(label1.Text.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            byte[] bori = AESDecrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            string stemp = ASCIIEncoding.ASCII.GetString(bori);
            label6.Text = stemp;

            listBox2.Items.Add("sn hdd ori : " + label6.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            byte[] bdata1 = ASCIIEncoding.ASCII.GetBytes(label5.Text);
            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(label6.Text);
            byte[] bdata2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata2, 32);
            else
                Array.Copy(bdatatemp, bdata2, bdatatemp.Length);

            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata1);
            string str = "";
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata2);
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            byte[] bhitung = ASCIIEncoding.ASCII.GetBytes(str);
            byte[] bmac = AESCMAC(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x33, 0xA2 }, bhitung);

            str = "";
            for (int i = 0; i < bmac.Length; i++)
                str += bmac[i].ToString("X02");

            label3.Text = str;

            listBox2.Items.Add("Actv Key : " + label3.Text);
        }

        // cek dulu
        private int CekFirstReg()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
                if (rktemp == null)
                    return 123;

                if (((string)rktemp.GetValue("dp") == "") || ((string)rktemp.GetValue("sn") == ""))
                    return 123;

                if (((string)rktemp.GetValue("dp") != "") && ((string)rktemp.GetValue("sn") != "") && ((string)rktemp.GetValue("ak") == ""))
                    return 3;

                if (((string)rktemp.GetValue("dp") != "") && ((string)rktemp.GetValue("sn") != "") && ((string)rktemp.GetValue("ak") != ""))
                    return 0;

                return 123;
            }
            catch
            {
                return 123;
            }
        }
        // kalo ga ada -- 123 .. bikin baru.. kluarkan serial number
        private string InjectReg()
        {
            string spd = "";
            string sn = "";

            string stempsn = "";
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
            foreach (ManagementObject mo in search.Get())
            {
                if (mo["InterfaceType"].ToString() != "USB")
                    stempsn = mo["SerialNumber"].ToString();
            }

            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
            byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata, 32);
            else
                Array.Copy(bdatatemp, bdata, bdatatemp.Length);

            byte[] bhasil = AESEncrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            sn = "";
            for (int i = 0; i < bhasil.Length; i++)
                sn += bhasil[i].ToString("X02");

            string stanggalsekarang = DateTime.Now.ToString("yyyy.MM/dd-HH:mm");
            bdata = ASCIIEncoding.ASCII.GetBytes(stanggalsekarang);
            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            spd = "";
            for (int i = 0; i < benc.Length; i++)
                spd += benc[i].ToString("X02");

            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.CreateSubKey("SOFTWARE\\eBJPST\\kiosk");
            rktemp.SetValue("dp", spd);
            rktemp.SetValue("sn", sn);
            rktemp.SetValue("ak", "");

            return spd + " - " + sn;  // out SN aplikasi
        }
        // kalo kunci aktivasi saja yg blm ada
        private bool ValidateValueSN()
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
            string ssn = (string)rktemp.GetValue("sn");
            // cek ssn
            string stempsn = "";
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
            foreach (ManagementObject mo in search.Get())
            {
                if (mo["InterfaceType"].ToString() != "USB")
                    stempsn = mo["SerialNumber"].ToString();
            }

            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
            byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata, 32);
            else
                Array.Copy(bdatatemp, bdata, bdatatemp.Length);

            byte[] bhasil = AESEncrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            stempsn = "";
            for (int i = 0; i < bhasil.Length; i++)
                stempsn += bhasil[i].ToString("X02");

            if (ssn != stempsn)     // SN tersimpan tidak valid
                return false;
            else
                return true;
        }
        // masukkan kunci aktivasi
        private bool ValidateNinsertActKey(string sActivationKey)
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
            string sdp = (string)rktemp.GetValue("dp");
            string ssn = (string)rktemp.GetValue("sn");

            // dekrip dulu
            byte[] bdata = new byte[sdp.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(sdp.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            byte[] bori = AESDecrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            string stemp = ASCIIEncoding.ASCII.GetString(bori);
            sdp = stemp;

            bdata = new byte[ssn.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(ssn.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            bori = AESDecrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            stemp = ASCIIEncoding.ASCII.GetString(bori);
            ssn = stemp;
            // hitung actv key
            byte[] bdata1 = ASCIIEncoding.ASCII.GetBytes(sdp);
            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(ssn);
            byte[] bdata2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata2, 32);
            else
                Array.Copy(bdatatemp, bdata2, bdatatemp.Length);

            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata1);
            string str = "";
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata2);
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            byte[] bhitung = ASCIIEncoding.ASCII.GetBytes(str);
            byte[] bmac = AESCMAC(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x33, 0xA2 }, bhitung);

            str = "";
            for (int i = 0; i < bmac.Length; i++)
                str += bmac[i].ToString("X02");
            // compare
            if (str == sActivationKey)
            {
                rktemp.SetValue("ak", str);
                return true;
            }
            else
                return false;
        }
        // validasi nilai yg tersimpan -- error : 0 .. kalo ga sama false, kalo sama true
        private bool CekValueRegNActKey()
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
            string sdp = (string)rktemp.GetValue("dp");
            string ssn = (string)rktemp.GetValue("sn");
            string sak = (string)rktemp.GetValue("ak");
            // cek ssn
            string stempsn = "";
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
            foreach (ManagementObject mo in search.Get())
            {
                if (mo["InterfaceType"].ToString() != "USB")
                    stempsn = mo["SerialNumber"].ToString();
            }

            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
            byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata, 32);
            else
                Array.Copy(bdatatemp, bdata, bdatatemp.Length);

            byte[] bhasil = AESEncrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            stempsn = "";
            for (int i = 0; i < bhasil.Length; i++)
                stempsn += bhasil[i].ToString("X02");

            if (ssn != stempsn)     // SN tersimpan tidak valid
                return false;

            // dekrip dulu
            bdata = new byte[sdp.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(textBox1.Text.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            byte[] bori = AESDecrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            string stemp = ASCIIEncoding.ASCII.GetString(bori);
            sdp = stemp;

            bdata = new byte[ssn.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(label1.Text.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            bori = AESDecrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            stemp = ASCIIEncoding.ASCII.GetString(bori);
            ssn = stemp;
            // hitung actv key
            byte[] bdata1 = ASCIIEncoding.ASCII.GetBytes(sdp);
            bdatatemp = ASCIIEncoding.ASCII.GetBytes(ssn);
            byte[] bdata2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata2, 32);
            else
                Array.Copy(bdatatemp, bdata2, bdatatemp.Length);

            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata1);
            string str = "";
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata2);
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            byte[] bhitung = ASCIIEncoding.ASCII.GetBytes(str);
            byte[] bmac = AESCMAC(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x33, 0xA2 }, bhitung);

            str = "";
            for (int i = 0; i < bmac.Length; i++)
                str += bmac[i].ToString("X02");
            // compare
            if (str == sak)
                return true;
            else
                return false;
        }
        // generate kunci aktivasi -- sudah OK
        private string GenerateActKey(string SN)
        {
            string[] ssnapp = SN.Replace(" ", "").Split('-');

            // dekrip first date
            byte[] bdata = new byte[ssnapp[0].Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(ssnapp[0].Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            byte[] bori = AESDecrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            string sdp = ASCIIEncoding.ASCII.GetString(bori);

            // dekrip sn hdd
            bdata = new byte[ssnapp[1].Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(ssnapp[1].Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            bori = AESDecrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            string stemp = ASCIIEncoding.ASCII.GetString(bori);
            string shsn = stemp;

            byte[] bdata1 = ASCIIEncoding.ASCII.GetBytes(sdp);
            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(shsn);
            byte[] bdata2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata2, 32);
            else
                Array.Copy(bdatatemp, bdata2, bdatatemp.Length);

            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata1);
            string str = "";
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata2);
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            byte[] bhitung = ASCIIEncoding.ASCII.GetBytes(str);
            byte[] bmac = AESCMAC(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x33, 0xA2 }, bhitung);

            str = "";
            for (int i = 0; i < bmac.Length; i++)
                str += bmac[i].ToString("X02");

            return str;
        }

        private bool CekReg(out string spd, out string sn)
        {
            spd = "";
            sn = "";

            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\aplikasiku\\kiosk");
            if (rktemp == null)
            {
                // create baru

                MessageBox.Show("Ga ada");
            }
            else
            {
                MessageBox.Show(rktemp.GetValue("dp") + " " + rktemp.GetValue("sn"));
            }


            string stempsn = "";
            string stemppd = "";
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
            foreach (ManagementObject mo in search.Get())
            {
                if (mo["InterfaceType"].ToString() != "USB")
                    stempsn = mo["SerialNumber"].ToString();
            }
            stemppd = DateTime.Now.ToString("yyyy.MM/dd-HH:mm");



            return true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            System.IO.StreamWriter sw = System.IO.File.CreateText("debug.txt");
            for (int i = 0; i < listBox2.Items.Count; i++)
                sw.WriteLine(listBox2.Items[i].ToString());
            sw.Close();

            listBox2.Items.Clear();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox3.Text = GenerateActKey(textBox2.Text);
        }
    }
}
