﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace DemoRetribusiKapal
{
    public partial class PersoDemoAplikasi : Form
    {
        public PersoDemoAplikasi()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            throw new NotImplementedException();
        }

        #region SmartCard Comm Properties
        private struct SCARD_IO_REQUEST
        {
            public int dwProtocol;
            public int cbPciLength;
        }

        [DllImport("winscard.dll")]
        private static extern int SCardEstablishContext(int dwScope, int pvReserved1, int pvReserved2, ref int phContext);

        [DllImport("winscard.dll")]
        private static extern int SCardReleaseContext(int phContext);

        [DllImport("winscard.dll")]
        private static extern int SCardConnect(int hContext, string szReaderName, int dwShareMode, int dwPrefProtocol, ref int phCard, ref int ActiveProtocol);

        [DllImport("winscard.dll")]
        private static extern int SCardDisconnect(int hCard, int Disposition);

        [DllImport("winscard.DLL", EntryPoint = "SCardListReadersA", CharSet = CharSet.Ansi)]
        private static extern int SCardListReaders(int hContext, byte[] Groups, byte[] Readers, ref int pcchReaders);

        [DllImport("winscard.dll")]
        private static extern int SCardStatus(int hCard, string szReaderName, ref int pcchReaderLen, ref int State, ref int Protocol, ref byte ATR, ref int ATRLen);

        [DllImport("winscard.dll")]
        private static extern int SCardTransmit(int hCard, ref SCARD_IO_REQUEST pioSendRequest, ref byte SendBuff, int SendBuffLen, ref SCARD_IO_REQUEST pioRecvRequest, ref byte RecvBuff, ref int RecvBuffLen);

        private int retCode, hContext, hCard, Protocol;
        SCARD_IO_REQUEST pioSendRequest;
        private byte[] SendBuff = new byte[256];
        private byte[] RecvBuff = new byte[256];
        private int SendLen = 0;
        private int RecvLen = 255;

        private string _stringNama;
        private string _stringAlamat;
        private string _stringNIM;
        private string _stringNoKTP;
        private string _stringKunciNama;
        private string _stringKunciAlamat;
        private string _stringKunciNIM_KTP;
        private string _stringKunci;
        private string _stringKunciBaru;

        private string _stringKunciDataPenjualan;

        private int SendCommand()
        {
            pioSendRequest.dwProtocol = Protocol;
            pioSendRequest.cbPciLength = 8;
            RecvLen = 255;
            RecvBuff[0] = 0;
            RecvBuff[1] = 0;
            retCode = SCardTransmit(hCard, ref pioSendRequest, ref SendBuff[0], SendLen, ref pioSendRequest, ref RecvBuff[0], ref RecvLen);

            if (retCode != 0)
                return retCode;

            return 0;
        }

        #endregion

        private bool LoadKey(byte[] bKey)
        {
            SendBuff[0] = 0xFF;		//CLA
            SendBuff[1] = 0x82;		//INS
            SendBuff[2] = 0x20;		//P1 : Non volatile memory
            SendBuff[3] = 0x00;		//P2 : memory location
            SendBuff[4] = 0x06;		//P3
            SendBuff[5] = bKey[0];		//k
            SendBuff[6] = bKey[1];		//k
            SendBuff[7] = bKey[2];		//k
            SendBuff[8] = bKey[3];		//k
            SendBuff[9] = bKey[4];		//k
            SendBuff[10] = bKey[5];		//k
            SendLen = 11;

            if (SendCommand() == 0)
            {
                if ((RecvBuff[RecvLen - 2] != 0x90) && (RecvBuff[RecvLen - 1] != 0x00))
                    return false;
                else
                    return true;
            }

            return false;
        }

        private bool AuthCard(byte bBlok)
        {
            SendBuff[0] = 0xFF;
            SendBuff[1] = 0x88;
            SendBuff[2] = 0x00;
            SendBuff[3] = bBlok;	    // blok number    					
            SendBuff[4] = 0x60;         // key type	= Key A					
            SendBuff[5] = 0x00;	        // key number -- memory location					
            SendLen = 6;

            if (SendCommand() == 0)
            {
                if ((RecvBuff[RecvLen - 2] == 0x90) && (RecvBuff[RecvLen - 1] == 0))
                    return true;
                else
                    return false;
            }
            return false;
        }

        private bool ReadCard(byte bBlok, ref byte[] sDataRead)
        {
            SendBuff[0] = 0xFF;		//CLA
            SendBuff[1] = 0xB0;		//INS
            SendBuff[2] = 0x00;		//P1 : 
            SendBuff[3] = bBlok;	//blok number 
            SendBuff[4] = 0x10;		//P3 --- baca tiap 16 byte 
            SendLen = 5;

            if (SendCommand() == 0)
            {
                if ((RecvBuff[RecvLen - 2] == 0x90) && (RecvBuff[RecvLen - 1] == 0))
                {
                    for (int i = 0; i < RecvLen - 2; i++)
                        sDataRead[i] = RecvBuff[i];
                    //if (RecvBuff[i] != 0)
                    //    sDataRead = sDataRead + ASCIIEncoding.ASCII.GetString(RecvBuff, i, 1);

                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        private bool WriteCard(byte bBlok, byte[] bDataWrite)
        {
            SendBuff[0] = 0xFF;			//CLA
            SendBuff[1] = 0xD6;			//INS
            SendBuff[2] = 0x00;			//P1 : 
            SendBuff[3] = bBlok;
            SendBuff[4] = 0x10;	        //P3

            for (int i = 0; i < 16; i++)
                //SendBuff[i + 5] = Convert.ToByte(sDataWrite[i]);
                SendBuff[i + 5] = bDataWrite[i];

            SendLen = 21;

            if (SendCommand() == 0)
            {
                if ((RecvBuff[RecvLen - 2] == 0x90) && (RecvBuff[RecvLen - 1] == 0))
                    return true;
                else
                    return false;
            }
            return false;
        }

        private bool ChangeKey(byte bBlok, byte[] bKeyHexa)
        {
            SendBuff[0] = 0xFF;			//CLA
            SendBuff[1] = 0xD6;			//INS
            SendBuff[2] = 0x00;			//P1 : 
            SendBuff[3] = bBlok;
            SendBuff[4] = 0x10;	        //P3

            for (int i = 0; i < 6; i++)
                SendBuff[i + 5] = bKeyHexa[i];
            //FF078069FFFFFFFFFFFF
            SendBuff[11] = 0xFF;
            SendBuff[12] = 0x07;
            SendBuff[13] = 0x80;
            SendBuff[14] = 0x69;
            SendBuff[15] = 0xFF;
            SendBuff[16] = 0xFF;
            SendBuff[17] = 0xFF;
            SendBuff[18] = 0xFF;
            SendBuff[19] = 0xFF;
            SendBuff[20] = 0xFF;
            SendLen = 21;

            if (SendCommand() == 0)
            {
                if ((RecvBuff[RecvLen - 2] == 0x90) && (RecvBuff[RecvLen - 1] == 0))
                    return true;
                else
                    return false;
            }
            return false;
        }

        private void PersoDemoAplikasi_Load(object sender, EventArgs e)
        {
            // establish context
            if (SCardEstablishContext(0, 0, 0, ref hContext) != 0)
            {
                MessageBox.Show("Koneksi Reader Error");
                this.Close();
            }
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
            WriteKartu();
        }

       

        

        private void button3_Click(object sender, EventArgs e)
        {
            ReadKartu();
        }

        private void ReadKartu()
        {
            tbNamaKapal.Text = "";
            tbNamaNasabah.Text = "";
            tbDaerahPenangkapan.Text = "";
            tbJenisKapal.Text = "";
            tbAlatTangkap.Text = "";
            textBox6.Text = "";
            tbSPB.Text = "";
            // connect card
            if (SCardConnect(hContext, "OMNIKEY CardMan 5x21-CL 0", 2, 1 | 2, ref hCard, ref Protocol) != 0)
            {
                MessageBox.Show("Tidak ada kartu");
                return;
            }
            // load key
            if (!LoadKey(new byte[6] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }))
            {
                MessageBox.Show("Kartu tidak dapat dibaca");
                return;
            }

            // ============================= read nama Kapal =============================
            if (!AuthCard(1))
            {
                MessageBox.Show("Auth Nama Kapal error");
                return;
            }

            byte[] btemp = new byte[16];
            string stemp = "";
            if (!ReadCard(1, ref btemp))
            {
                MessageBox.Show("Read Nama Kapal error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(2, ref btemp))
            {
                MessageBox.Show("Read Nama Kapal error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;

            tbNamaKapal.Text = stemp;

            // ================== Read Nama Pemilik ==========================

            btemp = new byte[16];
            stemp = "";
            if (!AuthCard(4))
            {
                MessageBox.Show("Auth Nama Pemilik error");
                return;
            }

            if (!ReadCard(4, ref btemp))
            {
                MessageBox.Show("Read Nama Pemilik error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;

            if (!ReadCard(5, ref btemp))
            {
                MessageBox.Show("Read Nama Pemilik error");
                return;
            }

            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;

            tbNamaNasabah.Text = stemp;

            // ===================== Read Daerah Penangkapan ==============

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(6, ref btemp))
            {
                MessageBox.Show("Read Daerah Penangkapan");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;

            if (!AuthCard(8))
            {
                MessageBox.Show("Auth Daerah Penangkapan error");
                return;
            }

            if (!ReadCard(8, ref btemp))
            {
                MessageBox.Show("Read Daerah Penangkapan error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;

            tbDaerahPenangkapan.Text = stemp;

            // ===================== Read Jenis Kapal ==============
            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(9, ref btemp))
            {
                MessageBox.Show("Read Jenis Kapal");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;

            tbJenisKapal.Text = stemp;

            // ===================== Alat Tangkap =================
            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(10, ref btemp))
            {
                MessageBox.Show("Read Alat Tangkap");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;

            tbAlatTangkap.Text = stemp;

            // ====================== Masa Berlaku ============================
            if (!AuthCard(12))
            {
                MessageBox.Show("Auth Masa Berlaku");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(12, ref btemp))
            {
                MessageBox.Show("Read Masa Berlaku");
                return;
            }

            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            comboBox1.Text = stemp[0] + "" + stemp[1];
            comboBox2.Text = stemp[2] + "" + stemp[3];
            textBox6.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];

            // ============================= read KUPP =============================
            if (!AuthCard(16))
            {
                MessageBox.Show("Auth KUPP error");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(16, ref btemp))
            {
                MessageBox.Show("Read KUPP error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(17, ref btemp))
            {
                MessageBox.Show("Read KUPP error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            tbKUPP.Text = stemp;

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(18, ref btemp))
            {
                MessageBox.Show("Read tanggal KUPP error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            cb1.Text = stemp[0] + "" + stemp[1];
            cb2.Text = stemp[2] + "" + stemp[3];
            tb1.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];
            cb3.Text = stemp[8] + "" + stemp[9];
            cb4.Text = stemp[10] + "" + stemp[11];
            tb2.Text = stemp[12] + "" + stemp[13] + stemp[14] + stemp[15];

            // ============================= read SPB =============================
            if (!AuthCard(20))
            {
                MessageBox.Show("Auth SPB error");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(20, ref btemp))
            {
                MessageBox.Show("Read SPB error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(21, ref btemp))
            {
                MessageBox.Show("Read SPB error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            tbSPB.Text = stemp;

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(22, ref btemp))
            {
                MessageBox.Show("Read tanggal SPB error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            cb5.Text = stemp[0] + "" + stemp[1];
            cb6.Text = stemp[2] + "" + stemp[3];
            tb3.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];
            cb7.Text = stemp[8] + "" + stemp[9];
            cb8.Text = stemp[10] + "" + stemp[11];
            tb4.Text = stemp[12] + "" + stemp[13] + stemp[14] + stemp[15];

            // ============================= read SIUP =============================
            if (!AuthCard(24))
            {
                MessageBox.Show("Auth SIUP error");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(24, ref btemp))
            {
                MessageBox.Show("Read SIUP error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(25, ref btemp))
            {
                MessageBox.Show("Read SIUP error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            tbSIUP.Text = stemp;

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(26, ref btemp))
            {
                MessageBox.Show("Read tanggal SIUP error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            cb9.Text = stemp[0] + "" + stemp[1];
            cb10.Text = stemp[2] + "" + stemp[3];
            tb5.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];
            cb11.Text = stemp[8] + "" + stemp[9];
            cb12.Text = stemp[10] + "" + stemp[11];
            tb6.Text = stemp[12] + "" + stemp[13] + stemp[14] + stemp[15];

            // ============================= read SIPI =============================
            if (!AuthCard(28))
            {
                MessageBox.Show("Auth SIPI error");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(28, ref btemp))
            {
                MessageBox.Show("Read SIPI error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(29, ref btemp))
            {
                MessageBox.Show("Read SIPI error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            tbSIPI.Text = stemp;

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(30, ref btemp))
            {
                MessageBox.Show("Read tanggal SIPI error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            cb13.Text = stemp[0] + "" + stemp[1];
            cb14.Text = stemp[2] + "" + stemp[3];
            tb7.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];
            cb15.Text = stemp[8] + "" + stemp[9];
            cb16.Text = stemp[10] + "" + stemp[11];
            tb8.Text = stemp[12] + "" + stemp[13] + stemp[14] + stemp[15];

            // ============================= read Kesehatan =============================
            if (!AuthCard(32))
            {
                MessageBox.Show("Auth Kesehatan error");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(32, ref btemp))
            {
                MessageBox.Show("Read Kesehatan error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(33, ref btemp))
            {
                MessageBox.Show("Read Kesehatan error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            tbKesehatan.Text = stemp;

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(34, ref btemp))
            {
                MessageBox.Show("Read tanggal Kesehatan error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            cb17.Text = stemp[0] + "" + stemp[1];
            cb18.Text = stemp[2] + "" + stemp[3];
            tb9.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];
            cb19.Text = stemp[8] + "" + stemp[9];
            cb20.Text = stemp[10] + "" + stemp[11];
            tb10.Text = stemp[12] + "" + stemp[13] + stemp[14] + stemp[15];

            // ============================= read SLO =============================
            if (!AuthCard(36))
            {
                MessageBox.Show("Auth SLO error");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(36, ref btemp))
            {
                MessageBox.Show("Read SLO error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(37, ref btemp))
            {
                MessageBox.Show("Read SLO error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            tbSLO.Text = stemp;

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(38, ref btemp))
            {
                MessageBox.Show("Read tanggal SLO error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            cb21.Text = stemp[0] + "" + stemp[1];
            cb22.Text = stemp[2] + "" + stemp[3];
            tb11.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];
            cb23.Text = stemp[8] + "" + stemp[9];
            cb24.Text = stemp[10] + "" + stemp[11];
            tb12.Text = stemp[12] + "" + stemp[13] + stemp[14] + stemp[15];

            // ============================= read Rek ukur ulang P3 =============================
            if (!AuthCard(40))
            {
                MessageBox.Show("Auth Rek ukur ulang P3 error");
                return;
            }

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(40, ref btemp))
            {
                MessageBox.Show("Read Rek ukur ulang P3 error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            if (!ReadCard(41, ref btemp))
            {
                MessageBox.Show("Read Rek ukur ulang P3 error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            tbRUUP3.Text = stemp;

            btemp = new byte[16];
            stemp = "";
            if (!ReadCard(42, ref btemp))
            {
                MessageBox.Show("Read tanggal Rek ukur ulang P3 error");
                return;
            }
            for (int i = 0; i < 16; i++)
                if (btemp[i] != 0xFF)
                    stemp = stemp + ASCIIEncoding.ASCII.GetString(btemp, i, 1);
                else
                    break;
            cb25.Text = stemp[0] + "" + stemp[1];
            cb26.Text = stemp[2] + "" + stemp[3];
            tb13.Text = stemp[4] + "" + stemp[5] + stemp[6] + stemp[7];
            cb27.Text = stemp[8] + "" + stemp[9];
            cb28.Text = stemp[10] + "" + stemp[11];
            tb14.Text = stemp[12] + "" + stemp[13] + stemp[14] + stemp[15];


            MessageBox.Show("Read Success");
        }

        private void WriteKartu()
        {
            // connect card
            if (SCardConnect(hContext, "OMNIKEY CardMan 5x21-CL 0", 2, 1 | 2, ref hCard, ref Protocol) != 0)
            {
                MessageBox.Show("Tidak ada kartu");
                return;
            }

            // load key
            if (!LoadKey(new byte[6] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }))
            {
                MessageBox.Show("Kartu tidak dapat dibaca");
                return;
            }

            string stemp = "";
            byte[] btemp = new byte[16];
            byte[] bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

            //========================= write nama kapal =================================== 
            if (!AuthCard(1))
            {
                MessageBox.Show("Auth Nama Kapal error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbNamaKapal.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(1, bwrite))
                {
                    MessageBox.Show("Write nama error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(2, bwrite))
                {
                    MessageBox.Show("Write nama error");
                    return;
                }
            }

            //============= write Nama Pemilik ====================
            if (!AuthCard(4))
            {
                MessageBox.Show("Auth Nama Pemilik error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbNamaNasabah.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(4, bwrite))
                {
                    MessageBox.Show("Write Nama Pemilik error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];

                if (!WriteCard(5, bwrite))
                {
                    MessageBox.Show("Write Nama Pemilik error");
                    return;
                }
            }

            // =================== Write Daerah Penangkapan ===================

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbDaerahPenangkapan.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(6, bwrite))
                {
                    MessageBox.Show("Write Daerah Penangkapan error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];

                if (!AuthCard(8))
                {
                    MessageBox.Show("Auth Nama Pemilik error");
                    return;
                }
                if (!WriteCard(8, bwrite))
                {
                    MessageBox.Show("Write Nama Pemilik error");
                    return;
                }
            }

            //================ Write Jenis Kapal ================
            stemp = tbJenisKapal.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(9, bwrite))
            {
                MessageBox.Show("Write Jenis Kapal error");
                return;
            }

            //================ Write Alat Tangkap ======================
            stemp = tbAlatTangkap.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(10, bwrite))
            {
                MessageBox.Show("Write Alat Tangkap error");
                return;
            }

            // ======================== Write Masa Berlaku ==================
            if (!AuthCard(12))
            {
                MessageBox.Show("Auth Masa Berlaku error");
                return;
            }
            stemp = comboBox1.Text + comboBox2.Text + textBox6.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(12, bwrite))
            {
                MessageBox.Show("Write Masa Berlaku error");
                return;
            }

            // ======================== Write KUPP ==================
            if (!AuthCard(16))
            {
                MessageBox.Show("Auth KUPP error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbKUPP.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(16, bwrite))
                {
                    MessageBox.Show("Write KUPP error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(17, bwrite))
                {
                    MessageBox.Show("Write KUPP error");
                    return;
                }

            }
            stemp = cb1.Text + cb2.Text + tb1.Text + cb3.Text + cb4.Text + tb2.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(18, bwrite))
            {
                MessageBox.Show("Write tanggal KUPP error");
                return;
            }

            // ======================== Write SPB ==================
            if (!AuthCard(20))
            {
                MessageBox.Show("Auth SPB error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbSPB.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(20, bwrite))
                {
                    MessageBox.Show("Write SPB error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(21, bwrite))
                {
                    MessageBox.Show("Write SPB error");
                    return;
                }

            }
            stemp = cb5.Text + cb6.Text + tb3.Text + cb7.Text + cb8.Text + tb4.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(22, bwrite))
            {
                MessageBox.Show("Write tanggal SPB error");
                return;
            }

            // ======================== Write SIUP ==================
            if (!AuthCard(24))
            {
                MessageBox.Show("Auth SPB error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbSIUP.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(24, bwrite))
                {
                    MessageBox.Show("Write SIUP error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(25, bwrite))
                {
                    MessageBox.Show("Write SIUP error");
                    return;
                }

            }
            stemp = cb9.Text + cb10.Text + tb5.Text + cb11.Text + cb12.Text + tb6.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(26, bwrite))
            {
                MessageBox.Show("Write tanggal SIUP error");
                return;
            }

            // ======================== Write SIPI ==================
            if (!AuthCard(28))
            {
                MessageBox.Show("Auth SIPI error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbSIPI.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(28, bwrite))
                {
                    MessageBox.Show("Write SIPI error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(29, bwrite))
                {
                    MessageBox.Show("Write SIPI error");
                    return;
                }
            }
            stemp = cb13.Text + cb14.Text + tb7.Text + cb15.Text + cb16.Text + tb8.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(30, bwrite))
            {
                MessageBox.Show("Write tanggal SIPI error");
                return;
            }

            // ======================== Write Kesehatan ==================
            if (!AuthCard(32))
            {
                MessageBox.Show("Auth Kesehatan error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbKesehatan.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(32, bwrite))
                {
                    MessageBox.Show("Write Kesehatan error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(33, bwrite))
                {
                    MessageBox.Show("Write Kesehatan error");
                    return;
                }
            }
            stemp = cb17.Text + cb18.Text + tb9.Text + cb19.Text + cb20.Text + tb10.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(34, bwrite))
            {
                MessageBox.Show("Write tanggal Kesehatan error");
                return;
            }

            // ======================== Write SLO ==================
            if (!AuthCard(36))
            {
                MessageBox.Show("Auth SLO error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbSLO.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(36, bwrite))
                {
                    MessageBox.Show("Write SLO error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(37, bwrite))
                {
                    MessageBox.Show("Write SLO error");
                    return;
                }
            }
            stemp = cb21.Text + cb22.Text + tb11.Text + cb23.Text + cb24.Text + tb12.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(38, bwrite))
            {
                MessageBox.Show("Write Tanggal SLO error");
                return;
            }

            // ======================== Write RekP3 ==================
            if (!AuthCard(40))
            {
                MessageBox.Show("Auth SLO error");
                return;
            }

            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            stemp = tbRUUP3.Text;
            btemp = ASCIIEncoding.ASCII.GetBytes(stemp);

            if (btemp.Length > 0)
            {
                if (btemp.Length <= 16)
                    for (int i = 0; i < btemp.Length; i++)
                        bwrite[i] = btemp[i];
                else if (btemp.Length > 16)
                    for (int i = 0; i < 16; i++)
                        bwrite[i] = btemp[i];

                if (!WriteCard(40, bwrite))
                {
                    MessageBox.Show("Write Rek Ukur Ulang P3 error");
                    return;
                }

                bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
                if ((btemp.Length > 16) && (btemp.Length <= 32))
                    for (int i = 16; i < btemp.Length; i++)
                        bwrite[i - 16] = btemp[i];
                else if ((btemp.Length > 16) && (btemp.Length > 32))
                    for (int i = 16; i < 32; i++)
                        bwrite[i - 16] = btemp[i];
                if (!WriteCard(41, bwrite))
                {
                    MessageBox.Show("Write Rek Ukur Ulang P3 error");
                    return;
                }
            }
            stemp = cb25.Text + cb26.Text + tb13.Text + cb27.Text + cb28.Text + tb14.Text;
            bwrite = new byte[16] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Array.Copy(ASCIIEncoding.ASCII.GetBytes(stemp), 0, bwrite, 0, stemp.Length);
            if (!WriteCard(42, bwrite))
            {
                MessageBox.Show("Write Tanggal Rek Ukur Ulang P3 error");
                return;
            }
            MessageBox.Show("Write Sukses");
        }

    }
}
