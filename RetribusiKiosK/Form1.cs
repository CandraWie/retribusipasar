﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using libCard;
using LibSmartCard;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using System.Net;
using System.Xml;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.IO;
using System.Management;

namespace RetribusiKiosK
{
    public partial class Form1 : Form
    {
        string sApp = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        string imei = "";
        string uid = "";
        string hp = "";
        string vacc = "";
        string idagent = "";
        string idterminal = "";
        string idmerchant = "";
        string url = "";
        string port = "";
        string sidkios = "";

        private byte[] AESEncrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        private byte[] AESDecrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        private byte[] Rol(byte[] b)
        {
            byte[] r = new byte[b.Length];
            byte carry = 0;

            for (int i = b.Length - 1; i >= 0; i--)
            {
                ushort u = (ushort)(b[i] << 1);
                r[i] = (byte)((u & 0xff) + carry);
                carry = (byte)((u & 0xff00) >> 8);
            }

            return r;
        }

        private byte[] AESCMAC(byte[] key, byte[] data)
        {
            byte[] L = AESEncrypt(key, new byte[16], new byte[16]);
            byte[] FirstSubkey = Rol(L);
            if ((L[0] & 0x80) == 0x80)
                FirstSubkey[15] ^= 0x87;
            byte[] SecondSubkey = Rol(FirstSubkey);
            if ((FirstSubkey[0] & 0x80) == 0x80)
                SecondSubkey[15] ^= 0x87;
            if (((data.Length != 0) && (data.Length % 16 == 0)) == true)
            {
                for (int j = 0; j < FirstSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= FirstSubkey[j];
            }
            else
            {
                byte[] padding = new byte[16 - data.Length % 16];
                padding[0] = 0x80;

                data = data.Concat<byte>(padding.AsEnumerable()).ToArray();

                for (int j = 0; j < SecondSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= SecondSubkey[j];
            }

            byte[] encResult = AESEncrypt(key, new byte[16], data);

            byte[] HashValue = new byte[16];
            Array.Copy(encResult, encResult.Length - HashValue.Length, HashValue, 0, HashValue.Length);

            return HashValue;
        }

        // cek dulu
        private int CekFirstReg()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
                if (rktemp == null)
                    return 123;

                if (((string)rktemp.GetValue("dp") == "") || ((string)rktemp.GetValue("sn") == ""))
                    return 123;

                if (((string)rktemp.GetValue("dp") != "") && ((string)rktemp.GetValue("sn") != "") && ((string)rktemp.GetValue("ak") == ""))
                    return 3;

                if (((string)rktemp.GetValue("dp") != "") && ((string)rktemp.GetValue("sn") != "") && ((string)rktemp.GetValue("ak") != ""))
                    return 0;

                return 123;
            }
            catch
            {
                return 123;
            }
        }
        // kalo ga ada -- 123 .. bikin baru.. kluarkan serial number
        private string InjectReg()
        {
            string spd = "";
            string sn = "";

            string stempsn = "";
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
            foreach (ManagementObject mo in search.Get())
            {
                if (mo["InterfaceType"].ToString() != "USB")
                {
                    string stemp1 = "", stemp2 = "";
                    try { stemp1 = mo["SerialNumber"].ToString(); } catch { };
                    try { stemp2 = mo["Model"].ToString(); } catch { };
                    stempsn = stemp1 + stemp2;
                }
            }

            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
            byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata, 32);
            else
                Array.Copy(bdatatemp, bdata, bdatatemp.Length);

            byte[] bhasil = AESEncrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            sn = "";
            for (int i = 0; i < bhasil.Length; i++)
                sn += bhasil[i].ToString("X02");

            string stanggalsekarang = DateTime.Now.ToString("yyyy.MM/dd-HH:mm");//"2016.09/17-23:17";
            bdata = ASCIIEncoding.ASCII.GetBytes(stanggalsekarang);
            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            spd = "";
            for (int i = 0; i < benc.Length; i++)
                spd += benc[i].ToString("X02");

            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.CreateSubKey("SOFTWARE\\eBJPST\\kiosk");
            rktemp.SetValue("dp", spd);
            rktemp.SetValue("sn", sn);
            rktemp.SetValue("ak", "");

            return spd + " - " + sn;  // out SN aplikasi
        }
        // kalo kunci aktivasi saja yg blm ada
        private bool ValidateValueSN()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
                string ssn = (string)rktemp.GetValue("sn");
                // cek ssn
                string stempsn = "";
                ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
                foreach (ManagementObject mo in search.Get())
                {
                    if (mo["InterfaceType"].ToString() != "USB")
                    {
                        string stemp1 = "", stemp2 = "";
                        try { stemp1 = mo["SerialNumber"].ToString(); } catch { };
                        try { stemp2 = mo["Model"].ToString(); } catch { };
                        stempsn = stemp1 + stemp2;
                    }
                }

                byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
                byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                if (bdatatemp.Length >= 32)
                    Array.Copy(bdatatemp, bdata, 32);
                else
                    Array.Copy(bdatatemp, bdata, bdatatemp.Length);

                byte[] bhasil = AESEncrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

                stempsn = "";
                for (int i = 0; i < bhasil.Length; i++)
                    stempsn += bhasil[i].ToString("X02");

                if (ssn != stempsn)     // SN tersimpan tidak valid
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }
        // validasi nilai yg tersimpan -- error : 0 .. kalo ga sama false, kalo sama true
        private bool CekValueRegNActKey()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
                string sdp = (string)rktemp.GetValue("dp");
                string ssn = (string)rktemp.GetValue("sn");
                string sak = (string)rktemp.GetValue("ak");
                // cek ssn
                string stempsn = "";
                ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
                foreach (ManagementObject mo in search.Get())
                {
                    if (mo["InterfaceType"].ToString() != "USB")
                    {
                        string stemp1 = "", stemp2 = "";
                        try { stemp1 = mo["SerialNumber"].ToString(); } catch { };
                        try { stemp2 = mo["Model"].ToString(); } catch { };
                        stempsn = stemp1 + stemp2;
                    }
                }

                byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
                byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                if (bdatatemp.Length >= 32)
                    Array.Copy(bdatatemp, bdata, 32);
                else
                    Array.Copy(bdatatemp, bdata, bdatatemp.Length);

                byte[] bhasil = AESEncrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

                stempsn = "";
                for (int i = 0; i < bhasil.Length; i++)
                    stempsn += bhasil[i].ToString("X02");

                if (ssn != stempsn)     // SN tersimpan tidak valid
                    return false;

                // dekrip dulu
                bdata = new byte[sdp.Length / 2];
                for (int i = 0; i < bdata.Length; i++)
                    bdata[i] = byte.Parse(sdp.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

                byte[] bori = AESDecrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

                string stemp = ASCIIEncoding.ASCII.GetString(bori);
                sdp = stemp;

                bdata = new byte[ssn.Length / 2];
                for (int i = 0; i < bdata.Length; i++)
                    bdata[i] = byte.Parse(ssn.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

                bori = AESDecrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

                stemp = ASCIIEncoding.ASCII.GetString(bori);
                ssn = stemp;
                // hitung actv key
                byte[] bdata1 = ASCIIEncoding.ASCII.GetBytes(sdp);
                bdatatemp = ASCIIEncoding.ASCII.GetBytes(ssn);
                byte[] bdata2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                if (bdatatemp.Length >= 32)
                    Array.Copy(bdatatemp, bdata2, 32);
                else
                    Array.Copy(bdatatemp, bdata2, bdatatemp.Length);

                byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata1);
                string str = "";
                for (int i = 0; i < benc.Length; i++)
                    str += benc[i].ToString("X02");

                benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata2);
                for (int i = 0; i < benc.Length; i++)
                    str += benc[i].ToString("X02");

                byte[] bhitung = ASCIIEncoding.ASCII.GetBytes(str);
                byte[] bmac = AESCMAC(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x33, 0xA2 }, bhitung);

                str = "";
                for (int i = 0; i < bmac.Length; i++)
                    str += bmac[i].ToString("X02");
                // compare
                if (str == sak)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        // ambil sn saja
        private string GetValueSN()
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
            return (string)rktemp.GetValue("dp") + " - " + (string)rktemp.GetValue("sn");
        }
        // reset ak
        private void ResetActKey()
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk", true);

            rktemp.SetValue("ak", "");
        }

        LibCard kartu = new LibCard();
        public Form1()
        {
            InitializeComponent();
        }
        // koneksi awal
        private void Form1_Load(object sender, EventArgs e)
        {
            int ierror = CekFirstReg();
            if (ierror == 123)
            {
                AKRequest akr = new AKRequest(InjectReg());
                akr.ShowDialog();
            }
            else if (ierror == 3)
            {
                string snapp = "";
                if (ValidateValueSN())
                    snapp = GetValueSN();
                else
                    snapp = InjectReg();

                AKRequest akr = new AKRequest(snapp);
                akr.ShowDialog();
            }

            if (!CekValueRegNActKey())
            {
                MessageBox.Show("Kunci Aktivasi belum ada / tidak valid");
                ResetActKey();
                this.Close();
            }


            try
            {
                if (!kartu.ConnectSAMCard())
                {
                    MessageBox.Show("READER RUSAK 001");
                    return;
                }
                if (!kartu.AuthSAM())
                {
                    MessageBox.Show("READER RUSAK 002");
                    return;
                }

                XmlDocument xml = new XmlDocument();
                XmlNode node;

                xml.Load(sApp + "\\parameter.xml");
                node = xml.SelectSingleNode("Device");

                imei = node["imei"].InnerText;
                uid = node["uid"].InnerText;
                hp = node["hp"].InnerText;
                vacc = node["vacc"].InnerText;
                idagent = node["idagent"].InnerText;
                idterminal = node["idterminal"].InnerText;
                idmerchant = node["idmerchant"].InnerText;
                sidkios = node["idkios"].InnerText;
                url = node["wsadd"].InnerText;
                port = node["wsadp"].InnerText;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }

            label11.Text = "SILAHKAN TAP KARTU E-RETRIBUSI";

        }

        bool bsimulasi = true;
        string spassingvariable = "";       // id - nominal
        string sreturnvariable = "";        // false - desc ; true - nominal hasil saldo - jumlah pemotongan - tanggaljam - desc
        private void button1_Click(object sender, EventArgs e)
        {
            bsimulasi = !bsimulasi;
            if (bsimulasi)
                button1.Text = "WS ON";
            else
                button1.Text = "WS OFF";
        }

        private bool fSimulasiWebService_cekpenagihan()
        {
            System.IO.StreamReader sr;
            System.IO.StreamWriter sw;
            bool bret = false;
            string sretribusival = "";
            string ssisasaldo = "";
            

            string[] sdataclient = spassingvariable.Split(';');
            string stanggal = DateTime.Now.ToString("yyyyMMdd");
            string stanggaljam = DateTime.Now.ToString("yyyyMMddHHmmss");

            if (!System.IO.File.Exists("simulasi.txt"))
            {
                sw = System.IO.File.CreateText("simulasi.txt");
                sw.Close();
            }

            sr = System.IO.File.OpenText("simulasi.txt");
            bool bketemu = false;
            while (!sr.EndOfStream)
            {
                string sdata = sr.ReadLine();
                string[] ssplit = sdata.Split(';');

                if ((ssplit[0] == sdataclient[0]) && (ssplit[1] == stanggal))
                {
                    sreturnvariable = "retribusi hari ini sudah dibayarkan";
                    bret = false;
                    bketemu = true;
                    break;
                }
                else if ((ssplit[0] == sdataclient[0]) && (ssplit[1] != stanggal))
                {
                    Int64 inominal = Int64.Parse(sdataclient[1]);
                    Random rnd = new Random();
                    Int64 iretribusi = rnd.Next(2000, 5000);
                    Int64 ideposit = 0;
                    if (inominal > iretribusi)
                    {
                        ideposit = inominal - iretribusi;
                        sreturnvariable = ideposit.ToString() + ";" + iretribusi.ToString() + ";" + stanggaljam + ";" + "Retribusi tanggal " + stanggal.Substring(6, 2) + "/" + stanggal.Substring(4, 2) + "/" + stanggal.Substring(0, 4) + " dibayarkan";

                        sretribusival = iretribusi.ToString();
                        ssisasaldo = ideposit.ToString();

                        bret = true;
                        bketemu = true;
                        break;
                    }
                    else
                    {
                        sreturnvariable = "saldo tidak cukup";
                        bret = false;
                        bketemu = true;
                        break;
                    }
                }
            }
            sr.Close();

            if (!bketemu)
            {
                Int64 inominal = Int64.Parse(sdataclient[1]);
                Random rnd = new Random();
                Int64 iretribusi = rnd.Next(2000, 5000);
                Int64 ideposit = 0;
                if (inominal > iretribusi)
                {
                    ideposit = inominal - iretribusi;
                    sreturnvariable = ideposit.ToString() + ";" + iretribusi.ToString() + ";" + stanggaljam + ";" + "Retribusi tanggal " + stanggal.Substring(6, 2) + "/" + stanggal.Substring(4, 2) + "/" + stanggal.Substring(0, 4) + " dibayarkan";

                    sretribusival = iretribusi.ToString();
                    ssisasaldo = ideposit.ToString();

                    bret = true;
                }
                else
                {
                    sreturnvariable = "saldo tidak cukup";
                    bret = false;
                }
            }

            if (bret)
            {
                // tulis log
                if (!System.IO.File.Exists("simulasi.txt"))
                {
                    sw = System.IO.File.CreateText("simulasi.txt");
                    sw.Close();
                }

                List<string> stemplog = new List<string>();
                sr = System.IO.File.OpenText("simulasi.txt");
                while (!sr.EndOfStream)
                {
                    stemplog.Add(sr.ReadLine());
                }
                sr.Close();

                sw = System.IO.File.CreateText("simulasi.txt");
                sw.WriteLine(sdataclient[0] + ";" + stanggal + ";" + sretribusival + ";" + ssisasaldo);

                foreach (string ssatuan in stemplog)
                {
                    sw.WriteLine(ssatuan);
                }
                sw.Close();
            }

            return bret;
        }

        private bool fSimulasiWebService_updatepenagihan()
        {
            System.IO.StreamReader sr;
            System.IO.StreamWriter sw;
            string[] sdataclient = spassingvariable.Split(';');
            string stanggal = DateTime.Now.ToString("YYYYMMDD");

            if (!System.IO.File.Exists("simulasi.txt"))
            {
                sw = System.IO.File.CreateText("simulasi.txt");
                sw.Close();
            }

            List<string> stemplog = new List<string>();
            sr = System.IO.File.OpenText("simulasi.txt");
            while (!sr.EndOfStream)
            {
                stemplog.Add(sr.ReadLine());
            }
            sr.Close();

            sw = System.IO.File.CreateText("simulasi.txt");
            sw.WriteLine(sdataclient[0] + ";" + stanggal);
            foreach (string ssatuan in stemplog)
            {
                sw.WriteLine(ssatuan);
            }
            sw.Close();

            return true;
        }

        string uidprev = "";
        string uidnow = "";
        string slastdate = "";
        bool bsudahdibayar = false;
        string resultwc = "";
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (kartu.ConnectCard())
            {
                if (kartu.GetUIDCard(out uidnow))
                {
                    if ((uidnow != uidprev) && (uidprev == ""))
                    {
                        this.Invoke(new MethodInvoker(delegate { label11.Text = "MEMBACA KARTU E-RETRIBUSI"; }));
                        Application.DoEvents();
                        if (kartu.DivKeyCard())
                        {
                            string ssaldokartu = "";
                            if (kartu.ReadData("0005", "265", "11", out ssaldokartu) == 0)
                            {
                                int ilen = int.Parse(ssaldokartu.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                                string ssaldoriil = Function.HexStringToASCIIString(ssaldokartu.Substring(2, ilen * 2));

                                string snamakartu = "";
                                string snoloskartu = "";
                                string sidnpwrd = "";       // tambahan
                                kartu.ReadData("0005", "25", "201", out snamakartu);
                                kartu.ReadData("0003", "88", "51", out snoloskartu);

                                if (snamakartu != "")
                                {
                                    ilen = int.Parse(snamakartu.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                                    snamakartu = Function.HexStringToASCIIString(snamakartu.Substring(2, ilen * 2));
                                }
                                if (snoloskartu != "")
                                {
                                    ilen = int.Parse(snoloskartu.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                                    snoloskartu = Function.HexStringToASCIIString(snoloskartu.Substring(2, ilen * 2));
                                }


                                this.Invoke(new MethodInvoker(delegate {
                                    label6.Text = snamakartu.ToUpper();
                                    label7.Text = "ID KIOS : " + snoloskartu.ToUpper();
                                }));
                                Application.DoEvents();

                                kartu.ReadData("0003", "34", "21", out sidnpwrd);
                                if (sidnpwrd != "")
                                {
                                    ilen = int.Parse(sidnpwrd.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                                    sidnpwrd = Function.HexStringToASCIIString(sidnpwrd.Substring(2, ilen * 2)).Replace(".", "");
                                }
                                spassingvariable = uidnow + ";" + ssaldoriil;
                                //if (fSimulasiWebService_cekpenagihan())
                                //bool bSimulasi1 = true;
                                //Random rnd = new Random();
                                //int isimulasi = rnd.Next(0, 2);
                                //isimulasi = 1;
                                //if (isimulasi == 0)
                                //    bSimulasi1 = false;
                                //if (snamakartu.StartsWith("AM OKTARINI"))
                                //{
                                //    bSimulasi1 = false;
                                //    resultwc = "tagihan: ,saldo_akhir: ,resp_desc:TIDAK ADA TAGIHAN / LUNAS";
                                //}
                                //if(bSimulasi1)
                                if (sendWebServiceInq(sidnpwrd, ssaldoriil))
                                {
                                    string snoReferensi = GenReffNo("T1");
                                    //bSimulasi1 = true;
                                    //isimulasi = rnd.Next(0, 5);
                                    //isimulasi = 1;
                                    //if (isimulasi == 0)
                                    //{
                                    //    resultwc = "resp_desc:TIDAK ADA TAGIHAN / LUNAS";
                                    //    bSimulasi1 = false;
                                    //}
                                    //else
                                    //{
                                    //    resultwc = "tagihan:5000,saldo_akhir:25000,resp_desc:SUKSES";
                                    //}
                                    //Int64 isaldoawal = Int64.Parse(ssaldoriil);
                                    //Random rnd = new Random();
                                    //Int64 itagihan = rnd.Next(2, 7) * 1000;
                                    //Int64 isaldoakhir = isaldoawal - itagihan;
                                    //resultwc = "tagihan:" + itagihan.ToString() + ",saldo_akhir:" + isaldoakhir.ToString() + ",resp_desc:SUKSES";
                                    //if (snamakartu.StartsWith("SLAMET RIYADI"))
                                    //{
                                    //    resultwc = "tagihan: ,saldo_akhir: ,resp_desc:SALDO TIDAK CUKUP";
                                    //    bSimulasi1 = false;
                                    //}
                                    //if (bSimulasi1)
                                    if (sendWebServicePos(sidnpwrd, ssaldoriil, snoReferensi, snamakartu, snoloskartu))
                                    {
                                        // return true;
                                        string[] shasil = resultwc.Split(',');
                                        string snilairet = "";
                                        string ssaldoakhir = "";
                                        string serror = "";
                                        for (int i = 0; i < shasil.Length; i++)
                                        {
                                            if (shasil[i].StartsWith("tagihan"))      // cari nilai retribusi
                                                snilairet = shasil[i].Replace("tagihan:", "");
                                            if (shasil[i].StartsWith("saldo_akhir"))
                                                ssaldoakhir = shasil[i].Replace("saldo_akhir:", "");
                                            if (shasil[i].StartsWith("resp_desc"))
                                                serror = shasil[i].Replace("resp_desc:", "").Trim();
                                        }

                                        //string[] sretval = sreturnvariable.Split(';');
                                        this.Invoke(new MethodInvoker(delegate
                                        {
                                            label8.Text = int.Parse(ssaldoriil).ToString("#,0").Replace(",", ".");
                                            //label10.Text = int.Parse(sretval[0]).ToString("#,0").Replace(",", ".");
                                            label10.Text = int.Parse(ssaldoakhir.Replace(".","")).ToString("#,0").Replace(",", ".");
                                            //label9.Text = int.Parse(sretval[1]).ToString("#,0").Replace(",", ".");
                                            label9.Text = int.Parse(snilairet).ToString("#,0").Replace(",", ".");
                                            label2.Visible = true;
                                            label1.Visible = true;
                                            //label12.Text = sretval[2];
                                            //label11.Text = sretval[3].ToUpper();
                                            label11.Text = serror.ToUpper();
                                        }));
                                        Application.DoEvents();

                                        // update saldo
                                        //string updatesaldokartu = sretval[0].Length.ToString("X02") + Function.ASCIIStringToHexString(sretval[0], "");
                                        string updatesaldokartu = ssaldoakhir.Length.ToString("X02") + Function.ASCIIStringToHexString(ssaldoakhir, "");
                                        if (kartu.WriteData("0005", "265", (updatesaldokartu.Length / 2).ToString(), updatesaldokartu) != 0)
                                            if (kartu.WriteData("0005", "265", (updatesaldokartu.Length / 2).ToString(), updatesaldokartu) != 0)
                                            {
                                                this.Invoke(new MethodInvoker(delegate { label11.Text = "KARTU E-RETRIBUSI RUSAK"; }));
                                                Application.DoEvents();
                                                //MessageBox.Show("Gagal update saldo");
                                            }

                                        // print out
                                        //this.Invoke(new MethodInvoker(delegate
                                        //{
                                        try
                                        {
                                            ReportDocument rd = new ReportDocument();
                                            rd.Load(@".\PrintStruk.rpt");
                                            rd.SetParameterValue("NoKios", snoloskartu.ToUpper());
                                            rd.SetParameterValue("Pemilik", snamakartu.ToUpper());
                                            rd.SetParameterValue("Jumlah", "Rp. " + int.Parse(snilairet).ToString("#,0").Replace(",", "."));
                                            DateTime skrg = DateTime.Now;
                                            rd.SetParameterValue("TGL", skrg.ToString("dd/MM/yyyy HH:mm:ss"));
                                            rd.SetParameterValue("Reff", snoReferensi);
                                            rd.PrintToPrinter(1, false, 0, 0);
                                            rd.Close();
                                            rd.Dispose();
                                        }
                                        catch(Exception ex)
                                        {
                                            MessageBox.Show(ex.Message);
                                        }
                                        //}));
                                    }
                                    else
                                    {
                                        // tampilkan error
                                        string[] shasil = resultwc.Split(',');
                                        string serrorret = "";
                                        for (int i = 0; i < shasil.Length; i++)
                                            if (shasil[i].StartsWith("resp_desc"))
                                            {
                                                serrorret = shasil[i].Replace("resp_desc:", "");
                                                break;
                                            }

                                        label8.Text = ssaldoriil;   // saldo kartu
                                        label10.Text = "";  // retribusi
                                        label9.Text = "";   // saldo akhir

                                        label1.Visible = false;
                                        label2.Visible = false;
                                        label12.Text = "-";
                                        //label11.Text = sreturnvariable.Split(';')[0].ToUpper();
                                        label11.Text = serrorret.ToUpper();
                                    }
                                }
                                else
                                {
                                    // tampilkan error
                                    string[] shasil = resultwc.Split(',');
                                    string serrorret = "";
                                    for (int i = 0; i < shasil.Length; i++)
                                        if (shasil[i].StartsWith("resp_desc"))
                                        {
                                            serrorret = shasil[i].Replace("resp_desc:", "");
                                            break;
                                        }

                                    label8.Text = ssaldoriil;   // saldo kartu
                                    label10.Text = "";  // retribusi
                                    label9.Text = "";   // saldo akhir

                                    label1.Visible = false;
                                    label2.Visible = false;
                                    label12.Text = "-";
                                    //label11.Text = sreturnvariable.Split(';')[0].ToUpper();
                                    label11.Text = serrorret.ToUpper();
                                }
                            }
                            else
                            {
                            }
                        }
                        uidprev = uidnow;
                    }
                    else if (uidnow == uidprev)
                    {
                        // ga usah update tampilan
                    }
                }
            }
            else
            {
                uidprev = "";
                uidnow = "";
                label6.Text = "";
                label7.Text = "";

                label8.Text = "";
                label10.Text = "";
                label9.Text = "";
                label1.Visible = false;
                label2.Visible = false;
                label12.Text = "";
                label11.Text = "SILAKAN TAP KARTU E-RETRIBUSI";
            }

            timer1.Enabled = true;

        }

        // skode = TR - transaksi; UP - topup
        private string GenReffNo(string sKode)
        {
            DateTime skrg = DateTime.Now;
            string soutreff = sKode;
            soutreff += Convert.ToChar((skrg.Year - 2015) + 65);
            soutreff += Convert.ToChar(skrg.Month + 65);
            soutreff += skrg.Day.ToString("X02");
            soutreff += Convert.ToChar(skrg.Hour + 65);
            soutreff += skrg.Minute.ToString("X02");
            soutreff += skrg.Second.ToString("X02");

            return soutreff;
        }

        private bool sendWebServiceInq(string sIdNpwrd, string sNominal)
        {
            //string sapiws = "http://10.133.0.21:81/smartcard/getInqEret/{\"id_merchant\":\"4595\", \"uid\":\"8964004632641876\", \"imei\":\"353953068592966\", \"hp\":\"085640301325\", \"vacc\":\"3371010202840001\", \"id_npwrd\":\"" + sIdNpwrd.ToUpper() + "\", \"nominal\":" + sNominal + "}";
            string sapiws = "http://" + url + ":" + port + "/smartcard/getInqEret/{\"id_merchant\":\"" + idmerchant + "\", \"uid\":\"" + uid + "\", \"imei\":\"" + imei + "\", \"hp\":\"" + hp + "\", \"vacc\":\"" + vacc + "\", \"id_npwrd\":\"" + sIdNpwrd.ToUpper() + "\", \"nominal\":" + sNominal + "}";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sapiws);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                
                using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                {
                    resultwc = streamReader.ReadToEnd();
                }
                resultwc = resultwc.Replace("\"", "").Replace("{", "").Replace("}", "");
                string stemp = "";

                string[] shasil = resultwc.Split(',');
                for (int i = 0; i < shasil.Length; i++)
                    if (shasil[i].StartsWith("resp_code"))
                    {
                        stemp = shasil[i].Replace("resp_code:", "");
                        break;
                    }

                if (stemp == "00")
                    return true;
                else
                    return false;
                //MessageBox.Show(resultwc);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString(), "Gagal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // cek data dulu!!!!!!!
            return true;
            //textBox1.Text = result;
        }

        private bool sendWebServicePos(string sIdNpwrd, string sNominal, string snoReff, string sNama, string sIDKios)
        {                                                              //"uid":"89620140004632641876","imei":"353953068592966","hp":"085640301325","vacc":"3371010202840001","id_npwrd":"R200003860395","nominal":100000,"noreff":"123456","nama_pedagang":"Heni Purwanti","id_merchant":"DEPOK","id_terminal":"W001DS53","id_kios":"IKAN HIAS A.016","time":"235544"
            //string sapiws = "http://10.133.0.21:81/smartcard/getPostEret/{\"id_merchant\":\"DEPOK\",\"uid\":\"89620140004632641876\",\"imei\":\"353953068592966\",\"hp\":\"085640301325\",\"vacc\":\"3371010202840001\",\"id_npwrd\":\"" + sIdNpwrd.ToUpper() + "\",\"noreff\":\"" + snoReff.ToUpper() + "\",\"nominal\":" + sNominal + ",\"nama_pedagang\":\"" + sNama.ToUpper() + "\",\"id_kios\":\"" + sIDKios.ToUpper() + "\",\"time\":\"" + DateTime.Now.ToString("HHmmss") + "\",\"id_terminal\":\"W001DS53\"}";
            string sapiws = "http://" + url + ":" + port + "/smartcard/getPostEret/{\"id_merchant\":\"" + idmerchant + "\",\"uid\":\"" + uid + "\",\"imei\":\"" + imei + "\",\"hp\":\"" + hp + "\",\"vacc\":\"" + vacc + "\",\"id_npwrd\":\"" + sIdNpwrd.ToUpper() + "\",\"noreff\":\"" + snoReff.ToUpper() + "\",\"nominal\":" + sNominal + ",\"nama_pedagang\":\"" + sNama.ToUpper() + "\",\"id_kios\":\"" + sIDKios.ToUpper() + "\",\"time\":\"" + DateTime.Now.ToString("HHmmss") + "\",\"id_terminal\":\"" + idterminal + "\"}";
            /////////////////http://<ipaddress>:<port>/smartcard/getPostEret/       {uid":"<uid>",         "imei":"<imei>",          "hp":"<hp>",          "vacc":"<vacc>",          "id_npwrd":"<idnpwrd>",                     "noreff":"<noreff>",                     "nominal":<nominal>,         "nama_pedagang":"<namapedagang>",             "id_kios":"<idkios>",                     "time":"<time>",                                     "id_terminal":"<idterminal>",}
            
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sapiws);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                {
                    resultwc = streamReader.ReadToEnd();
                }
                resultwc = resultwc.Replace("\"", "").Replace("{", "").Replace("}", "");
                string stemp = "";

                string[] shasil = resultwc.Split(',');
                for (int i = 0; i < shasil.Length; i++)
                    if (shasil[i].StartsWith("resp_code"))
                    {
                        stemp = shasil[i].Replace("resp_code:", "");
                        break;
                    }

                if (stemp == "00")
                    return true;
                else
                    return false;
                //MessageBox.Show(resultwc);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString(), "Gagal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // cek data dulu!!!!!!!
            return true;
            //textBox1.Text = result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (sendWebServiceInq("R200003860511", "50000"))
            {
                MessageBox.Show(resultwc);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
