﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Security.Cryptography;
using System.Management;
using Microsoft.Win32;
using System.IO;

namespace RetribusiKiosK
{
    public partial class AKRequest : Form
    {
        string snapp = "";
        public AKRequest(string sSN)
        {
            InitializeComponent();
            snapp = sSN;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private byte[] AESEncrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        private byte[] AESDecrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        private byte[] Rol(byte[] b)
        {
            byte[] r = new byte[b.Length];
            byte carry = 0;

            for (int i = b.Length - 1; i >= 0; i--)
            {
                ushort u = (ushort)(b[i] << 1);
                r[i] = (byte)((u & 0xff) + carry);
                carry = (byte)((u & 0xff00) >> 8);
            }

            return r;
        }

        private byte[] AESCMAC(byte[] key, byte[] data)
        {
            // SubKey generation
            // step 1, AES-128 with key K is applied to an all-zero input block.
            byte[] L = AESEncrypt(key, new byte[16], new byte[16]);

            // step 2, K1 is derived through the following operation:
            byte[] FirstSubkey = Rol(L); //If the most significant bit of L is equal to 0, K1 is the left-shift of L by 1 bit.
            if ((L[0] & 0x80) == 0x80)
                FirstSubkey[15] ^= 0x87; // Otherwise, K1 is the exclusive-OR of const_Rb and the left-shift of L by 1 bit.

            // step 3, K2 is derived through the following operation:
            byte[] SecondSubkey = Rol(FirstSubkey); // If the most significant bit of K1 is equal to 0, K2 is the left-shift of K1 by 1 bit.
            if ((FirstSubkey[0] & 0x80) == 0x80)
                SecondSubkey[15] ^= 0x87; // Otherwise, K2 is the exclusive-OR of const_Rb and the left-shift of K1 by 1 bit.

            // MAC computing
            if (((data.Length != 0) && (data.Length % 16 == 0)) == true)
            {
                // If the size of the input message block is equal to a positive multiple of the block size (namely, 128 bits),
                // the last block shall be exclusive-OR'ed with K1 before processing
                for (int j = 0; j < FirstSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= FirstSubkey[j];
            }
            else
            {
                // Otherwise, the last block shall be padded with 10^i
                byte[] padding = new byte[16 - data.Length % 16];
                padding[0] = 0x80;

                data = data.Concat<byte>(padding.AsEnumerable()).ToArray();

                // and exclusive-OR'ed with K2
                for (int j = 0; j < SecondSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= SecondSubkey[j];
            }

            // The result of the previous process will be the input of the last encryption.
            byte[] encResult = AESEncrypt(key, new byte[16], data);

            byte[] HashValue = new byte[16];
            Array.Copy(encResult, encResult.Length - HashValue.Length, HashValue, 0, HashValue.Length);

            return HashValue;
        }

        // masukkan kunci aktivasi
        private bool ValidateNinsertActKey(string sActivationKey)
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk");
            string sdp = (string)rktemp.GetValue("dp");
            string ssn = (string)rktemp.GetValue("sn");
            rktemp.Close();
            rk.Close();

            // dekrip dulu
            byte[] bdata = new byte[sdp.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(sdp.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            byte[] bori = AESDecrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);

            string stemp = ASCIIEncoding.ASCII.GetString(bori);
            sdp = stemp;

            bdata = new byte[ssn.Length / 2];
            for (int i = 0; i < bdata.Length; i++)
                bdata[i] = byte.Parse(ssn.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            bori = AESDecrypt(new byte[] { 0x20, 0x05, 0x24, 0x06, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);

            stemp = ASCIIEncoding.ASCII.GetString(bori);
            ssn = stemp;
            // hitung actv key
            byte[] bdata1 = ASCIIEncoding.ASCII.GetBytes(sdp);
            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(ssn);
            byte[] bdata2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata2, 32);
            else
                Array.Copy(bdatatemp, bdata2, bdatatemp.Length);

            byte[] benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata1);
            string str = "";
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            benc = AESEncrypt(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata2);
            for (int i = 0; i < benc.Length; i++)
                str += benc[i].ToString("X02");

            byte[] bhitung = ASCIIEncoding.ASCII.GetBytes(str);
            byte[] bmac = AESCMAC(new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x33, 0xA2 }, bhitung);

            str = "";
            for (int i = 0; i < bmac.Length; i++)
                str += bmac[i].ToString("X02");
            // compare
            if (str == sActivationKey)
            {
                rk = Registry.CurrentUser;
                rktemp = rk.OpenSubKey("SOFTWARE\\eBJPST\\kiosk", true);
                rktemp.SetValue("ak", str);
                rktemp.Close();
                rk.Close();
                return true;
            }
            else
                return false;
        }

        private void AKRequest_Load(object sender, EventArgs e)
        {
            textBox1.Text = snapp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // validasi kunci aktivasi
            if (ValidateNinsertActKey(textBox2.Text))
                this.Close();
            else
            {
                MessageBox.Show("Kunci aktivasi salah");
                textBox2.Focus();
            }
        }
    }
}
