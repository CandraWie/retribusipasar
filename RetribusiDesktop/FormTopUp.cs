﻿using System;
using System.Windows.Forms;
using libCard;
using LibSmartCard;
using System.Net;
using System.Xml;
using System.Drawing;
using CrystalDecisions.CrystalReports.Engine;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;
using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;

using System.Text;

namespace RetribusiDesktop
{
    public partial class FormTopUp : Form
    {
        string sApp = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        //string inputdata = "";
        byte[] passing;
        string skey = "3008201630102016";
        private LibCard smartCard;
        int retCode = 0;
        byte[] passparameter = new byte[16];
        byte[] key = new byte[8];
        string uidprev = "";
        string uidnow = "";
        string idnpwprd = "";
        string namapedagang = "";
        string nomorkios = "";
        string saldo = "";
        string norek = "";
        string imei = "";
        string uid = "";
        string hp = "";
        string vacc = "";
        string idagent = "";
        string idterminal = "";
        string idmerchant = "";
        string idkios = "";
        string url = "";
        string port = "";
        string[] databackground;
        string infodata = "";
        string deskripsierror = "";
        string logdata = "";
        string username = "";
        DateTime skrg = DateTime.Now;
        string sAppAdd = AppDomain.CurrentDomain.BaseDirectory;

        public FormTopUp(string usernamedata)
        {
            InitializeComponent();
            username = usernamedata;
        }

        public void AmbilDataXML()
        {
            string sApp = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            XmlDocument xml = new XmlDocument();
            XmlNode node;
            xml.Load(sApp + "\\parameter.xml");
            node = xml.SelectSingleNode("Device");
            imei = node["imei"].InnerText;
            uid = node["uid"].InnerText;
            hp = node["hp"].InnerText;
            vacc = node["vacc"].InnerText;
            idagent = node["idagent"].InnerText;
            idterminal = node["idterminal"].InnerText;
            idmerchant = node["idmerchant"].InnerText;
            url = node["wsadd"].InnerText;
            port = node["wsadp"].InnerText;
            idkios = node["idkios"].InnerText;
        }

        private bool sendWebServiceTopUp(string sIdNpwrd, string sReff, string sNominalTopUp, string nama, string sIdKios, string sSerialNumber, ref string saldoakhir)
        {
            infodata = "";
            deskripsierror = "";
            string sapiws = "http://" + url + ":" + port + "/smartcard/getTopUp/{\"id_merchant\":\"" + idmerchant.ToUpper() + "\",\"uid\":\"" + uid + "\",\"imei\":\"" + imei + "\",\"hp\":\"" + hp + "\",\"vacc\":\"" + vacc + "\",\"id_npwrd\":\"" + sIdNpwrd.Replace(".", "").ToUpper() + "\",\"nama_pedagang\":\"";
            string result = "";
            sapiws += nama.ToUpper() + "\",\"noreff\":\"";
            sapiws += sReff + "\",\"nominal\":";
            sapiws += sNominalTopUp + ",\"serial_num\":\"";
            sapiws += sSerialNumber + "\",\"id_terminal\":\"" + idterminal + "\",\"time\":\"";
            // sapiws += sSerialNumber + ",\"id_agent\":\"" + idagent + "\",\"id_terminal\":\"" + idterminal + "\",\"time\":\"";
            sapiws += DateTime.Now.ToString("HHmmss") + "\"}";

            //http://localhost:83/smartcard/getTopUp/{"id_merchant":"D-PC-00001","uid":"896210012613705942","imei":"3241340700479697","hp":"08112676895","vacc":"6274510000000001","id_npwrd":"","nama_pedagang":"","noreff":"","nominal":"","serial_num_f":"","id_terminal":"","time":""}

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sapiws);
                httpWebRequest.Timeout = 60000;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                result = result.Replace("\"", "").Replace("{", "").Replace("}", "");

                string[] shasil = result.Split(',');
                for (int i = 0; i < shasil.Length; i++)
                {
                    if (shasil[i].StartsWith("resp_code"))
                    {
                        infodata = shasil[i].Replace("resp_code:", "");
                    }
                    if (shasil[i].StartsWith("saldo_akhir"))
                    {
                        saldoakhir = shasil[i].Replace("saldo_akhir:", "");
                    }
                    if (shasil[i].StartsWith("resp_desc"))
                    {
                        deskripsierror = shasil[i].Replace("resp_desc:", "");
                    }
                }
                if (infodata == "00")
                    return true;
                else if (infodata == "")
                {
                    infodata = "Time out";
                    deskripsierror = "Server Tidak ada Respon";
                    return false;
                }
                else
                    return false;
            }
            catch
            {
                deskripsierror = "Tidak ada Respon";
                return false;
            }
        }

        private void Top_up(string jumlahTopUp)
        {
            if (labelNama.Text == "" || labelNpwrd.Text == "")
            {
                MessageBox.Show("Tidak ada kartu", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            bool suksescetak = false;
            bool passing = true;
            string data = "";
            string snkartu = "";
            string isaldo = "";
            string reffrensi = GenReffNo("u2");
            string saldoakhir = "";

            DialogResult dialogResult1 = MessageBox.Show("Apakah anda yakin dengan Top Up sebesar : Rp " + Convert.ToInt32(jumlahTopUp).ToString("#,0").Replace(",", "."), "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult1 == DialogResult.No)
                return;

            if (!smartCard.ConnectCard()) { MessageBox.Show("Tidak dapat membaca smartCard Samsat", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.DivKeyCard()) { MessageBox.Show("smartCard Samsat tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            
            smartCard.GetSerialCard(out snkartu);
            passing = true;// sendWebServiceTopUp(idnpwprd, reffrensi, jumlahTopUp, namapedagang, nomorkios, snkartu, ref saldoakhir);
            if(saldoakhir=="")
            {
                saldoakhir = jumlahTopUp;
            }
            if (passing)
            {
                isaldo = Function.NumberToHexa(saldoakhir.TrimEnd(' ').Length, 2) + Function.ASCIIStringToHexString(saldoakhir.TrimEnd(' '), "");
                data = isaldo.PadRight(22, '0');
                retCode = smartCard.WriteData("0005", "265", "11", data);
                if (retCode == 0)
                {
                    skrg = DateTime.Now;
                    labelSaldo.Text = "Rp " + Int64.Parse(saldoakhir).ToString("#,0").Replace(",", ".");
                    WriteLog(jumlahTopUp, reffrensi, saldoakhir);
                    dialogResult1 = MessageBox.Show("Proses Top Up Berhasil ! \nApakah anda ingin Mencetak Struk ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult1 == DialogResult.No)
                        return;
                    else
                        CetakStruk(jumlahTopUp, reffrensi, out suksescetak);

                    if (suksescetak)
                    {
                        MessageBox.Show("Berhasil", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                        return;
                }

                else
                {
                    MessageBox.Show("Gagal Top Up ke Kartu, Tap Kartu Kembali Untuk Lakukan Sinkronisasi!", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Tidak dapat melakukan Top Up - " + deskripsierror + " - " + infodata, "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void WriteLog(string jumlahTopUp, string reffrensi, string saldoakhir)
        {
            logdata = "";
            logdata = username + ";" + idagent + ";" + namapedagang.ToUpper() + ";" + idnpwprd + ";" + Convert.ToInt32(jumlahTopUp).ToString("#,0").Replace(",", ".") + ";" + skrg.ToString("dd/MM/yyyy;HH:mm:ss") + ";" + reffrensi + ";" + Int64.Parse(saldoakhir).ToString("#,0").Replace(",", ".");
            int ilongdata = logdata.Length + 16-(logdata.Length % 16);
            byte[] passing = new byte[ilongdata];
            byte[] key = Function.HexaStringToArrayOfByte(skey);
            //logdata = Function.ASCIIStringToArrayOfByte(logdata.PadLeft(ilongdata,'*'));
            passing = Function.ASCIIStringToArrayOfByte(logdata.PadLeft(ilongdata, '*'));
            encryptDESECB(passing.Length, passing, key, out passing);
            logdata = Function.ArrayOfByteToHexString(passing, passing.Length, "");
            System.IO.StreamWriter sw = System.IO.File.AppendText(sAppAdd + "SLL.txt");

            sw.WriteLine(logdata);
            sw.Close();
        }

        private bool sendWebServiceSinkron(string sIdNpwrd, string nama, ref string saldoakhir)
        {
            string sapiws = "http://" + url + ":" + port + "/smartcard/getSynBalance/{\"id_merchant\":\"" + idmerchant.ToUpper() + "\",\"uid\":\"" + uid + "\",\"imei\":\"" + imei + "\",\"hp\":\"" + hp + "\",\"vacc\":\"" + vacc + "\",\"id_npwrd\":\"" + sIdNpwrd.Replace(".", "").ToUpper() + "\",\"nama_pedagang\":\"";
            string result = "";
            sapiws += nama.ToUpper() + "\",\"nominal\":0}";
            
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sapiws);
                httpWebRequest.Timeout = 60000;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                result = result.Replace("\"", "").Replace("{", "").Replace("}", "");

                string[] shasil = result.Split(',');
                for (int i = 0; i < shasil.Length; i++)
                {
                    if (shasil[i].StartsWith("resp_code"))
                    {
                        infodata = shasil[i].Replace("resp_code:", "");
                    }
                    if (shasil[i].StartsWith("saldo_akhir"))
                    {
                        saldoakhir = shasil[i].Replace("saldo_akhir:", "");
                    }
                    if (shasil[i].StartsWith("resp_desc"))
                    {
                        deskripsierror = shasil[i].Replace("resp_desc:", "");
                    }
                }
                if (infodata == "00")
                    return true;
                else if (infodata == "")
                {
                    infodata = "Time out";
                    deskripsierror = "Server Tidak ada Respon";
                    return false;
                }
                else
                    return false;
            }
            catch
            {
                deskripsierror = "Tidak ada Respon";
                return false;
            }
        }

        private void Synchronize()
        {
            if (labelNama.Text == "" || labelNpwrd.Text == "")
            {
                MessageBox.Show("Tidak Ada Data", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            bool passing = true;
            string data = "";
            string isaldo = "";
            string reffrensi = GenReffNo("u2");
            string saldoakhir = "";
            
            passing = sendWebServiceSinkron(idnpwprd, namapedagang, ref saldoakhir);
            if (passing)
            {
                isaldo = Function.NumberToHexa(saldoakhir.TrimEnd(' ').Length, 2) + Function.ASCIIStringToHexString(saldoakhir.TrimEnd(' '), "");
                data = isaldo.PadRight(22, '0');
                retCode = smartCard.WriteData("0005", "265", "11", data);
                if (retCode == 0)
                {
                    labelSaldo.Text = "Rp " + Int64.Parse(saldoakhir).ToString("#,0").Replace(",", ".");
                    return;
                }
                else
                {
                    MessageBox.Show("Gagal Sinkronisasi Kartu, \nSilakan Lakukan Tapping Kembali!", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Tidak dapat melakukan Sinkronisasi - " + deskripsierror + " - " + infodata + " \nTap Kartu Kembali untuk Lakukan Sinkronisasi", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private string GenReffNo(string sKode)
        {
            DateTime skrg = DateTime.Now;
            string soutreff = sKode;
            soutreff += Convert.ToChar((skrg.Year - 2015) + 65);
            soutreff += Convert.ToChar(skrg.Month + 65);
            soutreff += skrg.Day.ToString("X02");
            soutreff += Convert.ToChar(skrg.Hour + 65);
            soutreff += skrg.Minute.ToString("X02");
            soutreff += skrg.Second.ToString("X02");
            return soutreff;
        }

        private void b20_Click(object sender, EventArgs e)
        {
            Top_up("20000");
        }

        private void b50_Click(object sender, EventArgs e)
        {
            Top_up("50000");
        }

        private void b100_Click(object sender, EventArgs e)
        {
            Top_up("100000");
        }

        private void b200_Click(object sender, EventArgs e)
        {
            Top_up("200000");
        }

        private void b500_Click(object sender, EventArgs e)
        {
            Top_up("500000");
        }

        #region Tampilan

        private void b20_MouseLeave(object sender, EventArgs e)
        {
            b20.Font = new System.Drawing.Font(b20.Font.FontFamily, b20.Font.Size - 4, FontStyle.Bold);
            b20.Top = b20.Top + 10;
            b20.Left = b20.Left + 10;
            b20.Width = b20.Width - 20;
            b20.Height = b20.Height - 20;
        }

        private void b20_MouseEnter(object sender, EventArgs e)
        {
            b20.Font = new System.Drawing.Font(b20.Font.FontFamily, b20.Font.Size + 4, FontStyle.Bold);
            b20.Top = b20.Top - 10;
            b20.Left = b20.Left - 10;
            b20.Width = b20.Width + 20;
            b20.Height = b20.Height + 20;
        }

        private void b50_MouseEnter(object sender, EventArgs e)
        {
            b50.Font = new System.Drawing.Font(b50.Font.FontFamily, b50.Font.Size + 4, FontStyle.Bold);
            b50.Top = b50.Top - 10;
            b50.Left = b50.Left - 10;
            b50.Width = b50.Width + 20;
            b50.Height = b50.Height + 20;
        }

        private void b50_MouseLeave(object sender, EventArgs e)
        {
            b50.Font = new System.Drawing.Font(b50.Font.FontFamily, b50.Font.Size - 4, FontStyle.Bold);
            b50.Top = b50.Top + 10;
            b50.Left = b50.Left + 10;
            b50.Width = b50.Width - 20;
            b50.Height = b50.Height - 20;
        }

        private void b100_MouseEnter(object sender, EventArgs e)
        {
            b100.Font = new System.Drawing.Font(b100.Font.FontFamily, b100.Font.Size + 4, FontStyle.Bold);
            b100.Top = b100.Top - 10;
            b100.Left = b100.Left - 10;
            b100.Width = b100.Width + 20;
            b100.Height = b100.Height + 20;
        }

        private void b100_MouseLeave(object sender, EventArgs e)
        {
            b100.Font = new System.Drawing.Font(b100.Font.FontFamily, b100.Font.Size - 4, FontStyle.Bold);
            b100.Top = b100.Top + 10;
            b100.Left = b100.Left + 10;
            b100.Width = b100.Width - 20;
            b100.Height = b100.Height - 20;
        }

        private void b200_MouseEnter(object sender, EventArgs e)
        {
            b200.Font = new System.Drawing.Font(b200.Font.FontFamily, b200.Font.Size + 4, FontStyle.Bold);
            b200.Top = b200.Top - 10;
            b200.Left = b200.Left - 10;
            b200.Width = b200.Width + 20;
            b200.Height = b200.Height + 20;
        }

        private void b200_MouseLeave(object sender, EventArgs e)
        {
            b200.Font = new System.Drawing.Font(b200.Font.FontFamily, b200.Font.Size - 4, FontStyle.Bold);
            b200.Top = b200.Top + 10;
            b200.Left = b200.Left + 10;
            b200.Width = b200.Width - 20;
            b200.Height = b200.Height - 20;
        }

        private void b500_MouseEnter(object sender, EventArgs e)
        {
            b500.Font = new System.Drawing.Font(b500.Font.FontFamily, b500.Font.Size + 4, FontStyle.Bold);
            b500.Top = b500.Top - 10;
            b500.Left = b500.Left - 10;
            b500.Width = b500.Width + 20;
            b500.Height = b500.Height + 20;
        }

        private void b500_MouseLeave(object sender, EventArgs e)
        {
            b500.Font = new System.Drawing.Font(b500.Font.FontFamily, b500.Font.Size - 4, FontStyle.Bold);
            b500.Top = b500.Top + 10;
            b500.Left = b500.Left + 10;
            b500.Width = b500.Width - 20;
            b500.Height = b500.Height - 20;
        }

        private void btnExit_MouseEnter(object sender, EventArgs e)
        {
            btnExit.Top = btnExit.Top - 5;
            btnExit.Left = btnExit.Left - 5;
            btnExit.Width = btnExit.Width + 10;
            btnExit.Height = btnExit.Height + 10;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.Top = btnExit.Top + 5;
            btnExit.Left = btnExit.Left + 5;
            btnExit.Width = btnExit.Width - 10;
            btnExit.Height = btnExit.Height - 10;
        }

        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        cp.ExStyle |= 0x02000000;
        //        return cp;
        //    }
        //}

        #endregion

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Apakah anda yakin ingin keluar dari Aplikasi ini ", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.No)
            {
                return;
            }
            else
                this.Close();
        }
        
        private void timerTopUp_Tick(object sender, EventArgs e)
        {
            timerTopUp.Enabled = false;
            int executecard = 0;
            long ilengthnama;
            long ilengthsaldo;
            long ilengthrek;
            long ilengthnpwrd;
            long ilengthkios;
            string data = "";
            if (smartCard.ConnectCard())
            {

                if (smartCard.GetUIDCard(out uidnow))
                {
                    if ((uidnow != uidprev) && (uidprev == ""))
                    {
                        if (smartCard.DivKeyCard())
                        {
                            executecard = smartCard.ReadData("0005", "25", "285", out data);
                            if (executecard == 0)
                            {
                                ilengthnama = Function.HexaToNumber(data.Substring(0, 2));
                                ilengthrek = Function.HexaToNumber(data.Substring(418, 2));
                                ilengthsaldo = Function.HexaToNumber(data.Substring(480, 2));

                                data = Function.HexStringToASCIIString(data);
                                namapedagang = data.Substring(1, (int)ilengthnama).ToUpper();
                                norek = data.Substring(210, (int)ilengthrek).ToUpper();
                                saldo = data.Substring(241, (int)ilengthsaldo).ToUpper();

                                if (saldo == "")
                                    saldo = "0";
                            }

                            else
                            {
                                MessageBox.Show("Kartu Tidak Terdaftar", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                timerTopUp.Enabled = true;
                                return;
                            }
                            executecard = smartCard.ReadData("0003", "34", "139", out data);
                            if (executecard == 0)
                            {
                                ilengthnpwrd = Function.HexaToNumber(data.Substring(0, 2));
                                ilengthkios = Function.HexaToNumber(data.Substring(108, 2));
                                data = Function.HexStringToASCIIString(data);
                                idnpwprd = data.Substring(1, (int)ilengthnpwrd).ToUpper();
                                nomorkios = data.Substring(55, (int)ilengthkios).ToUpper();
                                label1.Visible = true;
                                label2.Visible = true;
                                label4.Visible = true;
                                labelNama.Visible = true;
                                labelNpwrd.Visible = true;
                                labelSaldo.Visible = true;
                                labelSilakan.Visible = false;
                            }
                            else
                            {
                                MessageBox.Show("Kartu Tidak Terdaftar", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                timerTopUp.Enabled = true;
                                return;
                            }
                        }
                        uidprev = uidnow;
                        labelNpwrd.Text = idnpwprd;
                        labelNama.Text = namapedagang;
                        labelSaldo.Text = "Rp " + Int64.Parse(saldo).ToString("#,0").Replace(",", ".");
                        System.Windows.Forms.Application.DoEvents();
                        Synchronize();
                        string snumbercard = "";
                        smartCard.GetSerialCard(out snumbercard);
                       }
                }
            }

            else
            {
                uidprev = "";
                uidnow = "";
                labelNama.Visible = false;
                labelNpwrd.Visible = false;
                labelSaldo.Visible = false;
                label1.Visible = false;
                label2.Visible = false;
                label4.Visible = false;
                labelSilakan.Visible = true;
            }
            timerTopUp.Enabled = true;
        }

        private void FormTopUp_Load(object sender, EventArgs e)
        {
           // cekLongData();
            databackground = System.IO.Directory.GetFiles(sApp + "\\Images");
            this.BackgroundImage = null;
            for(int i=0; i< databackground.Length;i++)
            {
                if(databackground[i].Contains("topupbground"))
                    this.BackgroundImage = Image.FromFile(databackground[i]); 
            }

            if (!System.IO.File.Exists(sAppAdd + "SLL.txt"))
                System.IO.File.CreateText(sAppAdd + "SLL.txt");

            smartCard = new LibCard();
            try
            {
                if (!smartCard.ConnectSAMCard())
                {
                    MessageBox.Show("Belum terhubung dengan reader", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                }
                if (!smartCard.AuthSAM())
                {
                    MessageBox.Show("Tidak dapat Autentikasi SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
            AmbilDataXML();
            timerTopUp.Enabled = true;
            labelNama.Text = "";
            labelNpwrd.Text = "";
        }

        private void CetakStruk(string sjumlahsaldo, string sreffrensi, out bool sukses)
        {
            try
            {
                ReportDocument rd = new ReportDocument();
                rd.Load(@".\PrintStruk.rpt");
                rd.SetParameterValue("NPWRD", idnpwprd);
                rd.SetParameterValue("Pemilik", namapedagang.ToUpper());
                rd.SetParameterValue("Jumlah", "Rp " + int.Parse(sjumlahsaldo).ToString("#,0").Replace(",", "."));
                
                rd.SetParameterValue("TGL", skrg.ToString("dd/MM/yyyy HH:mm:ss"));
                rd.SetParameterValue("Reff", sreffrensi);
                rd.SetParameterValue("saldo", labelSaldo.Text);
                rd.PrintToPrinter(1, false, 0, 0);
                rd.Close();
                rd.Dispose();
                sukses = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Tidak dapat mencetak struk pembayaran " + ex.ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sukses = false;
            }
        }

        private void encryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(true, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        private void cekLongData()
        {
            MessageBox.Show("Record Top Up Sudah Penuh \n Tunggu Proses Penyimpanan", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            string inputdata = "";
            List<string>[] dataready = new List<string>[9];
            dataready[0] = new List<string>();
            dataready[1] = new List<string>();
            dataready[2] = new List<string>();
            dataready[3] = new List<string>();
            dataready[4] = new List<string>();
            dataready[5] = new List<string>();
            dataready[6] = new List<string>();
            dataready[7] = new List<string>();
            dataready[8] = new List<string>();
            System.IO.StreamReader sr = System.IO.File.OpenText(sAppAdd + "SLL.txt");
            string allfiletext = sr.ReadToEnd();
            sr.Close();
            sr = System.IO.File.OpenText(sAppAdd + "SLL.txt");
            int x = 0;
            if (allfiletext.Length > 1000000)
            {
                try
                {
                    while (!sr.EndOfStream)
                    {
                        x++;
                        inputdata = sr.ReadLine();
                        passing = HexaStringToArrayOfByte(inputdata);
                        key = HexaStringToArrayOfByte(skey);
                        decryptDESECB(passing.Length, passing, key, out passing);
                        inputdata = ASCIIEncoding.ASCII.GetString(passing);
                        string[] splititem = inputdata.Replace("*", "").Split(';');
                        //dataGridView1.Rows.Add(splititem[0].ToUpper(), splititem[1], splititem[2].ToUpper(), splititem[3], splititem[4], splititem[5], splititem[6], splititem[7].ToUpper(), splititem[8]);
                        dataready[0].Add(splititem[0].ToUpper());
                        dataready[1].Add(splititem[1]);
                        dataready[2].Add(splititem[2].ToUpper());
                        dataready[3].Add(splititem[3]);
                        dataready[4].Add(splititem[4]);
                        dataready[5].Add(splititem[5]);
                        dataready[6].Add(splititem[6]);
                        dataready[7].Add(splititem[7].ToUpper());
                        dataready[8].Add(splititem[8]);
                    }
                    sr.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Gagal mengambil data "+ex.ToString(), "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

               try
                {
                    Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                    Microsoft.Office.Interop.Excel.Workbook workbook = app.Workbooks.Add(Type.Missing);
                    Microsoft.Office.Interop.Excel.Worksheet worksheet = null;
                    //app.Visible = true;
                    worksheet = workbook.Sheets["Sheet1"];
                    worksheet = workbook.ActiveSheet;
                    worksheet.Cells[1, 1] = "Nama User";
                    worksheet.Cells[1, 2] = "Lok. Transaksi";
                    worksheet.Cells[1, 3] = "Nama Nasabah";
                    worksheet.Cells[1, 4] = "ID NPWRD";
                    worksheet.Cells[1, 5] = "Nominal Top Up";
                    worksheet.Cells[1, 6] = "Tanggal";
                    worksheet.Cells[1, 7] = "Waktu";
                    worksheet.Cells[1, 8] = "No. Transaksi";
                    worksheet.Cells[1, 9] = "Saldo Akhir";

                    for (int i = 0; i < dataready[0].Count; i++)
                    {
                        for (int j = 0; j < 9; j++)
                        {
                            worksheet.Cells[i + 2, j + 1] = dataready[j][i];
                        }
                    }

                    //workbook.SaveAs("Record_Data" + skrg.ToString("dd/MM/yy"), XlFileFormat.xlWorkbookDefault);
                    app.Visible = true;

                    MessageBox.Show("Berhasil menyimpan data", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Gagal menyimpan data " + ex, "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        byte[] HexaStringToArrayOfByte(string hex)
        {
            hex = hex.Replace(" ", "");
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        private void decryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(false, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }
    }

}
