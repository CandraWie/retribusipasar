﻿using System;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Management;
using System.IO;


namespace RetribusiDesktop
{
    public class KeygenProtection
    {
        #region proteksi


        #region top up v2.0
        static string sKeyFD = "AA7898546711235A6CFF11D2567D91A2";
        static string sIVFD = "2378AB2276091C5A6CFF9A76567D9122";
        static string sKeySN = "24040418F5491787922438C4B12F3211";
        static string sIVSN = "4478131D67BA235A2F32112378A86276";
        static string sKeyActFD = "AA75E3FC6711235A6CFF9AD2567D33A2";
        static string sIVActFD = "2378AB5576751C5A6CFF68D2567D9032";
        static string sKeyActSN = "AA7813F71711235A6CA511D2567D33A2";
        static string sIVActSN = "2378A9A576261C5A6CB768D2567D91A2";
        static string sKeyCMAC = "A17813FC671126154A6CFF68D25633A2";
        static string sRegPath = "SOFTWARE\\eBJPST\\topup";
        #endregion
        
        public static byte[] AESEncrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        public static byte[] AESDecrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }

        public static byte[] Rol(byte[] b)
        {
            byte[] r = new byte[b.Length];
            byte carry = 0;

            for (int i = b.Length - 1; i >= 0; i--)
            {
                ushort u = (ushort)(b[i] << 1);
                r[i] = (byte)((u & 0xff) + carry);
                carry = (byte)((u & 0xff00) >> 8);
            }

            return r;
        }

        public static byte[] AESCMAC(byte[] key, byte[] data)
        {
            // SubKey generation
            // step 1, AES-128 with key K is applied to an all-zero input block.
            byte[] L = AESEncrypt(key, new byte[16], new byte[16]);

            // step 2, K1 is derived through the following operation:
            byte[] FirstSubkey = Rol(L); //If the most significant bit of L is equal to 0, K1 is the left-shift of L by 1 bit.
            if ((L[0] & 0x80) == 0x80)
                FirstSubkey[15] ^= 0x87; // Otherwise, K1 is the exclusive-OR of const_Rb and the left-shift of L by 1 bit.

            // step 3, K2 is derived through the following operation:
            byte[] SecondSubkey = Rol(FirstSubkey); // If the most significant bit of K1 is equal to 0, K2 is the left-shift of K1 by 1 bit.
            if ((FirstSubkey[0] & 0x80) == 0x80)
                SecondSubkey[15] ^= 0x87; // Otherwise, K2 is the exclusive-OR of const_Rb and the left-shift of K1 by 1 bit.

            // MAC computing
            if (((data.Length != 0) && (data.Length % 16 == 0)) == true)
            {
                // If the size of the input message block is equal to a positive multiple of the block size (namely, 128 bits),
                // the last block shall be exclusive-OR'ed with K1 before processing
                for (int j = 0; j < FirstSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= FirstSubkey[j];
            }
            else
            {
                // Otherwise, the last block shall be padded with 10^i
                byte[] padding = new byte[16 - data.Length % 16];
                padding[0] = 0x80;

                data = data.Concat<byte>(padding.AsEnumerable()).ToArray();

                // and exclusive-OR'ed with K2
                for (int j = 0; j < SecondSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= SecondSubkey[j];
            }

            // The result of the previous process will be the input of the last encryption.
            byte[] encResult = AESEncrypt(key, new byte[16], data);

            byte[] HashValue = new byte[16];
            Array.Copy(encResult, encResult.Length - HashValue.Length, HashValue, 0, HashValue.Length);

            return HashValue;
        }

        // cek dulu
        public static int CekFirstReg()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey rktemp = rk.OpenSubKey(sRegPath);
                if (rktemp == null)
                    return 123;

                if (((string)rktemp.GetValue("dp") == "") || ((string)rktemp.GetValue("sn") == ""))
                    return 123;

                if (((string)rktemp.GetValue("dp") != "") && ((string)rktemp.GetValue("sn") != "") && ((string)rktemp.GetValue("ak") == ""))
                    return 3;

                if (((string)rktemp.GetValue("dp") != "") && ((string)rktemp.GetValue("sn") != "") && ((string)rktemp.GetValue("ak") != ""))
                    return 0;

                return 123;
            }
            catch
            {
                return 123;
            }
        }
        // kalo ga ada -- 123 .. bikin baru.. kluarkan serial number
        public static string InjectReg()
        {
            string spd = "";
            string sn = "";

            string stempsn = "";
            ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
            foreach (ManagementObject mo in search.Get())
            {
                if (mo["InterfaceType"].ToString() != "USB")
                {
                    string stemp1 = "", stemp2 = "";
                    try { stemp1 = mo["SerialNumber"].ToString(); } catch { };
                    try { stemp2 = mo["Model"].ToString(); } catch { };

                    stempsn = stemp1 + stemp2;
                }
            }

            byte[] bkeysn = new byte[16];
            byte[] bivsn = new byte[16];
            byte[] bkeyfd = new byte[16];
            byte[] bivfd = new byte[16];
            for (int i = 0; i < 16; i++)
            {
                bkeysn[i] = byte.Parse(sKeySN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                bivsn[i] = byte.Parse(sIVSN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                bkeyfd[i] = byte.Parse(sKeyFD.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                bivfd[i] = byte.Parse(sIVFD.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }

            byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
            byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            if (bdatatemp.Length >= 32)
                Array.Copy(bdatatemp, bdata, 32);
            else
                Array.Copy(bdatatemp, bdata, bdatatemp.Length);

            //byte[] bhasil = AESEncrypt(new byte[] { 0x24, 0x05, 0x24, 0x18, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);
            byte[] bhasil = AESEncrypt(bkeysn, bivsn, bdata);

            sn = "";
            for (int i = 0; i < bhasil.Length; i++)
                sn += bhasil[i].ToString("X02");

            string stanggalsekarang = "2016.09/17-23:17";//DateTime.Now.ToString("yyyy.MM/dd-HH:mm");
            bdata = ASCIIEncoding.ASCII.GetBytes(stanggalsekarang);
            //byte[] benc = AESEncrypt(new byte[] { 0xAA, 0x78, 0x13, 0xFC, 0x67, 0x11, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);
            byte[] benc = AESEncrypt(bkeyfd, bivfd, bdata);

            spd = "";
            for (int i = 0; i < benc.Length; i++)
                spd += benc[i].ToString("X02");

            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.CreateSubKey(sRegPath);
            rktemp.SetValue("dp", spd);
            rktemp.SetValue("sn", sn);
            rktemp.SetValue("ak", "");

            return spd + " - " + sn;  // out SN aplikasi
        }
        // kalo kunci aktivasi saja yg blm ada
        public static bool ValidateValueSN()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey rktemp = rk.OpenSubKey(sRegPath);
                string ssn = (string)rktemp.GetValue("sn");

                byte[] bkeysn = new byte[16];
                byte[] bivsn = new byte[16];
                for (int i = 0; i < 16; i++)
                {
                    bkeysn[i] = byte.Parse(sKeySN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bivsn[i] = byte.Parse(sIVSN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }

                // cek ssn
                string stempsn = "";
                ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
                foreach (ManagementObject mo in search.Get())
                {
                    if (mo["InterfaceType"].ToString() != "USB")
                    {
                        string stemp1 = "", stemp2 = "";
                        try { stemp1 = mo["SerialNumber"].ToString(); } catch { };
                        try { stemp2 = mo["Model"].ToString(); } catch { };

                        stempsn = stemp1 + stemp2;
                    }
                }

                byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
                byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                if (bdatatemp.Length >= 32)
                    Array.Copy(bdatatemp, bdata, 32);
                else
                    Array.Copy(bdatatemp, bdata, bdatatemp.Length);

                //byte[] bhasil = AESEncrypt(new byte[] { 0x24, 0x05, 0x24, 0x18, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);
                byte[] bhasil = AESEncrypt(bkeysn, bivsn, bdata);

                stempsn = "";
                for (int i = 0; i < bhasil.Length; i++)
                    stempsn += bhasil[i].ToString("X02");

                if (ssn != stempsn)     // SN tersimpan tidak valid
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }
        // validasi nilai yg tersimpan -- error : 0 .. kalo ga sama false, kalo sama true
        public static bool CekValueRegNActKey()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey rktemp = rk.OpenSubKey(sRegPath);
                string sdp = (string)rktemp.GetValue("dp");
                string ssn = (string)rktemp.GetValue("sn");
                string sak = (string)rktemp.GetValue("ak");

                byte[] bkeysn = new byte[16];
                byte[] bivsn = new byte[16];
                byte[] bkeyfd = new byte[16];
                byte[] bivfd = new byte[16];
                byte[] bakkeysn = new byte[16];
                byte[] bakivsn = new byte[16];
                byte[] bakkeyfd = new byte[16];
                byte[] bakivfd = new byte[16];
                byte[] bkeycmac = new byte[16];
                for (int i = 0; i < 16; i++)
                {
                    bkeysn[i] = byte.Parse(sKeySN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bivsn[i] = byte.Parse(sIVSN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bkeyfd[i] = byte.Parse(sKeyFD.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bivfd[i] = byte.Parse(sIVFD.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bakkeysn[i] = byte.Parse(sKeyActSN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bakivsn[i] = byte.Parse(sIVActSN.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bakkeyfd[i] = byte.Parse(sKeyActFD.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bakivfd[i] = byte.Parse(sIVActFD.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    bkeycmac[i] = byte.Parse(sKeyCMAC.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }

                // cek ssn
                string stempsn = "";
                ManagementObjectSearcher search = new ManagementObjectSearcher("select * from win32_diskdrive");
                foreach (ManagementObject mo in search.Get())
                {
                    if (mo["InterfaceType"].ToString() != "USB")
                    {
                        string stemp1 = "", stemp2 = "";
                        try { stemp1 = mo["SerialNumber"].ToString(); } catch { };
                        try { stemp2 = mo["Model"].ToString(); } catch { };

                        stempsn = stemp1 + stemp2;
                    }

                }

                byte[] bdatatemp = ASCIIEncoding.ASCII.GetBytes(stempsn);
                byte[] bdata = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                if (bdatatemp.Length >= 32)
                    Array.Copy(bdatatemp, bdata, 32);
                else
                    Array.Copy(bdatatemp, bdata, bdatatemp.Length);

                //byte[] bhasil = AESEncrypt(new byte[] { 0x24, 0x05, 0x24, 0x18, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);
                byte[] bhasil = AESEncrypt(bkeysn, bivsn, bdata);

                stempsn = "";
                for (int i = 0; i < bhasil.Length; i++)
                    stempsn += bhasil[i].ToString("X02");

                if (ssn != stempsn)     // SN tersimpan tidak valid
                    return false;

                // dekrip dulu
                bdata = new byte[sdp.Length / 2];
                for (int i = 0; i < bdata.Length; i++)
                    bdata[i] = byte.Parse(sdp.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

                //byte[] bori = AESDecrypt(new byte[] { 0xAA, 0x78, 0x13, 0xFC, 0x67, 0x11, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x22, 0x76, 0x09, 0x1C, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata);
                byte[] bori = AESDecrypt(bkeyfd, bivfd, bdata);

                string stemp = ASCIIEncoding.ASCII.GetString(bori);
                sdp = stemp;

                bdata = new byte[ssn.Length / 2];
                for (int i = 0; i < bdata.Length; i++)
                    bdata[i] = byte.Parse(ssn.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

                //bori = AESDecrypt(new byte[] { 0x24, 0x05, 0x24, 0x18, 0xF5, 0x45, 0x57, 0x87, 0x92, 0x24, 0x38, 0xC4, 0xB1, 0x2F, 0x32, 0x11 }, new byte[] { 0x44, 0x78, 0x13, 0xFC, 0x67, 0xBA, 0x23, 0x5A, 0x2F, 0x32, 0x11, 0x23, 0x78, 0xAB, 0x22, 0x76 }, bdata);
                bori = AESDecrypt(bkeysn, bivsn, bdata);

                stemp = ASCIIEncoding.ASCII.GetString(bori);
                ssn = stemp;
                // hitung actv key
                byte[] bdata1 = ASCIIEncoding.ASCII.GetBytes(sdp);
                bdatatemp = ASCIIEncoding.ASCII.GetBytes(ssn);
                byte[] bdata2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                if (bdatatemp.Length >= 32)
                    Array.Copy(bdatatemp, bdata2, 32);
                else
                    Array.Copy(bdatatemp, bdata2, bdatatemp.Length);

                //byte[] benc = AESEncrypt(new byte[] { 0xAA, 0x78, 0x13, 0xFC, 0x67, 0x11, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata1);
                byte[] benc = AESEncrypt(bakkeyfd, bakivfd, bdata1);
                string str = "";
                for (int i = 0; i < benc.Length; i++)
                    str += benc[i].ToString("X02");

                //benc = AESEncrypt(new byte[] { 0xAA, 0x78, 0x13, 0xFC, 0x67, 0x11, 0x23, 0x5A, 0x6C, 0xFF, 0x11, 0xD2, 0x56, 0x7D, 0x33, 0xA2 }, new byte[] { 0x23, 0x78, 0xAB, 0x55, 0x76, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x7D, 0x91, 0xA2 }, bdata2);
                benc = AESEncrypt(bakkeysn, bakivsn, bdata2);
                for (int i = 0; i < benc.Length; i++)
                    str += benc[i].ToString("X02");

                byte[] bhitung = ASCIIEncoding.ASCII.GetBytes(str);
                //byte[] bmac = AESCMAC(new byte[] { 0xAA, 0x78, 0x13, 0xFC, 0x67, 0x11, 0x26, 0x1C, 0x5A, 0x6C, 0xFF, 0x68, 0xD2, 0x56, 0x33, 0xA2 }, bhitung);
                byte[] bmac = AESCMAC(bkeycmac, bhitung);

                str = "";
                for (int i = 0; i < bmac.Length; i++)
                    str += bmac[i].ToString("X02");
                // compare
                if (str == sak)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        // ambil sn saja
        public static string GetValueSN()
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey(sRegPath);
            return (string)rktemp.GetValue("dp") + " - " + (string)rktemp.GetValue("sn");
        }
        // reset ak
        public static void ResetActKey()
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey rktemp = rk.OpenSubKey(sRegPath, true);

            rktemp.SetValue("ak", "");
        }

        #endregion
    }

}

