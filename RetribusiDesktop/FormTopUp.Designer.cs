﻿namespace RetribusiDesktop
{
    partial class FormTopUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTopUp));
            this.timerTopUp = new System.Windows.Forms.Timer(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labelSaldo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.b20 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.b50 = new System.Windows.Forms.Button();
            this.labelNpwrd = new System.Windows.Forms.Label();
            this.b100 = new System.Windows.Forms.Button();
            this.b200 = new System.Windows.Forms.Button();
            this.b500 = new System.Windows.Forms.Button();
            this.labelNama = new System.Windows.Forms.Label();
            this.labelSilakan = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timerTopUp
            // 
            this.timerTopUp.Tick += new System.EventHandler(this.timerTopUp_Tick);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExit.BackgroundImage")));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Location = new System.Drawing.Point(1301, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(56, 52);
            this.btnExit.TabIndex = 35;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseEnter += new System.EventHandler(this.btnExit_MouseEnter);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Georgia", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(339, 401);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(364, 41);
            this.label2.TabIndex = 55;
            this.label2.Text = "NAMA PEDAGANG";
            this.label2.Visible = false;
            // 
            // labelSaldo
            // 
            this.labelSaldo.AutoSize = true;
            this.labelSaldo.BackColor = System.Drawing.Color.Transparent;
            this.labelSaldo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelSaldo.Font = new System.Drawing.Font("Arial", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSaldo.ForeColor = System.Drawing.Color.Black;
            this.labelSaldo.Location = new System.Drawing.Point(725, 464);
            this.labelSaldo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSaldo.Name = "labelSaldo";
            this.labelSaldo.Size = new System.Drawing.Size(149, 42);
            this.labelSaldo.TabIndex = 57;
            this.labelSaldo.Text = "SALDO";
            this.labelSaldo.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Georgia", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(339, 336);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 41);
            this.label1.TabIndex = 43;
            this.label1.Text = "ID NPWRD";
            this.label1.Visible = false;
            // 
            // b20
            // 
            this.b20.BackColor = System.Drawing.Color.Transparent;
            this.b20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("b20.BackgroundImage")));
            this.b20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.b20.FlatAppearance.BorderSize = 0;
            this.b20.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.b20.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.b20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b20.Font = new System.Drawing.Font("Georgia", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b20.ForeColor = System.Drawing.Color.Transparent;
            this.b20.Location = new System.Drawing.Point(100, 587);
            this.b20.Name = "b20";
            this.b20.Size = new System.Drawing.Size(210, 90);
            this.b20.TabIndex = 50;
            this.b20.Text = "Rp 20.000";
            this.b20.UseVisualStyleBackColor = false;
            this.b20.Click += new System.EventHandler(this.b20_Click);
            this.b20.MouseEnter += new System.EventHandler(this.b20_MouseEnter);
            this.b20.MouseLeave += new System.EventHandler(this.b20_MouseLeave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Georgia", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(339, 465);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 41);
            this.label4.TabIndex = 47;
            this.label4.Text = "SALDO";
            this.label4.Visible = false;
            // 
            // b50
            // 
            this.b50.BackColor = System.Drawing.Color.Transparent;
            this.b50.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("b50.BackgroundImage")));
            this.b50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.b50.FlatAppearance.BorderSize = 0;
            this.b50.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.b50.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.b50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b50.Font = new System.Drawing.Font("Georgia", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b50.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.b50.Location = new System.Drawing.Point(337, 587);
            this.b50.Name = "b50";
            this.b50.Size = new System.Drawing.Size(210, 90);
            this.b50.TabIndex = 51;
            this.b50.Text = "Rp 50.000";
            this.b50.UseVisualStyleBackColor = false;
            this.b50.Click += new System.EventHandler(this.b50_Click);
            this.b50.MouseEnter += new System.EventHandler(this.b50_MouseEnter);
            this.b50.MouseLeave += new System.EventHandler(this.b50_MouseLeave);
            // 
            // labelNpwrd
            // 
            this.labelNpwrd.AutoSize = true;
            this.labelNpwrd.BackColor = System.Drawing.Color.Transparent;
            this.labelNpwrd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelNpwrd.Font = new System.Drawing.Font("Arial", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNpwrd.ForeColor = System.Drawing.Color.Black;
            this.labelNpwrd.Location = new System.Drawing.Point(725, 335);
            this.labelNpwrd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNpwrd.Name = "labelNpwrd";
            this.labelNpwrd.Size = new System.Drawing.Size(206, 42);
            this.labelNpwrd.TabIndex = 56;
            this.labelNpwrd.Text = "ID NPWRD";
            this.labelNpwrd.Visible = false;
            // 
            // b100
            // 
            this.b100.BackColor = System.Drawing.Color.Transparent;
            this.b100.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("b100.BackgroundImage")));
            this.b100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.b100.FlatAppearance.BorderSize = 0;
            this.b100.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.b100.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.b100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b100.Font = new System.Drawing.Font("Georgia", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b100.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.b100.Location = new System.Drawing.Point(576, 587);
            this.b100.Name = "b100";
            this.b100.Size = new System.Drawing.Size(210, 90);
            this.b100.TabIndex = 52;
            this.b100.Text = "Rp 100.000";
            this.b100.UseVisualStyleBackColor = false;
            this.b100.Click += new System.EventHandler(this.b100_Click);
            this.b100.MouseEnter += new System.EventHandler(this.b100_MouseEnter);
            this.b100.MouseLeave += new System.EventHandler(this.b100_MouseLeave);
            // 
            // b200
            // 
            this.b200.BackColor = System.Drawing.Color.Transparent;
            this.b200.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("b200.BackgroundImage")));
            this.b200.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.b200.FlatAppearance.BorderSize = 0;
            this.b200.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.b200.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.b200.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b200.Font = new System.Drawing.Font("Georgia", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b200.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.b200.Location = new System.Drawing.Point(814, 587);
            this.b200.Name = "b200";
            this.b200.Size = new System.Drawing.Size(210, 90);
            this.b200.TabIndex = 53;
            this.b200.Text = "Rp 200.000";
            this.b200.UseVisualStyleBackColor = false;
            this.b200.Click += new System.EventHandler(this.b200_Click);
            this.b200.MouseEnter += new System.EventHandler(this.b200_MouseEnter);
            this.b200.MouseLeave += new System.EventHandler(this.b200_MouseLeave);
            // 
            // b500
            // 
            this.b500.BackColor = System.Drawing.Color.Transparent;
            this.b500.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("b500.BackgroundImage")));
            this.b500.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.b500.Cursor = System.Windows.Forms.Cursors.Default;
            this.b500.FlatAppearance.BorderSize = 0;
            this.b500.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.b500.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.b500.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.b500.Font = new System.Drawing.Font("Georgia", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b500.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.b500.Location = new System.Drawing.Point(1053, 587);
            this.b500.Name = "b500";
            this.b500.Size = new System.Drawing.Size(210, 90);
            this.b500.TabIndex = 54;
            this.b500.Text = "Rp 500.000";
            this.b500.UseVisualStyleBackColor = false;
            this.b500.Click += new System.EventHandler(this.b500_Click);
            this.b500.MouseEnter += new System.EventHandler(this.b500_MouseEnter);
            this.b500.MouseLeave += new System.EventHandler(this.b500_MouseLeave);
            // 
            // labelNama
            // 
            this.labelNama.AutoSize = true;
            this.labelNama.BackColor = System.Drawing.Color.Transparent;
            this.labelNama.Font = new System.Drawing.Font("Arial", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNama.ForeColor = System.Drawing.Color.Black;
            this.labelNama.Location = new System.Drawing.Point(725, 400);
            this.labelNama.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNama.Name = "labelNama";
            this.labelNama.Size = new System.Drawing.Size(354, 42);
            this.labelNama.TabIndex = 59;
            this.labelNama.Text = "NAMA PEDAGANG";
            this.labelNama.Visible = false;
            // 
            // labelSilakan
            // 
            this.labelSilakan.AutoSize = true;
            this.labelSilakan.BackColor = System.Drawing.Color.Transparent;
            this.labelSilakan.Font = new System.Drawing.Font("Georgia", 48F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSilakan.ForeColor = System.Drawing.Color.Blue;
            this.labelSilakan.Location = new System.Drawing.Point(334, 234);
            this.labelSilakan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSilakan.Name = "labelSilakan";
            this.labelSilakan.Size = new System.Drawing.Size(711, 72);
            this.labelSilakan.TabIndex = 60;
            this.labelSilakan.Text = "SILAKAN  TAP  KARTU";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RosyBrown;
            this.label3.Location = new System.Drawing.Point(1127, 724);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(223, 14);
            this.label3.TabIndex = 83;
            this.label3.Text = "Top Up E-Retribusi Bank Jateng v.2.0";
            // 
            // FormTopUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1362, 764);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.b20);
            this.Controls.Add(this.labelSilakan);
            this.Controls.Add(this.b200);
            this.Controls.Add(this.b100);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.b50);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.b500);
            this.Controls.Add(this.labelNama);
            this.Controls.Add(this.labelSaldo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelNpwrd);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Transparent;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormTopUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrasi dan Pengisian";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTopUp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerTopUp;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelSaldo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button b20;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button b50;
        private System.Windows.Forms.Label labelNpwrd;
        private System.Windows.Forms.Button b100;
        private System.Windows.Forms.Button b200;
        private System.Windows.Forms.Button b500;
        private System.Windows.Forms.Label labelNama;
        private System.Windows.Forms.Label labelSilakan;
        private System.Windows.Forms.Label label3;
    }
}

