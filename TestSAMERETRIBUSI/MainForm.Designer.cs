﻿namespace TestSAMERETRIBUSI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tbData = new System.Windows.Forms.TextBox();
            this.btnReadData = new System.Windows.Forms.Button();
            this.btnWriteSector1 = new System.Windows.Forms.Button();
            this.lblFileAddress = new System.Windows.Forms.Label();
            this.tbFileAddress = new System.Windows.Forms.TextBox();
            this.lblData = new System.Windows.Forms.Label();
            this.lblStartOffset = new System.Windows.Forms.Label();
            this.tbStartOffset = new System.Windows.Forms.TextBox();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.lblLength = new System.Windows.Forms.Label();
            this.lbCount = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDIVCard = new System.Windows.Forms.Button();
            this.btnAuthSAM = new System.Windows.Forms.Button();
            this.btnConnectCARD = new System.Windows.Forms.Button();
            this.btnConnectSAM = new System.Windows.Forms.Button();
            this.btnTEST = new System.Windows.Forms.Button();
            this.btnInfo = new System.Windows.Forms.Button();
            this.tbSNCard = new System.Windows.Forms.TextBox();
            this.lblSNCard = new System.Windows.Forms.Label();
            this.lblUIDCARD = new System.Windows.Forms.Label();
            this.tbUIDCARD = new System.Windows.Forms.TextBox();
            this.lblSNSAM = new System.Windows.Forms.Label();
            this.tbSNSAM = new System.Windows.Forms.TextBox();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.menuBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbData
            // 
            this.tbData.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbData.Location = new System.Drawing.Point(107, 99);
            this.tbData.Margin = new System.Windows.Forms.Padding(4);
            this.tbData.MaxLength = 1024;
            this.tbData.Name = "tbData";
            this.tbData.Size = new System.Drawing.Size(1015, 23);
            this.tbData.TabIndex = 13;
            this.tbData.TextChanged += new System.EventHandler(this.tbData_TextChanged);
            this.tbData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbData_KeyPress);
            // 
            // btnReadData
            // 
            this.btnReadData.Location = new System.Drawing.Point(634, 130);
            this.btnReadData.Margin = new System.Windows.Forms.Padding(4);
            this.btnReadData.Name = "btnReadData";
            this.btnReadData.Size = new System.Drawing.Size(133, 28);
            this.btnReadData.TabIndex = 19;
            this.btnReadData.Text = "Baca Data";
            this.btnReadData.UseVisualStyleBackColor = true;
            this.btnReadData.Click += new System.EventHandler(this.btnReadData_Click);
            // 
            // btnWriteSector1
            // 
            this.btnWriteSector1.Location = new System.Drawing.Point(775, 130);
            this.btnWriteSector1.Margin = new System.Windows.Forms.Padding(4);
            this.btnWriteSector1.Name = "btnWriteSector1";
            this.btnWriteSector1.Size = new System.Drawing.Size(133, 28);
            this.btnWriteSector1.TabIndex = 20;
            this.btnWriteSector1.Text = "Tulis Data";
            this.btnWriteSector1.UseVisualStyleBackColor = true;
            this.btnWriteSector1.Click += new System.EventHandler(this.btnWriteData_Click);
            // 
            // lblFileAddress
            // 
            this.lblFileAddress.AutoSize = true;
            this.lblFileAddress.Location = new System.Drawing.Point(14, 43);
            this.lblFileAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFileAddress.Name = "lblFileAddress";
            this.lblFileAddress.Size = new System.Drawing.Size(86, 17);
            this.lblFileAddress.TabIndex = 6;
            this.lblFileAddress.Text = "File Address";
            // 
            // tbFileAddress
            // 
            this.tbFileAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbFileAddress.Location = new System.Drawing.Point(107, 40);
            this.tbFileAddress.MaxLength = 4;
            this.tbFileAddress.Name = "tbFileAddress";
            this.tbFileAddress.Size = new System.Drawing.Size(100, 23);
            this.tbFileAddress.TabIndex = 7;
            this.tbFileAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFileAddress_KeyPress);
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(14, 102);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(38, 17);
            this.lblData.TabIndex = 12;
            this.lblData.Text = "Data";
            // 
            // lblStartOffset
            // 
            this.lblStartOffset.AutoSize = true;
            this.lblStartOffset.Location = new System.Drawing.Point(14, 72);
            this.lblStartOffset.Name = "lblStartOffset";
            this.lblStartOffset.Size = new System.Drawing.Size(80, 17);
            this.lblStartOffset.TabIndex = 8;
            this.lblStartOffset.Text = "Start Offset";
            // 
            // tbStartOffset
            // 
            this.tbStartOffset.Location = new System.Drawing.Point(107, 69);
            this.tbStartOffset.MaxLength = 4;
            this.tbStartOffset.Name = "tbStartOffset";
            this.tbStartOffset.Size = new System.Drawing.Size(100, 23);
            this.tbStartOffset.TabIndex = 9;
            this.tbStartOffset.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbStartOffset_KeyPress);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(306, 69);
            this.tbLength.MaxLength = 4;
            this.tbLength.Name = "tbLength";
            this.tbLength.Size = new System.Drawing.Size(100, 23);
            this.tbLength.TabIndex = 11;
            this.tbLength.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbLength_KeyPress);
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(213, 72);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(52, 17);
            this.lblLength.TabIndex = 10;
            this.lblLength.Text = "Length";
            // 
            // lbCount
            // 
            this.lbCount.AutoSize = true;
            this.lbCount.Location = new System.Drawing.Point(1129, 102);
            this.lbCount.Name = "lbCount";
            this.lbCount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbCount.Size = new System.Drawing.Size(16, 17);
            this.lbCount.TabIndex = 10;
            this.lbCount.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnDIVCard);
            this.panel1.Controls.Add(this.btnAuthSAM);
            this.panel1.Controls.Add(this.btnConnectCARD);
            this.panel1.Controls.Add(this.btnConnectSAM);
            this.panel1.Controls.Add(this.btnTEST);
            this.panel1.Controls.Add(this.btnInfo);
            this.panel1.Controls.Add(this.tbSNCard);
            this.panel1.Controls.Add(this.lblSNCard);
            this.panel1.Controls.Add(this.lblUIDCARD);
            this.panel1.Controls.Add(this.tbUIDCARD);
            this.panel1.Controls.Add(this.lblSNSAM);
            this.panel1.Controls.Add(this.tbSNSAM);
            this.panel1.Controls.Add(this.lblFileAddress);
            this.panel1.Controls.Add(this.lbCount);
            this.panel1.Controls.Add(this.tbData);
            this.panel1.Controls.Add(this.btnReadData);
            this.panel1.Controls.Add(this.tbLength);
            this.panel1.Controls.Add(this.btnWriteSector1);
            this.panel1.Controls.Add(this.lblLength);
            this.panel1.Controls.Add(this.tbFileAddress);
            this.panel1.Controls.Add(this.tbStartOffset);
            this.panel1.Controls.Add(this.lblData);
            this.panel1.Controls.Add(this.lblStartOffset);
            this.panel1.Location = new System.Drawing.Point(1, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1183, 174);
            this.panel1.TabIndex = 1;
            // 
            // btnDIVCard
            // 
            this.btnDIVCard.Location = new System.Drawing.Point(514, 130);
            this.btnDIVCard.Name = "btnDIVCard";
            this.btnDIVCard.Size = new System.Drawing.Size(113, 28);
            this.btnDIVCard.TabIndex = 18;
            this.btnDIVCard.Text = "DIV KEY";
            this.btnDIVCard.UseVisualStyleBackColor = true;
            this.btnDIVCard.Click += new System.EventHandler(this.btnDIVCard_Click);
            // 
            // btnAuthSAM
            // 
            this.btnAuthSAM.Location = new System.Drawing.Point(395, 130);
            this.btnAuthSAM.Name = "btnAuthSAM";
            this.btnAuthSAM.Size = new System.Drawing.Size(113, 28);
            this.btnAuthSAM.TabIndex = 17;
            this.btnAuthSAM.Text = "Auth SAM";
            this.btnAuthSAM.UseVisualStyleBackColor = true;
            this.btnAuthSAM.Click += new System.EventHandler(this.btnAuthSAM_Click);
            // 
            // btnConnectCARD
            // 
            this.btnConnectCARD.Location = new System.Drawing.Point(276, 130);
            this.btnConnectCARD.Name = "btnConnectCARD";
            this.btnConnectCARD.Size = new System.Drawing.Size(113, 28);
            this.btnConnectCARD.TabIndex = 16;
            this.btnConnectCARD.Text = "Connect Card";
            this.btnConnectCARD.UseVisualStyleBackColor = true;
            this.btnConnectCARD.Click += new System.EventHandler(this.btnConnectCARD_Click);
            // 
            // btnConnectSAM
            // 
            this.btnConnectSAM.Location = new System.Drawing.Point(157, 130);
            this.btnConnectSAM.Name = "btnConnectSAM";
            this.btnConnectSAM.Size = new System.Drawing.Size(113, 28);
            this.btnConnectSAM.TabIndex = 15;
            this.btnConnectSAM.Text = "Connect SAM";
            this.btnConnectSAM.UseVisualStyleBackColor = true;
            this.btnConnectSAM.Click += new System.EventHandler(this.btnConnectSAM_Click);
            // 
            // btnTEST
            // 
            this.btnTEST.Location = new System.Drawing.Point(924, 130);
            this.btnTEST.Name = "btnTEST";
            this.btnTEST.Size = new System.Drawing.Size(97, 28);
            this.btnTEST.TabIndex = 21;
            this.btnTEST.Text = "TEST";
            this.btnTEST.UseVisualStyleBackColor = true;
            this.btnTEST.Click += new System.EventHandler(this.btnTEST_Click);
            // 
            // btnInfo
            // 
            this.btnInfo.Location = new System.Drawing.Point(17, 130);
            this.btnInfo.Margin = new System.Windows.Forms.Padding(4);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(133, 28);
            this.btnInfo.TabIndex = 14;
            this.btnInfo.Text = "Info Kartu";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // tbSNCard
            // 
            this.tbSNCard.BackColor = System.Drawing.Color.White;
            this.tbSNCard.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSNCard.Enabled = false;
            this.tbSNCard.Location = new System.Drawing.Point(498, 11);
            this.tbSNCard.Name = "tbSNCard";
            this.tbSNCard.ReadOnly = true;
            this.tbSNCard.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbSNCard.Size = new System.Drawing.Size(168, 23);
            this.tbSNCard.TabIndex = 5;
            // 
            // lblSNCard
            // 
            this.lblSNCard.AutoSize = true;
            this.lblSNCard.Location = new System.Drawing.Point(423, 14);
            this.lblSNCard.Name = "lblSNCard";
            this.lblSNCard.Size = new System.Drawing.Size(69, 17);
            this.lblSNCard.TabIndex = 4;
            this.lblSNCard.Text = "SN CARD";
            // 
            // lblUIDCARD
            // 
            this.lblUIDCARD.AutoSize = true;
            this.lblUIDCARD.Location = new System.Drawing.Point(213, 14);
            this.lblUIDCARD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUIDCARD.Name = "lblUIDCARD";
            this.lblUIDCARD.Size = new System.Drawing.Size(73, 17);
            this.lblUIDCARD.TabIndex = 2;
            this.lblUIDCARD.Text = "UID CARD";
            // 
            // tbUIDCARD
            // 
            this.tbUIDCARD.BackColor = System.Drawing.Color.White;
            this.tbUIDCARD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbUIDCARD.Enabled = false;
            this.tbUIDCARD.Location = new System.Drawing.Point(306, 11);
            this.tbUIDCARD.Name = "tbUIDCARD";
            this.tbUIDCARD.ReadOnly = true;
            this.tbUIDCARD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbUIDCARD.Size = new System.Drawing.Size(100, 23);
            this.tbUIDCARD.TabIndex = 3;
            // 
            // lblSNSAM
            // 
            this.lblSNSAM.AutoSize = true;
            this.lblSNSAM.Location = new System.Drawing.Point(14, 14);
            this.lblSNSAM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSNSAM.Name = "lblSNSAM";
            this.lblSNSAM.Size = new System.Drawing.Size(60, 17);
            this.lblSNSAM.TabIndex = 0;
            this.lblSNSAM.Text = "SN SAM";
            // 
            // tbSNSAM
            // 
            this.tbSNSAM.BackColor = System.Drawing.Color.White;
            this.tbSNSAM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSNSAM.Enabled = false;
            this.tbSNSAM.Location = new System.Drawing.Point(107, 11);
            this.tbSNSAM.Name = "tbSNSAM";
            this.tbSNSAM.ReadOnly = true;
            this.tbSNSAM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbSNSAM.Size = new System.Drawing.Size(100, 23);
            this.tbSNSAM.TabIndex = 1;
            // 
            // menuBar
            // 
            this.menuBar.BackColor = System.Drawing.Color.Gainsboro;
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.configurationToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1188, 24);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.exitToolStripMenuItem.Text = "Keluar";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetToolStripMenuItem,
            this.settingToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.configurationToolStripMenuItem.Text = "Konfigurasi";
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.settingToolStripMenuItem.Text = "Pengaturan";
            this.settingToolStripMenuItem.Click += new System.EventHandler(this.settingToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1188, 202);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuBar;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sample Baca Tulis Kartu";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbData;
        private System.Windows.Forms.Button btnReadData;
        private System.Windows.Forms.Button btnWriteSector1;
        private System.Windows.Forms.Label lblFileAddress;
        private System.Windows.Forms.TextBox tbFileAddress;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblStartOffset;
        private System.Windows.Forms.TextBox tbStartOffset;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Label lbCount;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.Label lblUIDCARD;
        private System.Windows.Forms.TextBox tbUIDCARD;
        private System.Windows.Forms.Label lblSNSAM;
        private System.Windows.Forms.TextBox tbSNSAM;
        private System.Windows.Forms.TextBox tbSNCard;
        private System.Windows.Forms.Label lblSNCard;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Button btnTEST;
        private System.Windows.Forms.Button btnConnectCARD;
        private System.Windows.Forms.Button btnConnectSAM;
        private System.Windows.Forms.Button btnDIVCard;
        private System.Windows.Forms.Button btnAuthSAM;
    }
}

