﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using libCard;

namespace TestSAMERETRIBUSI
{
    public partial class SettingReader : Form
    {
        private LibCard smartCard;

        XmlDocument xSetting = new XmlDocument();
        XmlNode xNode;
        string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        List<string> reader = new List<string>();

        public SettingReader()
        {
            InitializeComponent();
            smartCard = new LibCard();
        }

        public void initializeSetting()
        {
            xSetting.Load(appPath + @"\setting.xml");
            xNode = xSetting.SelectSingleNode("UserSetting");
            GetSetting();
        }

        public void GetSetting()
        {
            string card = "";
            string sam = "";

            card = xNode["CARD"].InnerText;
            sam = xNode["SAM"].InnerText;

            SetReader(card, sam);
        }

        public void SetReader(string card, string sam)
        {
            cbCARD.Text = card;
            cbSAM.Text = sam;
        }

        private void SettingReader_Load(object sender, EventArgs e)
        {
            initializeSetting();

            IList readerSAM = this.cbSAM.Items;
            IList readerCARD = this.cbCARD.Items;

            try
            {
                smartCard.getReaderName(readerSAM);
                smartCard.getReaderName(readerCARD);
            }
            catch
            {
                MessageBox.Show("Tidak ada reader yang terkoneksi di komputer", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            xNode["CARD"].InnerText = cbCARD.Text;
            xNode["SAM"].InnerText = cbSAM.Text;
            xSetting.Save(appPath + @"\setting.xml");

            MessageBox.Show("Sukses menyimpan reader", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnCheckSAM_Click(object sender, EventArgs e)
        {
            if (!smartCard.TryConnectSAMCard(cbSAM.Text))
            {
                MessageBox.Show("Kartu tidak ditemukan", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Kartu ditemukan", "Selesai", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCheckCARD_Click(object sender, EventArgs e)
        {
            if (!smartCard.TryConnectCard(cbCARD.Text))
            {
                MessageBox.Show("Kartu tidak ditemukan", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Kartu ditemukan", "Selesai", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
