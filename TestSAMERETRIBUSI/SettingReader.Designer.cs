﻿namespace TestSAMERETRIBUSI
{
    partial class SettingReader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingReader));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCheckCARD = new System.Windows.Forms.Button();
            this.btnCheckSAM = new System.Windows.Forms.Button();
            this.cbSAM = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cbCARD = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnCheckCARD);
            this.panel1.Controls.Add(this.btnCheckSAM);
            this.panel1.Controls.Add(this.cbSAM);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.cbCARD);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(16, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(585, 134);
            this.panel1.TabIndex = 0;
            // 
            // btnCheckCARD
            // 
            this.btnCheckCARD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.btnCheckCARD.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCheckCARD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckCARD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckCARD.Location = new System.Drawing.Point(503, 46);
            this.btnCheckCARD.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheckCARD.Name = "btnCheckCARD";
            this.btnCheckCARD.Size = new System.Drawing.Size(61, 24);
            this.btnCheckCARD.TabIndex = 22;
            this.btnCheckCARD.Text = "Check";
            this.btnCheckCARD.UseVisualStyleBackColor = false;
            this.btnCheckCARD.Click += new System.EventHandler(this.btnCheckCARD_Click);
            // 
            // btnCheckSAM
            // 
            this.btnCheckSAM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.btnCheckSAM.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCheckSAM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckSAM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckSAM.Location = new System.Drawing.Point(503, 14);
            this.btnCheckSAM.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheckSAM.Name = "btnCheckSAM";
            this.btnCheckSAM.Size = new System.Drawing.Size(61, 24);
            this.btnCheckSAM.TabIndex = 21;
            this.btnCheckSAM.Text = "Check";
            this.btnCheckSAM.UseVisualStyleBackColor = false;
            this.btnCheckSAM.Click += new System.EventHandler(this.btnCheckSAM_Click);
            // 
            // cbSAM
            // 
            this.cbSAM.FormattingEnabled = true;
            this.cbSAM.Location = new System.Drawing.Point(153, 14);
            this.cbSAM.Margin = new System.Windows.Forms.Padding(4);
            this.cbSAM.Name = "cbSAM";
            this.cbSAM.Size = new System.Drawing.Size(344, 24);
            this.cbSAM.TabIndex = 14;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(409, 78);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(155, 34);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "Simpan";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 50);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 17);
            this.label9.TabIndex = 16;
            this.label9.Text = "CARD";
            // 
            // cbCARD
            // 
            this.cbCARD.FormattingEnabled = true;
            this.cbCARD.Location = new System.Drawing.Point(153, 46);
            this.cbCARD.Margin = new System.Windows.Forms.Padding(4);
            this.cbCARD.Name = "cbCARD";
            this.cbCARD.Size = new System.Drawing.Size(344, 24);
            this.cbCARD.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 18);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 17);
            this.label10.TabIndex = 13;
            this.label10.Text = "SAM";
            // 
            // SettingReader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 165);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingReader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setting Reader";
            this.Load += new System.EventHandler(this.SettingReader_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbSAM;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbCARD;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Button btnCheckSAM;
        public System.Windows.Forms.Button btnCheckCARD;
    }
}