﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using libCard;
using System.Xml;

namespace TestSAMERETRIBUSI
{
    public partial class Reset : Form
    {
        XmlDocument xSetting = new XmlDocument();
        XmlNode xNode;
        string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public Reset()
        {
            InitializeComponent();
        }

        private System.Threading.Thread rd1;
        private LibCard smartCard= new LibCard();
        int retCode = 0;
        bool konekSAM = false;

        private void button1_Click(object sender, EventArgs e)
        {
            panel1.BackColor = Color.Yellow;
            rd1 = new System.Threading.Thread(thread1);
            rd1.Start();
            
        }
        private void thread1()
        {
            if (!smartCard.ConnectSAMCard()) { MessageBox.Show("Tidak dapat terkoneksi dengan kartu SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.AuthSAM()) { MessageBox.Show("Kartu SAM tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }

            if (!smartCard.ConnectCard()) { MessageBox.Show("Tidak dapat membaca Kartu Samsat", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.DivKeyCard()) { MessageBox.Show("Kartu Samsat tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }

            retCode = smartCard.ResetCard();

            if (retCode != 0) { MessageBox.Show("Kartu tidak valid: " + retCode.ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            else
            {
                panel1.BackColor = Color.Green;
                MessageBox.Show("Finish: " + retCode.ToString(), "Selesai", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (konekSAM == false)
            {
                MessageBox.Show("Mohon tekan tombol Konek SAM terlebih dahulu", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (tbSNSAM.Text.Length != 12)
            {
                MessageBox.Show("Panjang SN harus 6byte", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbKEY.Text.Length != 32)
            {
                MessageBox.Show("Panjang Kunci harus 16byte", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!(smartCard.SetKey(tbSNSAM.Text, tbKEY.Text)))
            {
                MessageBox.Show("Gagal memasukkan kunci", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                MessageBox.Show("Sukses menyimpan kunci", "Selesai", MessageBoxButtons.OK, MessageBoxIcon.Information);
                xNode["DATA"].InnerText = cryp.encrypt(tbKEY.Text);
                xSetting.Save(appPath + @"\setting.xml");
                tbKEY.PasswordChar = '*';
                tbKEY.Enabled = false;
                konekSAM = false;
                tbSNSAM.Enabled = false;
                btnSave.Enabled = false;
            }
        }

        private void tbSNSAM_KeyPress(object sender, KeyPressEventArgs e)
        {
            // this will only allow valid hex values [0-9][a-f][A-F] to be entered. See ASCII table
            char c = e.KeyChar;
            if (c != '\b' && !((c <= 0x66 && c >= 0x61) || (c <= 0x46 && c >= 0x41) || (c >= 0x30 && c <= 0x39) || (c == 0x2c)))
            {
                e.Handled = true;
            }
        }

        private void tbKEY_KeyPress(object sender, KeyPressEventArgs e)
        {
            // this will only allow valid hex values [0-9][a-f][A-F] to be entered. See ASCII table
            char c = e.KeyChar;
            if (c != '\b' && !((c <= 0x66 && c >= 0x61) || (c <= 0x46 && c >= 0x41) || (c >= 0x30 && c <= 0x39) || (c == 0x2c)))
            {
                e.Handled = true;
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            xSetting.Load(appPath + @"\setting.xml");
            xNode = xSetting.SelectSingleNode("UserSetting");

            string snsam = "";
            string key="";

            if (!smartCard.ConnectSAMCard()) { MessageBox.Show("Tidak dapat terkoneksi dengan kartu SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); konekSAM = false; Cursor.Current = Cursors.Default; return; }
            konekSAM = true;
            try
            {
                key = cryp.decrypt(xNode["DATA"].InnerText);
            }
            catch
            {
                key = "";
            }
            if (key != "" && key.Length == 32)
            {
                tbKEY.Text = key;
                tbKEY.Enabled = false;
                tbKEY.PasswordChar = '*';
            }
            else
            {
                tbKEY.Enabled = true;
            }
            smartCard.AuthSAM();
            smartCard.GetSNSAM(out snsam);
            if (snsam == "000000000000")
            {
                btnSave.Enabled = true;
                tbSNSAM.Enabled = true;
                tbSNSAM.Text = snsam;
                tbSNSAM.Focus();
                tbSNSAM.SelectAll();                
            }
            else
            {
                tbSNSAM.Text = snsam;
                tbSNSAM.Enabled = false;
                tbKEY.Enabled = false;
                btnSave.Enabled = false;
            }

            Cursor.Current = Cursors.Default;
        }
    }
}
