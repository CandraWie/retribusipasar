﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using libCard;
using LibSmartCard;

namespace TestSAMERETRIBUSI
{
    public partial class MainForm : Form
    {
        private LibCard smartCard;
        int retCode = 0;
        public MainForm()
        {
            InitializeComponent();
            tbFileAddress.Focus(); ;
        }

        public static string TrimInput(string str)
        {
            string tmp = str.Replace(" ", "");
            tmp = tmp.Replace("-", "");
            return tmp;
        }

        private void btnReadData_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (tbFileAddress.Text == "")
            {
                MessageBox.Show("File Address tidak boleh kosong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbFileAddress.Focus();
                return;
            }
            if (tbStartOffset.Text == "")
            {
                MessageBox.Show("Start Offset tidak boleh kosong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbStartOffset.Focus();
                return;
            }
            if (tbLength.Text == "")
            {
                MessageBox.Show("Length tidak boleh kosong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbLength.Focus();
                return;
            }

            string sector = "";
            retCode = smartCard.ReadData(tbFileAddress.Text, tbStartOffset.Text, tbLength.Text, out sector);
            if (retCode == 0)
            {
                tbData.Text = sector;
            }
            else
            {
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
            }
            Cursor.Current = Cursors.Default;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //tbFileAddress.Focus();
            btnTEST.Focus();
            smartCard = new LibCard();
            
            
        }

        private void btnWriteData_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (tbFileAddress.Text == "")
            {
                MessageBox.Show("File Address tidak boleh kosong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbFileAddress.Focus();
                return;
            }
            if (tbStartOffset.Text == "")
            {
                MessageBox.Show("Start Offset tidak boleh kosong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbStartOffset.Focus();
                return;
            }
            if (tbLength.Text == "")
            {
                MessageBox.Show("Length tidak boleh kosong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbLength.Focus();
                return;
            }
            if (tbData.Text == "")
            {
                MessageBox.Show("Data tidak boleh kosong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbData.Focus();
                return;
            }
            if (tbData.Text.Length % 2 != 0)
            {
                MessageBox.Show("Data Length error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbData.Focus();
                return;
            }
            if (tbData.Text.Length / 2 != Convert.ToInt32(tbLength.Text))
            {
                MessageBox.Show("Data Length error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                tbData.Focus();
                return;
            }

            retCode = smartCard.WriteData(tbFileAddress.Text,tbStartOffset.Text,tbLength.Text,tbData.Text);
            if (retCode == 0)
            {
                MessageBox.Show("Sukses menyimpan data", "Selesai", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
            }
            Cursor.Current = Cursors.Default;
        }

        private void tbData_TextChanged(object sender, EventArgs e)
        {
            lbCount.Text = (TrimInput(tbData.Text).Length / 2).ToString();
        }

        private void tbStartOffset_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbLength_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbFileAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            // this will only allow valid hex values [0-9][a-f][A-F] to be entered. See ASCII table
            char c = e.KeyChar;
            if (c != '\b' && !((c <= 0x66 && c >= 0x61) || (c <= 0x46 && c >= 0x41) || (c >= 0x30 && c <= 0x39) || (c == 0x2c)))
            {
                e.Handled = true;
            }
        }

        private void tbData_KeyPress(object sender, KeyPressEventArgs e)
        {
            // this will only allow valid hex values [0-9][a-f][A-F] to be entered. See ASCII table
            char c = e.KeyChar;
            if (c != '\b' && !((c <= 0x66 && c >= 0x61) || (c <= 0x46 && c >= 0x41) || (c >= 0x30 && c <= 0x39) || (c == 0x2c)))
            {
                e.Handled = true;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingReader setting = new SettingReader();
            setting.ShowDialog();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reset resetCard = new Reset();
            resetCard.ShowDialog();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            string snsam = "";
            string uidcard = "";
            string sncard = "";

            Cursor.Current = Cursors.WaitCursor;
            if (!smartCard.ConnectSAMCard()) { MessageBox.Show("Tidak dapat terkoneksi dengan kartu SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.AuthSAM()) { MessageBox.Show("Kartu SAM tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }

            if (!smartCard.ConnectCard()) { MessageBox.Show("Tidak dapat membaca Kartu E-RETRIBUSI", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.DivKeyCard()) { MessageBox.Show("Kartu E-RETRIBUSI tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }

            

            if(!(smartCard.GetSNSAM(out snsam)))
            {
                MessageBox.Show("SAM error", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                tbSNSAM.Text = snsam;
            }

            if (!(smartCard.GetUIDCard(out uidcard)))
            {
                MessageBox.Show("Card  error", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                tbUIDCARD.Text = uidcard;
            }

            if (!smartCard.GetSerialCard(out sncard))
            {
                MessageBox.Show("Card error", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                tbSNCard.Text = sncard;
            }
            Cursor.Current = Cursors.Default;
        }

        private void btnTEST_Click(object sender, EventArgs e)
        {
            string snsam = "";
            string uidcard = "";
            string sncard = "";
            byte[] rdm = new byte[1024];
            Random random = new Random();
            string randomString = "";
            string sector = "";
            int retCode = 0;
            DateTime start = DateTime.Now;
            DateTime end;

            Cursor.Current = Cursors.WaitCursor;
            if (!smartCard.ConnectSAMCard()) { MessageBox.Show("Tidak dapat terkoneksi dengan kartu SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.AuthSAM()) { MessageBox.Show("Kartu SAM tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }

            if (!smartCard.ConnectCard()) { MessageBox.Show("Tidak dapat membaca Kartu Samsat", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.DivKeyCard()) { MessageBox.Show("Kartu Samsat tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }

            if (!(smartCard.GetSNSAM(out snsam)))
            {
                end = DateTime.Now;
                MessageBox.Show("SAM error in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }
            else
            {
                tbSNSAM.Text = snsam;
            }

            if (!(smartCard.GetUIDCard(out uidcard)))
            {
                end = DateTime.Now;
                MessageBox.Show("Card  error in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }
            else
            {
                tbUIDCARD.Text = uidcard;
            }

            if (!smartCard.GetSerialCard(out sncard))
            {
                end = DateTime.Now;
                MessageBox.Show("Card error in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }
            else
            {
                tbSNCard.Text = sncard;
            }

            Application.DoEvents();

            for (int i = 0; i < 1; i++)
            {
                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0001", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0001", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0002", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0002", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0003", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0003", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0004", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0004", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0005", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0005", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0006", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0006", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0007", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0007", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0008", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0008", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("0009", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("0009", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("000A", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("000A", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("000B", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("000B", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("000C", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("000C", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("000D", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("000D", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                random.NextBytes(rdm);
                randomString = Function.ArrayOfByteToHexString(rdm, rdm.Length, "");

                retCode = smartCard.WriteData("000E", "0", "1024", randomString);
                if (retCode != 0)
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                retCode = smartCard.ReadData("000E", "0", "1024", out sector);
                if (retCode == 0)
                {
                    if (randomString != sector)
                    {
                        end = DateTime.Now;
                        MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    end = DateTime.Now;
                    MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }

            byte[] defaultValue = new byte[1024];
            randomString = Function.ArrayOfByteToHexString(defaultValue, defaultValue.Length, "");

            retCode = smartCard.WriteData("0001", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0001", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("0002", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0002", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("0003", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0003", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }


            retCode = smartCard.WriteData("0004", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0004", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("0005", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0005", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("0006", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0006", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("0007", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0007", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("0008", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0008", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("0009", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("0009", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("000A", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("000A", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("000B", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("000B", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("000C", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("000C", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("000D", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("000D", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.WriteData("000E", "0", "1024", randomString);
            if (retCode != 0)
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            retCode = smartCard.ReadData("000E", "0", "1024", out sector);
            if (retCode == 0)
            {
                if (randomString != sector)
                {
                    end = DateTime.Now;
                    MessageBox.Show("Data Not Valid in " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
            }
            else
            {
                end = DateTime.Now;
                MessageBox.Show(libCard.ReturnError.GetErrorMessage(retCode) + " " + (end - start).ToString(), "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;
                return;
            }

            end = DateTime.Now;

            MessageBox.Show("Finish in: " + (end - start).ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Cursor.Current = Cursors.Default;
        }

        private void btnConnectSAM_Click(object sender, EventArgs e)
        {
            if (!smartCard.ConnectSAMCard())
            {
                MessageBox.Show("Tidak dapat terkoneksi dengan kartu SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return;
            }
            else
            {
                MessageBox.Show("Terkoneksi Ke SAM", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void btnConnectCARD_Click(object sender, EventArgs e)
        {
            if (!smartCard.ConnectCard())
            {
                MessageBox.Show("Tidak dapat membaca Kartu E-RETRIBUSI", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return;
            }
            else
            {
                MessageBox.Show("Terkoneksi Ke Kartu E-RETRIBUSI", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAuthSAM_Click(object sender, EventArgs e)
        {
            if (!smartCard.AuthSAM())
            {
                MessageBox.Show("Kartu SAM tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return;
            }
            else
            {
                MessageBox.Show("Authentikasi SAM berhasil", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDIVCard_Click(object sender, EventArgs e)
        {
            if (!smartCard.DivKeyCard())
            {
                MessageBox.Show("Kartu Samsat tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return;
            }
            else
            {
                MessageBox.Show("Authentikasi Kartu E-RETRIBUSI  berhasil", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
