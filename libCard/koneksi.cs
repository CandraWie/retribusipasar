﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace libCard
{
    public static class koneksi
    {
        public static string GetCardReaderName()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            XmlDocument xSetting = new XmlDocument();
            XmlNode xNode;
            xSetting.Load(appPath + @"\setting.xml");
            xNode = xSetting.SelectSingleNode("UserSetting");
            return xNode["CARD"].InnerText;
        }
        public static string GetSAMReaderName()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            XmlDocument xSetting = new XmlDocument();
            XmlNode xNode;
            xSetting.Load(appPath + @"\setting.xml");
            xNode = xSetting.SelectSingleNode("UserSetting");
            return xNode["SAM"].InnerText;

        }
    }

}
