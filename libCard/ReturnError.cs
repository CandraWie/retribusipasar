﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libCard
{
    public static class ReturnError
    {
        public enum Error
        {
            EstablishContext = 1,
            ReleaseContext,
            ConnectCard,
            ConnectSAMCard,
            DisconnectCard,
            DisconnectSAMCard,
            SelectFolder,
            FileAddress,
            AuthReadData,
            AuthWriteData,
            WriteData,
            ReadData,
            EmptyStartOffset,
            LengthData,
            EmptyData,
            DataHexNotValid,
            MaxStartOffset,
            OutOfBonds,
            CheckCard
        };

        public static int GetError(Error er)
        {
            return (int)(-1 * (int)er);
        }

        public static string GetErrorMessage(int kode)
        {
            string kembalian = "";
            switch (kode)
            {
                case -1: kembalian = "Error Establish Context"; break;
                case -2: kembalian = "Error Release Context"; break;
                case -3: kembalian = "Error Connect Card"; break;
                case -4: kembalian = "Error Connect SAM Card"; break;
                case -5: kembalian = "Error Disconnect Card"; break;
                case -6: kembalian = "Error Disconnect SAM Card"; break;
                case -7: kembalian = "Error Don't Have File Address"; break;
                case -8: kembalian = "Error File Address"; break;
                case -9: kembalian = "Error Auth Read Data"; break;
                case -10: kembalian = "Error Auth Write Data"; break;
                case -11: kembalian = "Error Write Data"; break;
                case -12: kembalian = "Error Read Data"; break;
                case -13: kembalian = "Error Start Offset"; break;
                case -14: kembalian = "Error Length"; break;
                case -15: kembalian = "Error Empty Data"; break;
                case -16: kembalian = "HEX Data Invalid"; break;
                case -17: kembalian = "Start Offset Can't More Than 512"; break;
                case -18: kembalian = "Index Out Of Bonds"; break;
            }
            return kembalian;


        }
    }
}
