﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibSmartCard;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Utilities.Encoders;
using Org.BouncyCastle.Crypto.Modes;

namespace libCard
{
    public class LibCard
    {
        private SmartCard osmartcard;
        private SCardProtocolIdentifiers CardProtocol, SAMProtocol;
        private string CAPDU, RAPDU;

        static string uidCard = "00000000";

        private static string SAMsessionKey;
        private byte[] SAMbyteSesKey;

        private byte[] iv;
        private byte[] k;

        public LibCard()
        {
            InitSmartCard();
            iv = new byte[] { 0x12, 0x06, 0x20, 0x24, 0x11, 0x00, 0x30, 0x12 };
            k = new byte[] { 0x30, 0x13, 0x20, 0x03, 0x07, 0x01, 0x00, 0x02 };
        }

        public bool getReaderName(IList reader)
        {
            int retCode = 0;
            if (osmartcard == null) InitSmartCard();

            retCode = osmartcard.ListReaders(reader);
            if (retCode==0)
            {
                return false;
            }

            return true;
        }

        private void InitSmartCard()
        {
            osmartcard = new SmartCard();
            CardProtocol = SCardProtocolIdentifiers.T1;
            SAMProtocol = SCardProtocolIdentifiers.T0;

            osmartcard.EstablishContext(SCardContextScope.System);
        }

        private void DeInitSmartCard()
        {
            osmartcard.ReleaseContext();
            osmartcard = null;
        }
       
        public bool ConnectCard()
        {
            if (osmartcard == null) InitSmartCard();
            if (!osmartcard.ConnectToContactlessCard(koneksi.GetCardReaderName(), CardProtocol))
            {
                return false;
            }
            return true;
        }

        public bool DisconnectCard()
        {
            if (osmartcard == null) return true;
            if (!osmartcard.DisconnectToContactlessCard()) return false;
            return true;
        }

        public bool ConnectSAMCard()
        {
            if (osmartcard == null) InitSmartCard();
            if (!osmartcard.ConnectToContactCard(koneksi.GetSAMReaderName(), SAMProtocol))
            {
                return false;
            }
            CAPDU = "00A4040009424A54454E4753414D00";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000"))) || (RAPDU.Length != 4)) return false;
            return true;
        }

        public bool TryConnectCard(string reader)
        {
            if (osmartcard == null) InitSmartCard();
            if (!osmartcard.ConnectToContactlessCard(koneksi.GetCardReaderName(), CardProtocol))
            {
                return false;
            }
            if (!(DisconnectCard())) return false;
            return true;
        }

        public bool TryConnectSAMCard(string reader)
        {
            if (osmartcard == null) InitSmartCard();
            if (!osmartcard.ConnectToContactCard(reader, SAMProtocol))
            {
                return false;
            }
            CAPDU = "00A4040009424A54454E4753414D00";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000"))) || (RAPDU.Length != 4)) return false;
            if(!(DisconnectSAMCard())) return false;
            return true;
        }

        public bool DisconnectSAMCard()
        {
            if (osmartcard == null) return true;
            if (!osmartcard.DisconnectToContactCard()) return false;
            return true;
        }

        public bool DivKeyCard()
        {
            CAPDU = "FFCA000000";
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 12) return false;
            uidCard = RAPDU.Substring(0, 8);
            CAPDU = "00D1000004" + uidCard;
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            return true;
        }

        private bool SelectFile(string sfileIDHexa)
        {
            string sfileidtrim = Function.TrimInput(sfileIDHexa);
            int lengthsfileidtrim = sfileidtrim.Length;
            if (lengthsfileidtrim != 4) return false;
            CAPDU = "00a40000" + (lengthsfileidtrim / 2).ToString("X2") + sfileidtrim;
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            return true;
        }

        private string Encrypt(string inputan, string kunci)
        {
            byte[] key = new byte[16];
            byte[] iv = new byte[16];
            byte[] input = new byte[8192], output = new byte[8192];
            string hasil = "";
            key = Function.HexaStringToArrayOfByte(Function.TrimInput(kunci));
            iv = Function.HexaStringToArrayOfByte(Function.TrimInput("0000000000000000"));
            input = Function.HexaStringToArrayOfByte(Function.TrimInput(inputan));
            UInt16 len = (UInt16)input.Length;
            BufferedBlockCipher cipher = new BufferedBlockCipher(new CbcBlockCipher(new DesEdeEngine()));
            cipher.Init(true, new ParametersWithIV(new DesEdeParameters(key), iv));
            byte[] outBytes = new byte[input.Length];
            int len1 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);
            cipher.DoFinal(outBytes, len1);
            hasil = BitConverter.ToString(outBytes, 0, (int)input.Length).Replace("-", "");
            return hasil;
        }

        private bool AuthCard(int noKey)
        {
            string getUID = "";
            string idx = "";
            string sDataInput = "";
            getUID = GetUIDCardS();
            if (getUID == "00000000") return false;

            string kunciMaster = Encrypt(getUID.PadRight(32, '0'), "657774727974657974726879686a7662");
            string kunciFileUID = Encrypt(getUID.PadRight(32, '1'), "77716666647374727974726867666b6f");
            string kunciFileSN = Encrypt(getUID.PadRight(32, '2'), "726577747265676468797479756a7271");

            if (noKey == 0)
            {
                if (!SelectFile("3F00")) return false;
                idx = "00";
                sDataInput = (kunciMaster).Replace(" ", "");
            }
            else if (noKey == 1)
            {
                if (!SelectFile("3F00")) return false;
                if (!SelectFile("0101")) return false;
                idx = "01";
                sDataInput = (kunciFileUID).Replace(" ", "");
            }
            else if (noKey == 2)
            {
                if (!SelectFile("3F00")) return false;
                if (!SelectFile("0202")) return false;
                idx = "02";
                sDataInput = (kunciFileSN).Replace(" ", "");
            }
            else
            {
                return false;
            }

            byte bInputKN = byte.Parse((idx).Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte[] bInputKV = new byte[16];
            byte[] bRspError = new byte[2];

            for (int i = 0; i < 16; i++)
                bInputKV[i] = byte.Parse(sDataInput.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);

            if (!ExtAuth(bInputKN, bInputKV))
            {
                return false;
            }

            return true;
        }

        private bool ExtAuth(byte bKeyNo, byte[] bKeyVal)
        {
            string resRnD = "";
            byte[] bRnd = new byte[8];
            if (!GetChallenge(8, ref bRnd))
                return false;

            resRnD = Encrypt(Function.ArrayOfByteToHexString(bRnd, bRnd.Length, ""), Function.ArrayOfByteToHexString(bKeyVal, bKeyVal.Length, ""));

            CAPDU = "008200" + bKeyNo.ToString("X2") + "08" + resRnD;
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;

            return true;
        }

        private bool GetChallenge(byte bLenVal, ref byte[] bChallengeVal)
        {
            CAPDU = "00840000" + bLenVal.ToString("X2");
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;

            bChallengeVal = Function.HexaStringToArrayOfByte(RAPDU.Substring(0, RAPDU.Length - 4));

            return true;
        }

        private bool AuthCardViaSAM(string keyNoInDecimal)
        {
            int noKey = Convert.ToInt32(keyNoInDecimal);
            CAPDU = "0084000008";
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 20) return false;

            CAPDU = "000200" + noKey.ToString("X2") + "08" + RAPDU.Substring(0, 16);
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("610D")))) return false;

            CAPDU = "00C000000D";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;

            if (RAPDU.Length != 30) return false;

            CAPDU = RAPDU.Substring(0, 26);
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;

            return true;
        }

        private bool AuthCardDefViaSAM(string keyNoInDecimal)
        {
            if (!CheckCard())
            {
                return false;
            }

            int noKey = Convert.ToInt32(keyNoInDecimal);
            CAPDU = "0084000008";
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 20) return false;

            CAPDU = "000201" + noKey.ToString("X2") + "08" + RAPDU.Substring(0, 16);
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("610D")))) return false;

            CAPDU = "00C000000D";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;

            if (RAPDU.Length != 30) return false;

            CAPDU = RAPDU.Substring(0, 26);
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;

            return true;
        }

        public bool AuthSAM()
        {
            byte[] rndA = new byte[8];
            byte[] rndB = new byte[8];

            byte[] rndAEnc = new byte[8];
            byte[] rndBRot = new byte[8];

            byte[] rndA_rndB = new byte[16];
            byte[] rndA_rndB_Enc = new byte[16];

            CAPDU = "00aa010008";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 20) return false;

            rndAEnc = Function.HexaStringToArrayOfByte(RAPDU.Substring(0, 16));


            CAPDU = "003D030008" + RAPDU.Substring(0, 16);
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("6108")))) return false;
            CAPDU = "00C0000008";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 20) return false;
            rndA = Function.HexaStringToArrayOfByte(RAPDU.Substring(0, 16));

            Random x = new Random();

            for (int i = 0; i < 8; i++)
            {
                rndB[i] = (byte)(x.Next(256));
                rndA_rndB[i] = (byte)(rndB[i] ^ rndA[i] ^ 0xDD);
                rndA_rndB[i + 8] = rndA[i];
            }


            string inpRndBxorRndA_RndA = Function.ArrayOfByteToHexString(rndA_rndB, 16, "");

            CAPDU = "003D030010" + inpRndBxorRndA_RndA;
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("6110")))) return false;
            CAPDU = "00C0000010";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 36) return false;
            rndA_rndB_Enc = Function.HexaStringToArrayOfByte(RAPDU.Substring(0, 32));


            CAPDU = "00AA020010" + Function.ArrayOfByteToHexString(rndA_rndB_Enc, 16, "");
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("6108")))) return false;
            CAPDU = "00C0000008";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 20) return false;

            CAPDU = "003D030008" + RAPDU.Substring(0, 16);
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("6108")))) return false;
            CAPDU = "00C0000008";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 20) return false;
            string rndbrot = RAPDU.Substring(0, 16);
            rndBRot = Function.HexaStringToArrayOfByte(rndbrot);


            for (int i = 1; i < 8; i++)
            {
                if (rndB[i] != rndBRot[i - 1]) return false;
            }
            if (rndB[0] != rndBRot[7]) return false;

            SAMbyteSesKey = new byte[16];

            for (int i = 0; i < 4; i++)
            {
                SAMbyteSesKey[i] = rndB[i];
                SAMbyteSesKey[i + 4] = rndA[i + 4];
                SAMbyteSesKey[i + 8] = rndB[i + 4];
                SAMbyteSesKey[i + 12] = rndA[i];
            }
            SAMsessionKey = Function.ArrayOfByteToHexString(SAMbyteSesKey, 16, "");

            return true;

        }

        private bool CheckCard()
        {
            string getUID = "";
            string result = "";
            getUID = GetUIDCardS();
            if (getUID == "00000000") return false;

            string compare = Encrypt(getUID.PadRight(32, '1'), "77716666647374727974726867666b6f");

            if (!AuthCard(1))
            {
                return false;
            }

            CAPDU = "00B0000010";
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 36) return false;
            result = RAPDU.Substring(0, 32);

            if (result != compare)
            {
                return false;
            }

            return true;
        }

        public int ReadData(string FileAddress, string StartOffsett, string Length, out string data)
        {
            data = "";
            int start=0;
            int lengthData=0;
            start=Convert.ToInt32(StartOffsett);
            lengthData=Convert.ToInt32(Length);
            if (StartOffsett == "") return ReturnError.GetError(ReturnError.Error.EmptyStartOffset);
            if (start >= 1024)
            {
                return ReturnError.GetError(ReturnError.Error.MaxStartOffset);
            }
            if (start + lengthData > 1024)
            {
                return ReturnError.GetError(ReturnError.Error.OutOfBonds);
            }
            if (Length == "") return ReturnError.GetError(ReturnError.Error.LengthData);
            byte[] dataKartu;

            //if (!CheckCard()) return ReturnError.GetError(ReturnError.Error.CheckCard);

            if (!SelectFile("2508")) return ReturnError.GetError(ReturnError.Error.SelectFolder);
            if (!SelectFile(FileAddress)) return ReturnError.GetError(ReturnError.Error.FileAddress);
            if (!AuthCardViaSAM("7")) return ReturnError.GetError(ReturnError.Error.AuthReadData);
            if (!ReadBinaryFileEncMac(StartOffsett, Length, out dataKartu)) return ReturnError.GetError(ReturnError.Error.ReadData);
            data = Function.ArrayOfByteToHexString(dataKartu, dataKartu.Length, "");
            return 0;
        }

        public int WriteData(string FileAddress, string StartOffsett, string Length, string data)
        {
            int start = 0;
            int lengthData = 0;
            start = Convert.ToInt32(StartOffsett);
            lengthData = Convert.ToInt32(Length);
            if (StartOffsett == "") return ReturnError.GetError(ReturnError.Error.EmptyStartOffset);
            if (Length == "") return ReturnError.GetError(ReturnError.Error.LengthData);
            if (data == "") return ReturnError.GetError(ReturnError.Error.EmptyData);
            if (data.Length % 2 != 0)
            {
                return ReturnError.GetError(ReturnError.Error.DataHexNotValid);
            }
            if (data.Length / 2 != Convert.ToInt32(Length))
            {
                return ReturnError.GetError(ReturnError.Error.LengthData);
            }
            if (start >= 1024)
            {
                return ReturnError.GetError(ReturnError.Error.MaxStartOffset);
            }
            if (start + lengthData > 1024)
            {
                return ReturnError.GetError(ReturnError.Error.OutOfBonds);
            }
            byte[] tempData = Function.HexaStringToArrayOfByte(data);

            //if (!CheckCard()) return ReturnError.GetError(ReturnError.Error.CheckCard);

            if (!SelectFile("2508")) return ReturnError.GetError(ReturnError.Error.SelectFolder);
            if (!SelectFile(FileAddress)) return ReturnError.GetError(ReturnError.Error.FileAddress);
            if (!AuthCardViaSAM("2")) return ReturnError.GetError(ReturnError.Error.AuthWriteData);
            if (!WriteBinaryFileEncMac(StartOffsett, Length, tempData)) return ReturnError.GetError(ReturnError.Error.WriteData);
            return 0;
        }

        private bool ReadBinaryFileEncMac(string offsetInDecimal, string lengthInDecimal, out byte[] data)
        {
            data = null;
            int offset = Convert.ToInt32(offsetInDecimal);
            int length = Convert.ToInt32(lengthInDecimal);
            int lengthTemp;
            string randGetChal = "";
            string tmpHex = "";

            while ((length > 0))
            {
                CAPDU = "0084000008";
                if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                randGetChal = RAPDU.Substring(0, 16);

                CAPDU = "000400000d04b0" + offset.ToString("X4") + "04" + randGetChal;
                if (!(SendCommandSAMCard())) return false;
                int enamsatu = (int)(RAPDU.Length - 4);
                int sekiansekian = enamsatu + 2;
                if (RAPDU.Substring(enamsatu, 2) == "61")
                {
                    CAPDU = "00C00000" + RAPDU.Substring(sekiansekian, 2);
                    if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                }

                if (length >= 200) lengthTemp = 200;
                else lengthTemp = length;
                CAPDU = "04B0" + offset.ToString("X4") + "04" + RAPDU.Substring(0, 8) + lengthTemp.ToString("X2");
                if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                int lensementara = (RAPDU.Length / 2) - 2;

                CAPDU = "00050000" + lensementara.ToString("X2") + RAPDU.Substring(0, RAPDU.Length - 4);
                if (!(SendCommandSAMCard())) return false;
                enamsatu = (int)(RAPDU.Length - 4);
                sekiansekian = enamsatu + 2;
                if (RAPDU.Substring(enamsatu, 2) == "61")
                {
                    CAPDU = "00C00000" + RAPDU.Substring(sekiansekian, 2);
                    if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                }
                if (RAPDU.Substring(0, 2) != lengthTemp.ToString("X2")) { return false; }

                tmpHex += RAPDU.Substring(2, lengthTemp * 2);
                length = length - lengthTemp;
                offset = offset + lengthTemp;
            }

            data = Function.HexaStringToArrayOfByte(tmpHex);

            return true;

        }

        private bool WriteBinaryFileEncMac(string offsetInDecimal, string lengthInDecimal, byte[] data)
        {
            int offset = Convert.ToInt32(offsetInDecimal);
            //int length = Convert.ToInt32(lengthInDecimal) & 0xFF;
            int length = Convert.ToInt32(lengthInDecimal);

            int lengthTemp;
            string byteDataToHexString = Function.ArrayOfByteToHexString(data, data.Length, "");
            int dataOffset = 0;

            string dataHex = "";
            string randGetChal = "";
            string dataKeSAM = "";
            while ((length > 0))
            {
                CAPDU = "0084000008";
                if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                randGetChal = RAPDU.Substring(0, 16);

                if (length >= 200) lengthTemp = 200;
                else lengthTemp = length;
                dataHex = byteDataToHexString.Substring(dataOffset, lengthTemp * 2);

                dataKeSAM = offset.ToString("X4") + lengthTemp.ToString("X2") + dataHex + randGetChal;

                CAPDU = "00030000" + (dataKeSAM.Length / 2).ToString("X2") + dataKeSAM;
                if (!(SendCommandSAMCard())) return false;
                int enamsatu = (int)(RAPDU.Length - 4);
                int sekiansekian = enamsatu + 2;
                if (RAPDU.Substring(enamsatu, 2) == "61")
                {
                    CAPDU = "00C00000" + RAPDU.Substring(sekiansekian, 2);
                    if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                }

                CAPDU = RAPDU.Substring(0, RAPDU.Length - 4);
                if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;

                length = length - lengthTemp;
                offset = offset + lengthTemp;
                dataOffset = dataOffset + lengthTemp*2;
            }

            return true;
        }

        private bool SendCommandCard()
        {
            return osmartcard.SendCommandContactless(CAPDU, CardProtocol, ref RAPDU);
        }

        private bool SendCommandSAMCard()
        {
            return osmartcard.SendCommand(CAPDU, SAMProtocol, ref RAPDU);
        }

        public bool GetSNSAM(out string SNSAM)
        {
            SNSAM = "000000000000";
            CAPDU = "0055000006";
            if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")) || ((RAPDU.Length != 16))))
            {
                return false;
            }
            SNSAM = RAPDU.Substring(0, 12);
            return true;
        }

        public bool GetUIDCard(out string UIDCard)
        {
            UIDCard = "00000000";
            CAPDU = "FFCA000000";
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 12) return false;
            UIDCard = RAPDU.Substring(0, 8);
            return true;
        }

        private string GetUIDCardS()
        {
            string UIDCard = "00000000";
            CAPDU = "FFCA000000";
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return UIDCard;
            if (RAPDU.Length != 12) return UIDCard;
            UIDCard = RAPDU.Substring(0, 8);
            return UIDCard;
        }

        public bool GetSerialCard(out string Serial)
        {
            string result = "";
            Serial = "";

            if (!CheckCard())
            {
                return false;
            }

            if (!AuthCard(2))
            {
                return false;
            }

            CAPDU = "00B0000010";
            if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
            if (RAPDU.Length != 36) return false;

            result = RAPDU.Substring(0, 32);
            result = Function.HexStringToASCIIString(result);
            Serial = result;
            return true;
        }
    }

    public static class cryp
    {
        public static string encrypt(string input)
        {
            string hasil = "";
            hasil = crypto.EncryptStringAES(input, "itpurasmarttechnology");
            return hasil;
        }
        public static string decrypt(string input)
        {
            string hasil = "";
            hasil = crypto.DecryptStringAES(input, "itpurasmarttechnology");
            return hasil;
        }
    }

    public static class crypto
    {
        private static byte[] _salt = Encoding.ASCII.GetBytes("o6806642kbM7c5");

        /// <summary>
        /// Encrypt the given string using AES.  The string can be decrypted using 
        /// DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        public static string EncryptStringAES(string plainText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException("plainText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            string outStr = null;                       // Encrypted string to return
            RijndaelManaged aesAlg = null;              // RijndaelManaged object used to encrypt the data.

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create a RijndaelManaged object
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                // Create a decryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // prepend the IV
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                    }
                    outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return outStr;
        }

        /// <summary>
        /// Decrypt the given string.  Assumes the string was encrypted using 
        /// EncryptStringAES(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        public static string DecryptStringAES(string cipherText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentNullException("cipherText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create the streams used for decryption.                
                byte[] bytes = Convert.FromBase64String(cipherText);
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    // Create a RijndaelManaged object
                    // with the specified key and IV.
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    // Get the initialization vector from the encrypted stream
                    aesAlg.IV = ReadByteArray(msDecrypt);
                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;
        }

        private static byte[] ReadByteArray(Stream s)
        {
            byte[] rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }
    }
}
