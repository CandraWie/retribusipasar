﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;

namespace SistemLogin
{
    public partial class ChangePassword : Form
    {
       
        string sAppAdd = AppDomain.CurrentDomain.BaseDirectory;
        byte[] passparameter = new byte[16];
        byte[] key = new byte[8];
        List<string>[] dataenkrip = new List<string>[2];
        byte[] dataacak = new byte[512];
        string skey = "3008201630102016";
        int plot = 0;
        string password = "";
        string username = "";

        public ChangePassword(string passworddata, string usernamedata, int plotdata)
        {
            InitializeComponent();
            password = passworddata;
            username = usernamedata;
            plot = plotdata;
        }

        private void lbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GotoFormMain()
        {
            Application.Run(new LoginForm());
        }

        byte[] HexaStringToArrayOfByte(string hex)
        {
            hex = hex.Replace(" ", "");
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        string arrayOfByteToHexaString(byte[] c)
        {
            string tmpr;
            tmpr = string.Empty;
            for (int j = 0; j < c.Length; j++)
            {
                tmpr = tmpr + string.Format("{0:X2}", c[j]) + " ";
            }
            return tmpr;
        }

        private void encryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(true, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            string allfiletext = "";
            if (tbPassLama.Text == "")
            {
                MessageBox.Show("Password Lama Belum diisi");
                tbPassLama.Focus();
                return;
            }
            if (tbPassBaru.Text == "")
            {
                MessageBox.Show("Password Baru Belum diisi");
                tbPassBaru.Focus();
                return;
            }
            if (tbPassRetype.Text == "")
            {
                MessageBox.Show("Ketik Ulang Password Baru");
                tbPassRetype.Focus();
                return;
            }

            if (tbPassLama.Text.ToLower() != password)
            {
                MessageBox.Show("Password Lama Tidak valid");
                tbPassLama.Focus();
                return;
            }

            if (tbPassBaru.Text.ToLower()!=tbPassRetype.Text.ToLower())
            {
                MessageBox.Show("Password Baru Tidak Valid");
                tbPassBaru.Focus();
                return;
            }

            for (int i = 0; i < tbPassBaru.Text.Length; i++)
            {
                if (!((tbPassBaru.Text[i] >= '0' && tbPassBaru.Text[i] <= '9') || (tbPassBaru.Text[i] >= 'a' && tbPassBaru.Text[i] <= 'z')))
                {
                    MessageBox.Show("Username harus karakter huruf atau angka", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbPassBaru.Focus();
                    return;
                }
            }

            System.IO.StreamReader sr = System.IO.File.OpenText(sAppAdd + "SSAL.txt");
            allfiletext = sr.ReadToEnd();
            sr.Close();
            dataacak = HexaStringToArrayOfByte(allfiletext);
            passparameter = ASCIIEncoding.ASCII.GetBytes(username.ToLower().PadRight(8,'*') + tbPassBaru.Text.ToLower().PadRight(8,'*'));
            key = HexaStringToArrayOfByte(skey);
            encryptDESECB(passparameter.Length, passparameter, key, out passparameter);
            Array.Copy(passparameter, 0, dataacak, plot/2, 16);

            allfiletext = arrayOfByteToHexaString(dataacak);
            System.IO.StreamWriter sw = System.IO.File.CreateText(sAppAdd + "SSAL.txt");
            sw.Write(allfiletext.Replace(" ", ""));
            sw.Close();

            System.Threading.Thread t1 = new System.Threading.Thread(GotoFormMain);
            t1.SetApartmentState(System.Threading.ApartmentState.STA);
            t1.Start();
            this.Close();
        }
    }
}
