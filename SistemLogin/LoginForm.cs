﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;


namespace SistemLogin
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }
        string sAppAdd = AppDomain.CurrentDomain.BaseDirectory;
        byte[] passparameter = new byte[16];
        byte[] key = new byte[8];
        List<string>[] dataenkrip = new List<string>[3];
        List<string> dataasli;
        List<string>[] dataready = new List<string>[3];
        string skey = "3008201630102016";
        bool loginin = false;
        int plot = 0;
        string username = "";
        string password = "";

        private void LoginForm_Load(object sender, EventArgs e)
        {
            btnAddUser.Visible = false;
            btnLookData.Visible = false;
            dataready[0] = new List<string>();
            dataready[1] = new List<string>();
            dataready[2] = new List<string>();
            dataenkrip[0] = new List<string>();
            dataenkrip[1] = new List<string>();
            dataenkrip[2] = new List<string>();
            dataasli = new List<string>();
            if (!System.IO.File.Exists(sAppAdd + "SSAL.txt"))
            {
                //MessageBox.Show("Belum Setting Login", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //this.Close();
                //return;
                System.IO.StreamWriter sw = System.IO.File.CreateText(sAppAdd + "SSAL.txt");
                string data = arrayOfByteToHexaString(randomtext());
                sw.WriteLine(data.Replace(" ", ""));
                sw.Close();
                loginin = true;
                return;
            }

            System.IO.StreamReader sr = System.IO.File.OpenText(sAppAdd + "SSAL.txt");
            string allfiletext = sr.ReadToEnd();
            for (int i = 0; i < 200; i++)
            {
                if (i % 40 == 6)
                {
                    dataenkrip[0].Add(allfiletext.Substring(i, 32));
                    dataenkrip[1].Add("admin");
                    dataenkrip[2].Add(i.ToString());
                }
            }
            for (int i = 200; i < 1024; i++)
            {
                if (i % 40 == 6)
                {
                    dataenkrip[0].Add(allfiletext.Substring(i, 32));
                    dataenkrip[1].Add("user");
                    dataenkrip[2].Add(i.ToString());
                }
            }
            sr.Close();
            for (int i = 0; i < dataenkrip[0].Count(); i++)
            {
                passparameter = HexaStringToArrayOfByte(dataenkrip[0][i]);
                key = HexaStringToArrayOfByte(skey);

                decryptDESECB(passparameter.Length, passparameter, key, out passparameter);
                dataasli.Add(ASCIIEncoding.ASCII.GetString(passparameter));
                for (int j = 0; j < dataasli[i].Length; j++)
                {
                    if (!((dataasli[i][j] >= '0' && dataasli[i][j] <= '9') || (dataasli[i][j] >= 'a' && dataasli[i][j] <= 'z') || (dataasli[i][j] == '*')))
                    {
                        dataasli[i] = "";
                        dataasli[i] = dataasli[i].PadRight(16, '*');
                    }
                }
            }

          
            for (int i = 0; i < dataasli.Count; i++)
            {
                if (dataasli[i] != "****************")
                {
                    dataready[0].Add(dataasli[i].Substring(0, 16));
                    dataready[1].Add(dataenkrip[1][i]);
                    dataready[2].Add(dataenkrip[2][i]);
                }
            }
        }

        private byte[] randomtext()
        {
            byte[] dataacak = new byte[1024];
            Random randomacak = new Random();
            randomacak.NextBytes(dataacak);
            return dataacak;
        }

        private void encryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(true, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        private void decryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(false, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        byte[] HexaStringToArrayOfByte(string hex)
        {
            hex = hex.Replace(" ", "");
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        string arrayOfByteToHexaString(byte[] c)
        {
            string tmpr;
            tmpr = string.Empty;
            for (int j = 0; j < c.Length; j++)
            {
                tmpr = tmpr + string.Format("{0:X2}", c[j]) + " ";
            }
            return tmpr;
        }

        private void btnLanjut_Click(object sender, EventArgs e)
        {
            if (tbUserName.Text == "")
            {
                MessageBox.Show("Username Belum diisi");
                tbUserName.Focus();
                return;
            }
            if (tbPassword.Text == "")
            {
                MessageBox.Show("Password Belum diisi");
                tbPassword.Focus();
                return;
            }

            if (loginin)
            {
                if (tbPassword.Text.ToLower() == "setlogin" && tbUserName.Text.ToLower() == "admin")
                {
                    MessageBox.Show("Login Berhasil \nSegera Lakukan Penambahan Admin dan User Baru");
                    System.Threading.Thread t1 = new System.Threading.Thread(GotoAddUserForm);
                    t1.SetApartmentState(System.Threading.ApartmentState.STA);
                    t1.Start();
                    this.Close();
                    return;
                }
                else
                {
                    MessageBox.Show("Password atau Username Salah");
                    tbPassword.Text = "";
                    tbUserName.Text = "";
                    return;
                }
            }
            else
            {
                if (btnLanjut.Text == "Ganti Password")
                {
                    System.Threading.Thread t1 = new System.Threading.Thread(GotoChangePass);
                    t1.SetApartmentState(System.Threading.ApartmentState.STA);
                    t1.Start();
                    this.Close();
                    return;
                }
                else
                {
                    for (int i = 0; i < dataready[0].Count; i++)
                    {
                        if (tbPassword.Text.ToLower() == dataready[0][i].Substring(8, 8).Replace("*", "") && tbUserName.Text.ToLower() == dataready[0][i].Substring(0, 8).Replace("*", ""))
                        {
                            if (dataready[1][i] == "user")
                            {
                                btnAddUser.Visible = false;
                                btnLookData.Visible = false;
                            }
                            else
                            {
                                btnAddUser.Visible = true;
                                btnLookData.Visible = true;
                            }
                            
                            MessageBox.Show("Login Berhasil");
                            password = tbPassword.Text.ToLower();
                            username = tbUserName.Text.ToLower();
                            plot = int.Parse(dataready[2][i]);
                            btnLanjut.Text = "Ganti Password";
                            tbPassword.Enabled = false;
                            tbUserName.Enabled = false;
                            return;
                        }
                    }
                    MessageBox.Show("Password atau Username Salah");
                    return;
                }
            }
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            if (tbPassword.Text == "12345678")
            {
                MessageBox.Show("Password belum diganti! \nGanti Password terlebih dahulu");
                return;
            }
            System.Threading.Thread t1 = new System.Threading.Thread(GotoAddUserForm);
            t1.SetApartmentState(System.Threading.ApartmentState.STA);
            t1.Start();
            this.Close();
            return;
        }

        private void GotoAddUserForm()
        {
            Application.Run(new FormLoginSetting());
        }

        private void GotoChangePass()
        {
            Application.Run(new ChangePassword(password, username, plot));
        }

        private void GotoFormLog()
        {
            Application.Run(new FormLog());
        }

        private void lbExit_Click(object sender, EventArgs e)
        {
            if (loginin)
                System.IO.File.Delete(sAppAdd + "SSAL.txt");
            this.Close();
        }

        private void btnLookData_Click(object sender, EventArgs e)
        {
            if (tbPassword.Text == "12345678")
            {
                MessageBox.Show("Password belum diganti! \nGanti Password terlebih dahulu");
                return;
            }
            System.Threading.Thread t1 = new System.Threading.Thread(GotoFormLog);
            t1.SetApartmentState(System.Threading.ApartmentState.STA);
            t1.Start();
            this.Close();
            return;
        }

       
    }
}
