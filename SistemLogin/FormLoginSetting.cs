﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;

namespace SistemLogin
{
    public partial class FormLoginSetting : Form
    {
        public FormLoginSetting()
        {
            InitializeComponent();
        }
        string sAppAdd = AppDomain.CurrentDomain.BaseDirectory;
        byte[] passparameter = new byte[16];
        byte[] key = new byte[8];
        List<string>[] dataenkrip = new List<string>[2];
        List<string> dataasli;
        List<string> dataadmin = new List<string>();
        List<string> datauser = new List<string>();
        string skey = "3008201630102016";

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3)
                return;

            int baris = e.RowIndex;
            dataGridView1.Rows.RemoveAt(baris);
            //dataready[0][baris] = "";
            dataadmin.RemoveAt(baris);
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1[0, i].Value = (i + 1).ToString();
            }
            //shorting();
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            string NewLevel = comboBox1.Text.ToLower();
            string NewUserName = tbNewUserName.Text.ToLower();
            int nomor = 0;
            if (tbNewUserName.Text == "")
            {
                MessageBox.Show("Username baru Belum diisi", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbNewUserName.Focus();
                return;
            }

            for (int i = 0; i < tbNewUserName.Text.Length; i++)
            {
                if (!((NewUserName[i] >= '0' && NewUserName[i] <= '9') || (NewUserName[i] >= 'a' && NewUserName[i] <= 'z')))
                {
                    MessageBox.Show("Username harus karakter huruf atau angka", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbNewUserName.Focus();
                    return;
                }
            }

            if (dataGridView2.Rows.Count >= 20)
            {
                MessageBox.Show("Daftar maksimal 20 User", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (dataGridView1.Rows.Count >= 5)
            {
                MessageBox.Show("Daftar maksimal 5 Admin", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (comboBox1.SelectedIndex == 1)
            {
                if (dataGridView1.Rows.Count == 0)
                    nomor = 1;
                else
                    nomor = int.Parse(dataGridView1[0, dataGridView1.Rows.Count - 1].Value.ToString()) + 1;

                dataGridView1.Rows.Add(nomor, NewUserName, NewLevel, "");
                dataadmin.Add(NewUserName.PadRight(8, '*') + "12345678");
            }
            else
            {
                if (dataGridView2.Rows.Count == 0)
                    nomor = 1;
                else
                    nomor = int.Parse(dataGridView2[0, dataGridView2.Rows.Count - 1].Value.ToString()) + 1;

                dataGridView2.Rows.Add(nomor, NewUserName, NewLevel, "");
                datauser.Add(NewUserName.PadRight(8, '*') + "12345678");
            }
            tbNewUserName.Text = "";
            comboBox1.SelectedIndex = 0;
        }
        
        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Belum Ada Admin", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (dataGridView2.Rows.Count == 0)
            {
                MessageBox.Show("Belum Ada User", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            byte[] dataacak = randomtext();
            for (int i = 0; i < dataadmin.Count; i++)
            {
                passparameter = ASCIIEncoding.ASCII.GetBytes(dataadmin[i]);
                key = HexaStringToArrayOfByte(skey);
                encryptDESECB(passparameter.Length, passparameter, key, out passparameter);
                Array.Copy(passparameter, 0, dataacak, 3 + 20 * i, 16);
            }
            for (int i = 0; i < datauser.Count; i++)
            {
                passparameter = ASCIIEncoding.ASCII.GetBytes(datauser[i]);
                key = HexaStringToArrayOfByte(skey);
                encryptDESECB(passparameter.Length, passparameter, key, out passparameter);
                Array.Copy(passparameter, 0, dataacak, 103 + 20 * i, 16);
            }
            string alltext = arrayOfByteToHexaString(dataacak);
            System.IO.StreamWriter sw = System.IO.File.CreateText(sAppAdd + "SSAL.txt");
            sw.Write(alltext.Replace(" ", ""));
            sw.Close();

            System.Threading.Thread t1 = new System.Threading.Thread(GotoLoginForm);
            t1.SetApartmentState(System.Threading.ApartmentState.STA);
            t1.Start();
            this.Close();
        }

        private void GotoLoginForm()
        {
            Application.Run(new LoginForm());
        }

        private void FormLoginSetting_Load(object sender, EventArgs e)
        {
            dataenkrip[0] = new List<string>();
            dataenkrip[1] = new List<string>();
            dataasli = new List<string>();
            string tmp = "";
            System.IO.StreamReader sr = System.IO.File.OpenText(sAppAdd + "SSAL.txt");
            string allfiletext = sr.ReadToEnd();
            for (int i = 0; i < 200; i++)
            {
                if (i % 40 == 6)
                {
                    passparameter = HexaStringToArrayOfByte(allfiletext.Substring(i, 32));
                    key = HexaStringToArrayOfByte(skey);
                    decryptDESECB(passparameter.Length, passparameter, key, out passparameter);
                    tmp = ASCIIEncoding.ASCII.GetString(passparameter);
                    for (int j = 0; j < tmp.Length; j++)
                    {
                        if (!((tmp[j] >= '0' && tmp[j] <= '9') || (tmp[j] >= 'a' && tmp[j] <= 'z') || (tmp[j] == '*')))
                        {
                            tmp = "";
                            tmp = tmp.PadRight(16, '*');
                        }
                    }
                    if (tmp != "****************")
                        dataadmin.Add(tmp);
                }
            }
            for (int i = 0; i < dataadmin.Count; i++)
            {
                dataGridView1.Rows.Add((i + 1).ToString(), dataadmin[i].Substring(0, 8).Replace("*", ""), "admin");
            }

            for (int i = 200; i < 1024; i++)
            {
                if (i % 40 == 6)
                {
                    passparameter = HexaStringToArrayOfByte(allfiletext.Substring(i, 32));
                    key = HexaStringToArrayOfByte(skey);
                    decryptDESECB(passparameter.Length, passparameter, key, out passparameter);
                    tmp = ASCIIEncoding.ASCII.GetString(passparameter);
                    
                    for (int j = 0; j < tmp.Length; j++)
                    {
                        if (!((tmp[j] >= '0' && tmp[j] <= '9') || (tmp[j] >= 'a' && tmp[j] <= 'z') || (tmp[j] == '*')))
                        {
                            tmp = "";
                            tmp = tmp.PadRight(16, '*');
                        }
                    }
                    if (tmp != "****************")
                    {
                        datauser.Add(tmp);
                    }
                }
            }
            sr.Close();
            for (int i = 0; i < datauser.Count; i++)
            {
                dataGridView2.Rows.Add((i + 1).ToString(), datauser[i].Substring(0, 8).Replace("*", ""), "user");
            }
            
        }

        private byte[] randomtext()
        {
            byte[] dataacak = new byte[1024];
            Random randomacak = new Random();
            randomacak.NextBytes(dataacak);
            return dataacak;
        }

        private void encryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(true, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        private void decryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(false, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        byte[] HexaStringToArrayOfByte(string hex)
        {
            hex = hex.Replace(" ", "");
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        string arrayOfByteToHexaString(byte[] c)
        {
            string tmpr;
            tmpr = string.Empty;
            for (int j = 0; j < c.Length; j++)
            {
                tmpr = tmpr + string.Format("{0:X2}", c[j]) + " ";
            }
            return tmpr;
        }

        byte[] ASCIIStringToArrayOfByte(string s)
        {
            byte[] tmp = new byte[s.Length];

            for (int i = 0; i < tmp.Length; i++)
            {
                tmp[i] = (byte)s[i];
            }

            return tmp;
        }

        private void lbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormLoginSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                System.IO.File.Delete(sAppAdd + "SSAL.txt");
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3)
                return;

            int baris = e.RowIndex;
            dataGridView2.Rows.RemoveAt(baris);
            datauser.RemoveAt(baris);
            for (int i = 0; i < dataGridView2.Rows.Count; i++)
                dataGridView2[0, i].Value = (i + 1).ToString();
        }
    }

}