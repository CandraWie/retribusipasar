﻿using System;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;
using System.Windows.Forms;
using System.Text;

namespace SistemLogin
{
    public partial class FormLog : Form
    {
        public FormLog()
        {
            InitializeComponent();
        }
        string sAppAdd = AppDomain.CurrentDomain.BaseDirectory;
        string inputdata = "";
        byte[] passing;
        string skey= "3008201630102016";
        byte[] key;

        private void FormLog_Load(object sender, EventArgs e)
        {
            if (!System.IO.File.Exists(sAppAdd + "SLL.txt"))
            {
                System.IO.StreamWriter sw = System.IO.File.CreateText(sAppAdd + "SLL.txt");
                MessageBox.Show("Data Kosong");
                return;
            }
            System.IO.StreamReader sr = System.IO.File.OpenText(sAppAdd + "SLL.txt");
            
            while (!sr.EndOfStream)
            {
                inputdata = sr.ReadLine();
                passing = HexaStringToArrayOfByte(inputdata);
                key = HexaStringToArrayOfByte(skey);
                decryptDESECB(passing.Length, passing, key, out passing);
                inputdata = ASCIIEncoding.ASCII.GetString(passing);
                string[] splititem = inputdata.Replace("*","").Split(';');
                dataGridView1.Rows.Add(splititem[0].ToUpper(), splititem[1], splititem[2].ToUpper(), splititem[3], splititem[4], splititem[5], splititem[6], splititem[7].ToUpper(), splititem[8]);
            }
            sr.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            System.Threading.Thread t1 = new System.Threading.Thread(GotoLogin);
            t1.SetApartmentState(System.Threading.ApartmentState.STA);
            t1.Start();
            this.Close();
        }

        private void GotoLogin()
        {
            Application.Run(new LoginForm());
        }

        string arrayOfByteToHexaString(byte[] c)
        {
            string tmpr;
            tmpr = string.Empty;
            for (int j = 0; j < c.Length; j++)
            {
                tmpr = tmpr + string.Format("{0:X2}", c[j]) + " ";
            }
            return tmpr;
        }

        byte[] HexaStringToArrayOfByte(string hex)
        {
            hex = hex.Replace(" ", "");
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        private void decryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(false, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }
    }
}
