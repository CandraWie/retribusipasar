﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Xml;

namespace SettingParameter
{
    public partial class Form1 : Form
    {
        string sApp = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            XmlNode node;

            xml.Load(sApp + "\\parameter.xml");
            node = xml.SelectSingleNode("Device");

            node["uid"].InnerText = textBox1.Text;
            node["imei"].InnerText = textBox2.Text;
            node["hp"].InnerText = textBox3.Text;
            node["vacc"].InnerText = textBox4.Text;
            node["idagent"].InnerText = textBox5.Text;
            node["idmerchant"].InnerText = textBox6.Text;
            node["idterminal"].InnerText = textBox7.Text;
            node["wsadd"].InnerText = textBox8.Text;
            node["wsadp"].InnerText = textBox9.Text;
            node["idkios"].InnerText = textBox10.Text;

            xml.Save(sApp + "\\parameter.xml");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            XmlNode node;

            if (!System.IO.File.Exists(sApp + "\\parameter.xml"))
            {
                node = xml.CreateXmlDeclaration("1.0", "UTF-8", null);
                xml.AppendChild(node);

                node = xml.CreateElement("Device");
                xml.AppendChild(node);

                XmlNode xxnode = xml.CreateElement("idmerchant");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("uid");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("imei");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("hp");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("vacc");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("idagent");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("idkios");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("idterminal");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("wsadd");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xxnode = xml.CreateElement("wsadp");
                xxnode.InnerText = "";
                node.AppendChild(xxnode);

                xml.Save(sApp + "\\parameter.xml");
            }
            else
            {
                xml.Load(sApp + "\\parameter.xml");
                node = xml.SelectSingleNode("Device");
                textBox1.Text = node["uid"].InnerText;
                textBox2.Text = node["imei"].InnerText;
                textBox3.Text = node["hp"].InnerText;
                textBox4.Text = node["vacc"].InnerText;
                textBox5.Text = node["idagent"].InnerText;
                textBox6.Text = node["idmerchant"].InnerText;
                textBox7.Text = node["idterminal"].InnerText;
                textBox8.Text = node["wsadd"].InnerText;
                textBox9.Text = node["wsadp"].InnerText;
                textBox10.Text = node["idkios"].InnerText;
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
