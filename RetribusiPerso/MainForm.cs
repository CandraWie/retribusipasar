﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using libCard;
using System.Xml;

namespace RetribusiPerso
{
    public partial class MainForm : Form
    {
        XmlDocument xSetting = new XmlDocument();
        XmlNode xNode;
        string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public MainForm()
        {
            InitializeComponent();
        }

        private System.Threading.Thread rd1;
        private LibCard smartCard= new LibCard();
        int retCode = 0;
        bool konekSAM = false;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (konekSAM == false)
            {
                MessageBox.Show("Please press Connect SAM!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (tbSNSAM.Text.Length != 12)
            {
                MessageBox.Show("Length SN SAM must be 6 byte!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbKEY.Text.Length != 16)
            {
                MessageBox.Show("Length KEY must be 16 characters!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbReenter.Text.Length != 16)
            {
                MessageBox.Show("Length Re-enter KEY must be 16 characters!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if(tbKEY.Text != tbReenter.Text)
            {
                MessageBox.Show("Input KEY does not match!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!(smartCard.SetKeyASCII(tbSNSAM.Text, tbKEY.Text)))
            {
                MessageBox.Show("Failed to save KEY!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                MessageBox.Show("Success", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                xNode["DATA"].InnerText = tbKEY.Text;
                xSetting.Save(appPath + @"\setting.xml");
                //tbKEY.PasswordChar = '*';
                tbKEY.Enabled = false;
                tbReenter.Enabled = false;
                konekSAM = false;
                tbSNSAM.Enabled = false;
                btnSave.Enabled = false;
            }
        }

        private void tbSNSAM_KeyPress(object sender, KeyPressEventArgs e)
        {
            // this will only allow valid hex values [0-9][a-f][A-F] to be entered. See ASCII table
            char c = e.KeyChar;
            if (c != '\b' && !((c <= 0x66 && c >= 0x61) || (c <= 0x46 && c >= 0x41) || (c >= 0x30 && c <= 0x39) || (c == 0x2c)))
            {
                e.Handled = true;
            }
        }

        private void tbKEY_KeyPress(object sender, KeyPressEventArgs e)
        {
            // this will only allow valid hex values [0-9][a-f][A-F] to be entered. See ASCII table
            //char c = e.KeyChar;
            //if (c != '\b' && !((c <= 0x66 && c >= 0x61) || (c <= 0x46 && c >= 0x41) || (c >= 0x30 && c <= 0x39) || (c == 0x2c)))
            //{
            //    e.Handled = true;
            //}
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            xSetting.Load(appPath + @"\setting.xml");
            xNode = xSetting.SelectSingleNode("UserSetting");

            string snsam = "";
            string key="";

            if (!smartCard.ConnectSAMCard()) { MessageBox.Show("Tidak dapat terkoneksi dengan kartu SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); konekSAM = false; Cursor.Current = Cursors.Default; return; }
            konekSAM = true;
            //try
            //{
            //    key = cryp.decrypt(xNode["DATA"].InnerText);
            //}
            //catch
            //{
            //    key = "";
            //}
            //if (key != "" && key.Length == 32)
            //{
            //    tbKEY.Text = key;
            //    tbKEY.Enabled = false;
            //    tbKEY.PasswordChar = '*';
            //}
            //else
            //{
            //    tbKEY.Enabled = true;
            //}
            smartCard.AuthSAM();
            smartCard.GetSNSAM(out snsam);
            if (snsam == "000000000000")
            {
                btnSave.Enabled = true;
                tbSNSAM.Enabled = true;
                if(tbKEY.Text == "" || tbReenter.Text=="")
                {
                    tbKEY.Enabled = true;
                    tbReenter.Enabled = true;
                }
                else
                {
                    tbKEY.Enabled = false;
                    tbReenter.Enabled = false;
                }
                tbSNSAM.Text = snsam;
                tbSNSAM.Focus();
                tbSNSAM.SelectAll();                
            }
            else
            {
                tbSNSAM.Text = snsam;
                tbSNSAM.Enabled = false;
                tbKEY.Enabled = false;
                tbReenter.Enabled = false;
                btnSave.Enabled = false;
            }

            Cursor.Current = Cursors.Default;
        }
    }
}
