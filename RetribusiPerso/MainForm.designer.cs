﻿namespace RetribusiPerso
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbReenter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbKEY = new System.Windows.Forms.TextBox();
            this.tbSNSAM = new System.Windows.Forms.TextBox();
            this.lblKey = new System.Windows.Forms.Label();
            this.lblSNSAM = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbReenter);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnConnect);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.tbKEY);
            this.groupBox2.Controls.Add(this.tbSNSAM);
            this.groupBox2.Controls.Add(this.lblKey);
            this.groupBox2.Controls.Add(this.lblSNSAM);
            this.groupBox2.Location = new System.Drawing.Point(13, 13);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(427, 290);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial Number and KEY SAM";
            // 
            // tbReenter
            // 
            this.tbReenter.Enabled = false;
            this.tbReenter.Location = new System.Drawing.Point(118, 149);
            this.tbReenter.MaxLength = 16;
            this.tbReenter.Name = "tbReenter";
            this.tbReenter.PasswordChar = '*';
            this.tbReenter.Size = new System.Drawing.Size(257, 23);
            this.tbReenter.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Re-enter KEY";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(230, 55);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(120, 25);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect SAM";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(146, 212);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(137, 50);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbKEY
            // 
            this.tbKEY.Enabled = false;
            this.tbKEY.Location = new System.Drawing.Point(118, 103);
            this.tbKEY.MaxLength = 16;
            this.tbKEY.Name = "tbKEY";
            this.tbKEY.PasswordChar = '*';
            this.tbKEY.Size = new System.Drawing.Size(257, 23);
            this.tbKEY.TabIndex = 4;
            this.tbKEY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKEY_KeyPress);
            // 
            // tbSNSAM
            // 
            this.tbSNSAM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbSNSAM.Enabled = false;
            this.tbSNSAM.Location = new System.Drawing.Point(118, 56);
            this.tbSNSAM.MaxLength = 12;
            this.tbSNSAM.Name = "tbSNSAM";
            this.tbSNSAM.Size = new System.Drawing.Size(106, 23);
            this.tbSNSAM.TabIndex = 1;
            this.tbSNSAM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSNSAM_KeyPress);
            // 
            // lblKey
            // 
            this.lblKey.AutoSize = true;
            this.lblKey.Location = new System.Drawing.Point(17, 106);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(35, 17);
            this.lblKey.TabIndex = 3;
            this.lblKey.Text = "KEY";
            // 
            // lblSNSAM
            // 
            this.lblSNSAM.AutoSize = true;
            this.lblSNSAM.Location = new System.Drawing.Point(17, 59);
            this.lblSNSAM.Name = "lblSNSAM";
            this.lblSNSAM.Size = new System.Drawing.Size(60, 17);
            this.lblSNSAM.TabIndex = 0;
            this.lblSNSAM.Text = "SN SAM";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(453, 316);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inject KEY";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbKEY;
        private System.Windows.Forms.TextBox tbSNSAM;
        private System.Windows.Forms.Label lblKey;
        private System.Windows.Forms.Label lblSNSAM;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox tbReenter;
        private System.Windows.Forms.Label label1;
    }
}

