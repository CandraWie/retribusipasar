﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using libCard;
using LibSmartCard;

namespace RegistrasiKartu
{
    public partial class RegistrasiKartuForm : Form
    {
        int retCode = 0;
        private LibCard smartCard;
        public RegistrasiKartuForm()
        {
            InitializeComponent();
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            if (tbNPWRD.Text == "")
            {
                MessageBox.Show("ID NPWPRD masih kosong", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbNPWRD.Focus();
                return;
            }
            if (tbNamaPedagang.Text == "")
            {
                MessageBox.Show("Nama Pedagang masih kosong", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbNamaPedagang.Focus();
                return;
            }

            //for (int i = 0; i < tbNamaPedagang.Text.Length; i++)
            //{
            //    if (!(tbNamaPedagang.Text[i] > 'A' && tbNamaPedagang.Text[i] < 'Z'))
            //    {
            //        MessageBox.Show("Nama Harus Huruf", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        tbNamaPedagang.Text = "";
            //        tbNamaPedagang.Focus();
            //        return;
            //    }
            //}

            DialogResult dialogResult = MessageBox.Show("Apakah anda yakin dengan data tersebut? \n " + tbNamaPedagang.Text.ToUpper() +" "+ tbNPWRD.Text.ToUpper(), "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.No)
                return;

            if (!smartCard.ConnectCard()) { MessageBox.Show("Tidak dapat membaca Kartu Samsat", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }
            if (!smartCard.DivKeyCard()) { MessageBox.Show("Kartu Samsat tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error); Cursor.Current = Cursors.Default; return; }

            int resetkartu = smartCard.ResetCard();
            if (resetkartu != 0)
            {
                MessageBox.Show("Kartu Sudah Terdaftar atau Bukan Kartu E-Retribusi", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string pedagang = Function.NumberToHexa(tbNamaPedagang.Text.Length, 2) + Function.ASCIIStringToHexString(tbNamaPedagang.Text.ToUpper(), "");
            string saldo = "0130";
            string idnopwrd = Function.NumberToHexa(tbNPWRD.Text.Length, 2) + Function.ASCIIStringToHexString(tbNPWRD.Text, "");

            retCode = smartCard.WriteData("0005", "25", "201", pedagang.PadRight(402, '0'));//nama
            if (retCode != 0)
            {
                MessageBox.Show("Penyimpanan Gagal", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbNPWRD.Text = "";
                tbNamaPedagang.Text = "";
                return;
            }
            
            retCode = smartCard.WriteData("0005", "265", "11", saldo.PadRight(22, '0'));
            if (retCode != 0)
            {
                MessageBox.Show("Penyimpanan Gagal", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbNPWRD.Text = "";
                tbNamaPedagang.Text = "";
                return;
            }

            retCode = smartCard.WriteData("0003", "34", "21", idnopwrd.PadRight(42, '0'));
            if (retCode != 0)
            {
                MessageBox.Show("Penyimpanan Gagal", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbNPWRD.Text = "";
                tbNamaPedagang.Text = "";
                return;
            }

            MessageBox.Show("Penyimpanan Berhasil", "Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information);
            tbNPWRD.Text = "";
            tbNamaPedagang.Text = "";
            return;
        }

        private void RegistrasiForm_Load(object sender, EventArgs e)
        {
            smartCard = new LibCard();
            try
            {
                if (!smartCard.ConnectSAMCard())
                {
                    MessageBox.Show("Belum terhubung dengan reader", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                }
                if (!smartCard.AuthSAM())
                {
                    MessageBox.Show("Tidak dapat Autentikasi SAM", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Apakah anda yakin ingin keluar dari Aplikasi ini ", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.No)
            {
                return;
            }
            else
                this.Close();
        }


        private void btnReg_MouseEnter(object sender, EventArgs e)
        {
            btnReg.Font = new Font(btnReg.Font.FontFamily, btnReg.Font.Size + 4, FontStyle.Bold);
            btnReg.Top = btnReg.Top - 10;
            btnReg.Left = btnReg.Left - 10;
            btnReg.Width = btnReg.Width + 20;
            btnReg.Height = btnReg.Height + 20;
        }

        private void btnReg_MouseLeave(object sender, EventArgs e)
        {
            btnReg.Font = new Font(btnReg.Font.FontFamily, btnReg.Font.Size - 4, FontStyle.Bold);
            btnReg.Top = btnReg.Top + 10;
            btnReg.Left = btnReg.Left + 10;
            btnReg.Width = btnReg.Width - 20;
            btnReg.Height = btnReg.Height - 20;
        }

        private void btnExit_MouseEnter(object sender, EventArgs e)
        {
            btnExit.Top = btnExit.Top - 5;
            btnExit.Left = btnExit.Left - 5;
            btnExit.Width = btnExit.Width + 10;
            btnExit.Height = btnExit.Height + 10;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.Top = btnExit.Top + 5;
            btnExit.Left = btnExit.Left + 5;
            btnExit.Width = btnExit.Width - 10;
            btnExit.Height = btnExit.Height - 10;
        }
    }
}
