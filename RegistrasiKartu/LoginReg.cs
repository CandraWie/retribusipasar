﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;
using LibSmartCard;

namespace RegistrasiKartu
{
    public partial class LoginReg : Form
    {
        string sAppAdd = AppDomain.CurrentDomain.BaseDirectory;
        byte[] passparameter = new byte[16];
        byte[] key = new byte[8];
        List<string> dataenkrip;
        List<string> dataasli;
        List<string>[] dataready = new List<string>[2];
        string skey = "3008201630102016";
        public LoginReg()
        {
            InitializeComponent();
        }

        private void GotoFormTopUp()
        {
            Application.Run(new RegistrasiKartuForm());
        }

        private void btnLanjut_Click(object sender, EventArgs e)
        {
            if (tbUserName.Text == "")
            {
                MessageBox.Show("Username Belum diisi", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbUserName.Focus();
                return;
            }
            if (tbPassword.Text == "")
            {
                MessageBox.Show("Password Belum diisi", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbPassword.Focus();
                return;
            }

            for (int i = 0; i < dataready[1].Count; i++)
            {
                if (tbPassword.Text.ToLower() == dataready[1][i] && tbUserName.Text.ToLower() == dataready[0][i])
                {
                    MessageBox.Show("Login Berhasil");
                    System.Threading.Thread t1 = new System.Threading.Thread(GotoFormTopUp);
                    t1.SetApartmentState(System.Threading.ApartmentState.STA);
                    t1.Start();
                    this.Close();
                    return;
                }
                else
                {
                    MessageBox.Show("Password atau Username Salah");
                    return;
                }
            }
        }

        private void SerialNumberKeyGen()
        {
            int ierror = KeygenProtection.CekFirstReg();
            if (ierror == 123)
            {
                ActivationKeyRequestReg akr = new ActivationKeyRequestReg(KeygenProtection.InjectReg());
                akr.ShowDialog();
            }
            else if (ierror == 3)
            {
                string snapp = "";
                if (KeygenProtection.ValidateValueSN())
                    snapp = KeygenProtection.GetValueSN();
                else
                    snapp = KeygenProtection.InjectReg();

                ActivationKeyRequestReg akr = new ActivationKeyRequestReg(snapp);
                akr.ShowDialog();
            }

            if (!KeygenProtection.CekValueRegNActKey())
            {
                MessageBox.Show("Kunci Aktivasi belum ada / tidak valid", "Informasi Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KeygenProtection.ResetActKey();
                this.Close();
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            SerialNumberKeyGen();
            dataenkrip = new List<string>();
            dataasli = new List<string>();
            if (!System.IO.File.Exists(sAppAdd + "SSAL.txt"))
            {
                MessageBox.Show("Belum Setting Login", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
           
            System.IO.StreamReader sr = System.IO.File.OpenText(sAppAdd + "SSAL.txt");
            string allfiletext = sr.ReadToEnd();
            for (int i = 200; i < 1024; i++)
            {
                if (i % 40 == 6)
                    dataenkrip.Add(allfiletext.Substring(i, 32));
            }
            sr.Close();
            for (int i = 0; i < dataenkrip.Count(); i++)
            {
                passparameter = Function.HexaStringToArrayOfByte(dataenkrip[i]);
                key = Function.HexaStringToArrayOfByte(skey);
                
                decryptDESECB(passparameter.Length, passparameter, key, out passparameter);
                dataasli.Add(ASCIIEncoding.ASCII.GetString(passparameter));
                for (int j = 0; j < dataasli[i].Length; j++)
                {
                    if (!((dataasli[i][j] >= '0' && dataasli[i][j] <= '9') || (dataasli[i][j] >= 'a' && dataasli[i][j] <= 'z') || (dataasli[i][j] == '*')))
                    {
                        dataasli[i] = "";
                        dataasli[i] = dataasli[i].PadRight(16, '*');
                    }
                }
            }

            dataready[0] = new List<string>();
            dataready[1] = new List<string>();
            for (int i = 0; i < dataasli.Count; i++)
            {
                if (dataasli[i] != "****************")
                {
                    dataready[0].Add(dataasli[i].Substring(0, 8).Replace("*", ""));
                    dataready[1].Add(dataasli[i].Substring(8, 8).Replace("*", ""));
                }
            }
        }

        private void encryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(true, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        private void decryptDESECB(int tmplong, byte[] input, byte[] key, out byte[] output)
        {
            BufferedBlockCipher cipher = new BufferedBlockCipher(new DesEngine());
            cipher.Init(false, new DesParameters(key));

            byte[] outBytes = new byte[input.Length];
            int len2 = cipher.ProcessBytes(input, 0, input.Length, outBytes, 0);

            cipher.DoFinal(outBytes, len2);
            output = outBytes;
        }

        private void lbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
