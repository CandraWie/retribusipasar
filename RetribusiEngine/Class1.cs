﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;



namespace RetribusiEngine
{
    public class Class1
    {

    }

    public class CardEngine
    {
        #region SmartCard
        private struct SCARD_IO_REQUEST
        {
            public int dwProtocol;
            public int cbPciLength;
        }

        [DllImport("winscard.dll")]
        private static extern int SCardEstablishContext(int dwScope, int pvReserved1, int pvReserved2, ref int phContext);

        [DllImport("winscard.dll")]
        private static extern int SCardReleaseContext(int phContext);

        [DllImport("winscard.dll")]
        private static extern int SCardConnect(int hContext, string szReaderName, int dwShareMode, int dwPrefProtocol, ref int phCard, ref int ActiveProtocol);

        [DllImport("winscard.dll")]
        private static extern int SCardDisconnect(int hCard, int Disposition);

        [DllImport("winscard.DLL", EntryPoint = "SCardListReadersA", CharSet = CharSet.Ansi)]
        private static extern int SCardListReaders(
            int hContext,
            byte[] Groups,
            byte[] Readers,
            ref int pcchReaders
            );


        [DllImport("winscard.dll")]
        private static extern int SCardStatus(int hCard, string szReaderName, ref int pcchReaderLen, ref int State, ref int Protocol, ref byte ATR, ref int ATRLen);

        [DllImport("winscard.dll")]
        private static extern int SCardTransmit(int hCard, ref SCARD_IO_REQUEST pioSendRequest, ref byte SendBuff, int SendBuffLen, ref SCARD_IO_REQUEST pioRecvRequest, ref byte RecvBuff, ref int RecvBuffLen);

        private string sCsReader, sCReader;
        int retCode, hContext, CshCard, ChCard, CsProtocol, CProtocol;
        SCARD_IO_REQUEST pioSendRequest;
        byte[] SendBuff = new byte[256];
        byte[] RecvBuff = new byte[256];
        int SendLen = 0;
        int RecvLen = 255;
        string spesan = "";

        private bool InitReader()
        {
            try
            {
                retCode = SCardEstablishContext(0, 0, 0, ref hContext);
                if (retCode != 0)
                    return false;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return true;
        }

        private bool ConnectCard(string varReaderName, ref int varhCard, ref int varProtocol)
        {
            try
            {
                retCode = SCardConnect(hContext, varReaderName, 2, 1 | 2, ref varhCard, ref varProtocol);
                if (retCode != 0)
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return true;
        }

        private void DisconnectCard(int varhCard)
        {
            SCardDisconnect(varhCard, 2);
        }

        private bool SendCommand(int varhCard, int varProtocol)
        {
            try
            {
                pioSendRequest.dwProtocol = varProtocol;
                pioSendRequest.cbPciLength = 8;
                RecvLen = 255;
                RecvBuff[0] = 0;
                RecvBuff[1] = 0;
                retCode = SCardTransmit(varhCard, ref pioSendRequest, ref SendBuff[0], SendLen, ref pioSendRequest, ref RecvBuff[0], ref RecvLen);

                if (retCode != 0)
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return true;
        }
        #endregion

        private byte[] SAMSesKey = new byte[16];

        public CardEngine()
        {
            string spath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            
            try
            {
                if (!System.IO.File.Exists(spath + "\\Parameter.par"))
                    throw new Exception("Aplikasi rusak");
                using (System.IO.StreamReader sr = System.IO.File.OpenText(spath + "\\Parameter.par"))
                {
                    while (!sr.EndOfStream)
                    {
                        string sparam = sr.ReadLine();
                        if (sparam.StartsWith("contact : "))
                            sCReader = sparam.Replace("contact : ", "").Trim();
                        if (sparam.StartsWith("contactless : "))
                            sCsReader = sparam.Replace("contactless : ", "").Trim();
                    }
                }

                if ((sCReader == "") || (sCsReader == ""))
                    throw new Exception("Parameter rusak");
            }
            catch
            {
                throw new Exception("Aplikasi rusak");
            }
        }

        public bool InitialReader()
        {
            try
            {
                if (!InitReader())
                    return false;
                InitSAM();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return true;
        }

        public void InitSAM()
        {
            if (!ConnectCard(sCReader, ref ChCard, ref CProtocol))
                throw new Exception("SAM tidak terdeteksi");

            SendBuff = new byte[] { 0x00, 0xA4, 0x04, 0x00, 0x09, 0x42, 0x4A, 0x54, 0x45, 0x4E, 0x47, 0x53, 0x41, 0x4D, 0x00 };
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 01");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0) || (RecvLen != 2))
                throw new Exception("SAM error : 02");

            // auth SAM
            byte[] rndA = new byte[8];
            byte[] rndB = new byte[8];

            byte[] rndAEnc = new byte[8];
            byte[] rndBRot = new byte[8];

            byte[] rndA_rndB = new byte[16];
            byte[] rndA_rndB_Enc = new byte[16];

            SendBuff = new byte[] { 0x00, 0xAA, 0x01, 0x00, 0x08 };
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 03");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0) || (RecvLen != 10))
                throw new Exception("SAM error : 04");
            Array.Copy(RecvBuff, rndAEnc, 8);

            SendBuff = new byte[13];
            Array.Copy(new byte[] { 0x00, 0x3D, 0x03, 0x00, 0x08 }, SendBuff, 5);
            Array.Copy(rndAEnc, 0, SendBuff, 5, 8);
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 05");
            if ((RecvBuff[RecvLen - 2] != 0x61) || (RecvBuff[RecvLen - 1] != 0x08))
                throw new Exception("SAM error : 06");

            SendBuff = new byte[] { 0x00, 0xC0, 0x00, 0x00, 0x08 };
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 07");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0) || (RecvLen != 10))
                throw new Exception("SAM error : 08");
            Array.Copy(RecvBuff, rndA, 8);

            Random x = new Random();
            for (int i = 0; i < 8; i++)
            {
                rndB[i] = (byte)(x.Next(256));
                rndA_rndB[i] = (byte)(rndB[i] ^ rndA[i] ^ 0xDD);
                rndA_rndB[i + 8] = rndA[i];
            }

            SendBuff = new byte[21];
            Array.Copy(new byte[] { 0x00, 0x3D, 0x03, 0x00, 0x10 }, SendBuff, 5);
            Array.Copy(rndA_rndB, 0, SendBuff, 5, 16);
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 09");
            if ((RecvBuff[RecvLen - 2] != 0x61) || (RecvBuff[RecvLen - 1] != 0x10))
                throw new Exception("SAM error : 10");

            SendBuff = new byte[] { 0x00, 0xC0, 0x00, 0x00, 0x10 };
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 11");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0) || (RecvLen != 18))
                throw new Exception("SAM error : 12");
            Array.Copy(RecvBuff, rndA_rndB_Enc, 16);

            SendBuff = new byte[21];
            Array.Copy(new byte[] { 0x00, 0xAA, 0x02, 0x00, 0x10 }, SendBuff, 5);
            Array.Copy(rndA_rndB_Enc, 0, SendBuff, 5, 16);
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 13");
            if ((RecvBuff[RecvLen - 2] != 0x61) || (RecvBuff[RecvLen - 1] != 0x08))
                throw new Exception("SAM error : 14");

            SendBuff = new byte[] { 0x00, 0xC0, 0x00, 0x00, 0x08 };
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 15");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0) || (RecvLen != 10))
                throw new Exception("SAM error : 16");

            SendBuff = new byte[13];
            Array.Copy(new byte[] { 0x00, 0x3D, 0x03, 0x00, 0x08 }, SendBuff, 5);
            Array.Copy(RecvBuff, 0, SendBuff, 5, 8);
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 17");
            if ((RecvBuff[RecvLen - 2] != 0x61) || (RecvBuff[RecvLen - 1] != 0x08))
                throw new Exception("SAM error : 18");

            SendBuff = new byte[] { 0x00, 0xC0, 0x00, 0x00, 0x08 };
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("SAM error : 19");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0) || (RecvLen != 10))
                throw new Exception("SAM error : 20");
            Array.Copy(RecvBuff, rndBRot, 8);

            for (int i = 1; i < 8; i++)
                if (rndB[i] != rndBRot[i - 1])
                    throw new Exception("SAM error : 21");

            if (rndB[0] != rndBRot[7])
                throw new Exception("SAM error : 22");

            SAMSesKey = new byte[16];

            for (int i = 0; i < 4; i++)
            {
                SAMSesKey[i] = rndB[i];
                SAMSesKey[i + 4] = rndA[i + 4];
                SAMSesKey[i + 8] = rndB[i + 4];
                SAMSesKey[i + 12] = rndA[i];
            }
        }

        private void DivKey()
        {
            SendBuff = new byte[] { 0xFF, 0xCA, 0x00, 0x00, 0x00 };
            SendLen = SendBuff.Length;
            if (!SendCommand(CshCard, CsProtocol))
                throw new Exception("Error Generate Key 01");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                throw new Exception("Error Generate Key 02");
            if (RecvLen != 6)
                throw new Exception("Error Generate Key 03");

            SendBuff = new byte[RecvLen - 2 + 5];
            Array.Copy(new byte[] { 0x00, 0xD1, 0x00, 0x00, 0x04 }, SendBuff, 5);
            Array.Copy(RecvBuff, 0, SendBuff, 5, RecvLen - 2);
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("Error Generate Key 04");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                throw new Exception("Error Generate Key 05");
        }

        public void CardMonitoring()
        {
            System.Threading.Thread t1 = new System.Threading.Thread(MainMonitoring);
            t1.Start();
        }

        public void ReadCard(out string sDataRead)
        {
            try
            {
                InitSAM();

                if (!ConnectCard(sCsReader, ref CshCard, ref CsProtocol))
                    throw new Exception("Error Read Data Card 01");

                DivKey();
                byte[] bDataRead = new byte[10];
                ReadCard(new byte[] { 0, 1 }, 1, 10, ref bDataRead);
                sDataRead = ASCIIEncoding.ASCII.GetString(bDataRead);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void ReadBinaryFileEncMac(int iOffset, int iLength, out byte[] data)
        {
            data = new byte[iLength];
            int ioff = iOffset;
            int ilen = iLength;
            int lengthTemp;
            int iindexdata = 0;

            while ((ilen > 0))
            {
                SendBuff = new byte[] { 0x00, 0x84, 0x00, 0x00, 0x08 }; //--0084000008
                SendLen = SendBuff.Length;
                if (!SendCommand(CshCard, CsProtocol))
                    throw new Exception("Error Read 01");
                if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                    throw new Exception("Error Read 02");
                byte[] bChall = new byte[8];
                Array.Copy(RecvBuff, bChall, 8);

                SendBuff = new byte[18];
                Array.Copy(new byte[] { 0x00, 0x04, 0x00, 0x00, 0x0d, 0x04, 0xb0 }, SendBuff, 7);   //--000400000d04b0
                SendBuff[7] = (byte)((ioff >> 8) & 0xFF);
                SendBuff[8] = (byte)(ioff & 0xFF);
                SendBuff[9] = 4;
                Array.Copy(bChall, 0, SendBuff, 10, 8);
                SendLen = SendBuff.Length;
                if (!SendCommand(ChCard, CProtocol))
                    throw new Exception("Error Read 03");

                if (RecvBuff[RecvLen - 2] == 0x61)
                {
                    SendBuff = new byte[5];
                    Array.Copy(new byte[] { 0x00, 0xC0, 0x00, 0x00 }, SendBuff, 4);         //-- 00C00000
                    SendBuff[4] = RecvBuff[RecvLen - 1];
                    SendLen = SendBuff.Length;
                    if (!SendCommand(ChCard, CProtocol))
                        throw new Exception("Error Read 04");
                    if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                        throw new Exception("Error Read 05");
                }

                lengthTemp = (ilen >= 200) ? 200 : ilen;
                SendBuff = new byte[14];
                SendBuff[0] = 0x04;         //
                SendBuff[1] = 0xB0;
                SendBuff[2] = (byte)((ioff >> 8) & 0xFF);
                SendBuff[3] = (byte)(ioff & 0xFF);
                SendBuff[4] = 0x04;
                Array.Copy(RecvBuff, 0, SendBuff, 5, 8);
                SendBuff[13] = (byte)lengthTemp;
                SendLen = SendBuff.Length;
                if (!SendCommand(CshCard, CsProtocol))
                    throw new Exception("Error Read 06");
                if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                    throw new Exception("Error Read 07");

                SendBuff = new byte[RecvLen - 2 + 5];
                Array.Copy(new byte[] { 0x00, 0x05, 0x00, 0x00 }, SendBuff, 4);     //-- 00050000
                SendBuff[4] = (byte)(RecvLen - 2);
                Array.Copy(RecvBuff, 0, SendBuff, 5, RecvLen - 2);
                SendLen = SendBuff.Length;
                if (!SendCommand(ChCard, CProtocol))
                    throw new Exception("Error Read 08");
                if (RecvBuff[RecvLen - 2] == 0x61)
                {
                    SendBuff = new byte[5];
                    Array.Copy(new byte[] { 0x00, 0xC0, 0x00, 0x00 }, SendBuff, 4); // -- 
                    SendBuff[4] = RecvBuff[RecvLen - 1];
                    SendLen = SendBuff.Length;
                    if (!SendCommand(ChCard, CProtocol))
                        throw new Exception("Error Read 09");
                    if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                        throw new Exception("Error Read 10");
                }
                if (RecvBuff[0] != lengthTemp)
                    throw new Exception("Error Read 11");

                Array.Copy(RecvBuff, 0, data, iindexdata, lengthTemp);
                iindexdata += lengthTemp;
                ilen -= lengthTemp;
                ioff += lengthTemp;
                //CAPDU = "04B0" + offset.ToString("X4") + "04" + RAPDU.Substring(0, 8) + lengthTemp.ToString("X2");
                //if (!(SendCommandCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                //int lensementara = (RAPDU.Length / 2) - 2;

                //CAPDU = "00050000" + lensementara.ToString("X2") + RAPDU.Substring(0, RAPDU.Length - 4);
                //if (!(SendCommandSAMCard())) return false;
                //enamsatu = (int)(RAPDU.Length - 4);
                //sekiansekian = enamsatu + 2;
                //if (RAPDU.Substring(enamsatu, 2) == "61")
                //{
                //    CAPDU = "00C00000" + RAPDU.Substring(sekiansekian, 2);
                //    if (!(SendCommandSAMCard()) || (!(RAPDU.EndsWith("9000")))) return false;
                //}
                //if (RAPDU.Substring(0, 2) != lengthTemp.ToString("X2")) { return false; }

                //tmpHex += RAPDU.Substring(2, lengthTemp * 2);
                //length = length - lengthTemp;
                //offset = offset + lengthTemp;
            }

            //data = Function.HexaStringToArrayOfByte(tmpHex);
        }

        private void ReadCard(byte[] bFileAddress, int iOffset, int iLength, ref byte[] bResult)
        {
            bResult = new byte[iLength];

            if ((iOffset > 1024) || (iOffset + iLength > 1024) || (iLength == 0))
                throw new Exception("Offset dan Panjang data tidak valid");
            try
            {
                SelectFile(new byte[] { 0x25, 0x08 });
                SelectFile(bFileAddress);
                AuthCardviaSAM(7);
                ReadBinaryFileEncMac(iOffset, iLength, out bResult);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void AuthCardviaSAM(int ikeyno)
        {
            SendBuff = new byte[] { 0x00, 0x84, 0x00, 0x00, 0x08 };
            SendLen = SendBuff.Length;
            if (!SendCommand(CshCard, CsProtocol))
                throw new Exception("Error AuthviaSAM 01");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                throw new Exception("Error AuthviaSAM 02");
            if (RecvLen != 10)
                throw new Exception("Error AuthvaiSAM 03");

            SendBuff = new byte[13];
            Array.Copy(new byte[] { 0x00, 0x02, 0x00 }, SendBuff, 3);
            SendBuff[3] = (byte)(ikeyno & 0xFF);
            SendBuff[4] = 0x08;
            Array.Copy(RecvBuff, 0, SendBuff, 5, 8);
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("Error AuthviaSAM 04");
            if ((RecvBuff[RecvLen - 2] != 0x61) || (RecvBuff[RecvLen - 1] != 0x0D))
                throw new Exception("Error AuthviaSAM 05");

            SendBuff = new byte[] { 0x00, 0xC0, 0x00, 0x00, 0x0D };
            SendLen = SendBuff.Length;
            if (!SendCommand(ChCard, CProtocol))
                throw new Exception("Error AuthviaSAM 06");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0x00))
                throw new Exception("Error AuthviaSAM 07");
            if (RecvLen != 15)
                throw new Exception("Error AuthviaSAM 08");

            SendBuff = new byte[13];
            Array.Copy(RecvBuff, SendBuff, 13);
            SendLen = SendBuff.Length;
            if (!SendCommand(CshCard, CsProtocol))
                throw new Exception("Error AuthviaSAM 09");
            if ((RecvBuff[RecvLen - 2] != 0x90) || (RecvBuff[RecvLen - 1] != 0))
                throw new Exception("Error AuthviaSAM 10");
        }

        private void SelectFile(byte[] sFileAddress)
        {
            SendBuff = new byte[7];
            Array.Copy(new byte[] { 0x00, 0xa4, 0x00, 0x00 }, SendBuff, 4);
            SendBuff[4] = 2;
            Array.Copy(sFileAddress, 0, SendBuff, 5, 2);
            SendLen = SendBuff.Length;

            if (!SendCommand(CshCard, CsProtocol))
                throw new Exception("Error Select File 01");
        }

        private bool WriteCard(string sFieldAdd, int Offset, int iLength, byte[] bData)
        {
            return true;
        }

        public void MainMonitoring()
        {

        }

    }

    public class AutoUpdateIdentitas : EventArgs
    {
        public string NamaPemegang { get; set; }
        public string Alamat { get; set; }
        public string SubAlamat1 { get; set; }
        public string SubAlamat2 { get; set; }
        public string Deposit { get; set; }
        public string TransaksiTerakhir { get; set; }
        public string Keterangan { get; set; }
    }

    public class AutoUpdateTransaksi : EventArgs
    {
        public string Deposit { get; set; }
        public string TransaksiTerakhir { get; set; }
        public string Keterangan { get; set; }
    }
}
